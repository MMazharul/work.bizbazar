<?php

defined('BASEPATH') or exit('No direct script access allowed');



class Loadapi

{
	/* ------------ SMS sending by BrandSMS---------------- */

	function load_api($number,$number_type,$operator,$amount)
	{

		/* number */
		$mob = preg_replace('/(880|0|88)/A', '', $number);

		$num = "880" . $mob;

		/* generated api key */

		$ci = &get_instance();

		$query = $ci->db->query("SELECT * from flexiload_api_setting WHERE id=1");
		$api_info= $query->result();
		$api_info=$api_info[0];
		$API_key = $api_info->api_key;


		$data = array(
			"api_key" => $API_key,
			"pin" => $api_info->pin_number,
			"number" => $num,
			"amount" => $amount,
			"number_type" => $number_type,
			"operator" => $operator,
		);

		$url = "http://sms.biz-bazar.com/api/v1/send-load";

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($ch, CURLOPT_POST, 1);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));

		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);

		curl_setopt($ch, CURLOPT_TIMEOUT, 60);



		$result = curl_exec($ch);

		if (curl_errno($ch) !== 0) {

			error_log('cURL error when connecting to ' . $url . ': ' . curl_error($ch));

		}

		curl_close($ch);
		return $result;

	}



}
