<?php
defined('BASEPATH') or exit('No direct script access allowed');

function get_user_data($user_id)
{
  $ci = &get_instance();

  $query = $ci->db->query("SELECT
    a.name,
    a.picture,
    a.active_status,
    a.own_refer_id,
    b.total_refered,
    b.reputation_lvl,
    b.milestone_lvl,
    c.percentage,
    c.reputation_milestone_name,
    c.reputation_milestone_icon,
    c.reputation_icon,
    c.milestone_icon,
    (d.`refer_earn_amount`+d.`work_earn_amount`+d.`refer_work_earn_amount`+d.`other_earn_amount`) as balance
    FROM
    users_detail a,
    user_lvl b,
    refer_earn_plan_distribution c,
    total_balance d
    WHERE	
    a.id = '$user_id'
    AND 
    b.user_id = '$user_id' AND c.reputation_lvl =(SELECT reputation_lvl FROM user_lvl WHERE user_id = '$user_id') 
    AND
    c.milestone_lvl =(SELECT milestone_lvl FROM user_lvl WHERE user_id = '$user_id')
    AND
    d.user_id = '$user_id'");

  $rs = $query->result();
  if ($rs == null) {
    $q = $ci->db->query("SELECT a.name, a.picture, a.active_status, a.own_refer_id,(0) as total_refered,(3) as percentage, (1) as reputation_lvl,(1) as milestone_lvl,('Bronze Basic') as reputation_milestone_name,('Bronze_1.svg') as reputation_milestone_icon, ('bronze.svg') as reputation_icon, ('1.svg') as milestone_icon, (0) as balance FROM users_detail a WHERE id = '$user_id'");
    $rs = $q->result();
  }
  return $rs[0];
}

function notifications($user_id)
{
  $ci = &get_instance();

  $query = $ci->db->query("SELECT * from notifications WHERE target = (SELECT active_status FROM users_detail WHERE id = '$user_id') or target=3 or user_id = '$user_id' ORDER  by created_at DESC LIMIT 3");
  return $query->result();
}

function notify_count($user_id)
{
  $ci = &get_instance();

  $query = $ci->db->query("SELECT count(id) as coun FROM `notifications` WHERE viewed = 0 AND user_id = '$user_id'");
  $r = $query->result();
  return $r[0]->coun;
}


function time_ago($timestamp)
{
  date_default_timezone_set("Asia/Dhaka");
  $time_ago = strtotime($timestamp);
  $current_time = time();
  $time_difference = $current_time - $time_ago;
  $seconds = $time_difference;

  $minutes = round($seconds / 60); // value 60 is seconds  
  $hours = round($seconds / 3600); //value 3600 is 60 minutes * 60 sec  
  $days = round($seconds / 86400); //86400 = 24 * 60 * 60;  
  $weeks = round($seconds / 604800); // 7*24*60*60;  
  $months = round($seconds / 2629440); //((365+365+365+365+366)/5/12)*24*60*60  
  $years = round($seconds / 31553280); //(365+365+365+365+366)/5 * 24 * 60 * 60

  if ($seconds <= 60) {

    return "Just Now";

  } else if ($minutes <= 60) {

    if ($minutes == 1) {

      return "1 min ago";

    } else {

      return "$minutes min ago";

    }

  } else if ($hours <= 24) {

    if ($hours == 1) {

      return "an hr ago";

    } else {

      return "$hours hrs ago";

    }

  } else if ($days <= 7) {

    if ($days == 1) {

      return "yesterday";

    } else {

      return "$days days ago";

    }

  } else if ($weeks <= 4.3) {

    if ($weeks == 1) {

      return "a week ago";

    } else {

      return "$weeks weeks ago";

    }

  } else if ($months <= 12) {

    if ($months == 1) {

      return "a month ago";

    } else {

      return "$months months ago";

    }

  } else {

    if ($years == 1) {

      return "one year ago";

    } else {

      return "$years years ago";

    }
  }
}
