<?php

defined('BASEPATH') or exit('No direct script access allowed');


function chk_login()
{
    if (isset($_SESSION['login']) && $_SESSION['login']) {
        return true;
    } else {
        redirect('/');
        return false;
    }
    return false;
}

function logout()
{
    $ci = &get_instance();
    $ci->output->delete_cache('/dashboardCtrl');
    $ci->output->delete_cache('/jobCtrl');
    $ci->output->delete_cache('/welcomeCtrl');

    unset($_SESSION['login'], $_SESSION['user_id'], $_SESSION['referer_id'], $_SESSION['referer_id'], $_SESSION['referer_earn']);
    session_destroy();
    session_unset();
    redirect('/');
}
