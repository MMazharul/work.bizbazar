
<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-danger text-white mr-2">
			<i class="mdi mdi-home"></i>
		</span>
		Refer Earned Reports
	</h3>
	
</div>
<div class="row">
	<div class="col-md-4 stretch-card grid-margin">
		<div class="card bg-gradient-primary card-img-holder shadow text-white">
			<div class="card-body">
				<img src="<?= base_url() ?>app-assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
				<h4 class="font-weight-normal mb-3">Today Earn by Refer Members
					<i class="mdi mdi-calendar-clock mdi-48px float-right"></i>
				</h4>
				<h2 class="mb-5">৳ <?= $today->total_earn==null ? "0" : number_format($today->total_earn,2) ?></h2>
				<h6 class="card-text">Total Job Completed <?= $today->total_job?></h6>
			</div>
		</div>
	</div>
	<div class="col-md-4 stretch-card grid-margin">
		<div class="card bg-gradient-info card-img-holder shadow text-white">
			<div class="card-body">
				<img src="<?= base_url() ?>app-assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
				<h4 class="font-weight-normal mb-3">Last 7 day Earn by Refer Members
					<i class="mdi mdi-calendar mdi-48px float-right"></i>
				</h4>
				<h2 class="mb-5">৳ <?= $svn_day->total_earn==null ? "0" : number_format($svn_day->total_earn,2) ?></h2>
				<h6 class="card-text">Total Job Completed <?=$svn_day->total_job?></h6>
			</div>
		</div>
	</div>
	<div class="col-md-4 stretch-card grid-margin">
		<div class="card bg-gradient-success card-img-holder shadow text-white">
			<div class="card-body">
				<img src="<?= base_url() ?>app-assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
				<h4 class="font-weight-normal mb-3">Last 30 day Earn by Refer Members
					<i class="mdi mdi-calendar-multiple-check mdi-48px float-right"></i>
				</h4>
				<h2 class="mb-5">৳ <?= $thirty_day->total_earn==null ? "0" : number_format($thirty_day->total_earn,2) ?></h2>
				<h6 class="card-text">Total Job Completed <?=$thirty_day->total_job?></h6>
			</div>
		</div>
	</div>
</div>

<div class="row">
<div class="col-12 stretch-card grid-margin">
		<div class="card bg-gradient-dark card-img-holder shadow text-white">
			<div class="card-body">
				<img src="<?= base_url() ?>app-assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
				<h4 class="font-weight-normal mb-3">Total Earn by Refer Members
					<i class="mdi mdi-counter mdi-48px float-right"></i>
				</h4>
				<h2 class="mb-5">৳ <?= $total->total_earn==null ? "0" : number_format($total->total_earn,2) ?></h2>
				<h6 class="card-text">Total Job Completed <?=$total->total_job?></h6>
			</div>
		</div>
	</div>
</div>
