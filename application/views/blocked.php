<div class="row justify-content-center align-items-center" style="height:100vh">
	<div class="col-md-4 col-10 shadow border20 text-center" >
	<img src="<?=base_url('app-assets/images/block.gif')?>" width="200px" alt="">
	
	<h2 class="text-center text-danger">
	<i class="mdi mdi-close-circle-outline mdi-48px text-danger"></i>
	<br>
	Blocked</h2>
	
	<p class="text-center">Dear User Your Account Has Been Blocked
	<br>
	Due To Some Violance.
	<br>
	Please Contact with Quick-Earn Support <span class="text-info">Facebook</span> Page.
	<br>
	Thank You !
	<br>
	<br>
	<small><a href="<?=base_url()?>">Back to Home</a></small>
	</p>
	</div>
</div>


<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
	window.fbAsyncInit = function () {
		FB.init({
			appId: '912333495590130',
			autoLogAppEvents: true,
			xfbml: true,
			version: 'v2.11'
		});
	};

	(function (d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s);
		js.id = id;
		js.src = "<?=base_url('app-assets/js/facebook.customerChat.js')?>";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>

<!-- Your customer chat code -->
<div class="fb-customerchat" attribution=setup_tool page_id="386353565445110" theme_color="#00b8ff">
</div>