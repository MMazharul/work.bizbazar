<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-primary text-white mr-2">
			<i class="mdi mdi-home"></i>
		</span>
		Activation Process
	</h3>
	<nav aria-label="breadcrumb">
		<ul class="breadcrumb">
			<li class="breadcrumb-item active" aria-current="page">
				<span></span>Steps
				<i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
			</li>
		</ul>
	</nav>
</div>

<div class="row justify-content-center">
    <div class="col-md-12 col-sm-12 text_center align-items-center card m-2 p-2">
		<img src="<?=base_url('app-assets/images/bkash/BKash-Logo-English.jpg')?>" width="200px" alt="quick-earn.info bkash">
        <br>
		<h4 class="text-center">1. Type <b>*247#</> </h4>
        <h4 class="text-center">2. Our Registration Fee : <b><?=$join_fee_mobile->join_fee?> Tk</b></h4>
		<h4 class="text-center">3. Please Send our fee in this</h4>
        <h4 class="text-center">bKash <u><?=$join_fee_mobile->pay_type?></u> Number : <b>0<?=$join_fee_mobile->mobile?></b></h4>
		<small class-"text-center">Biz-Bazar এর রেজিস্ট্রেশন এর জন্য নির্ধারিত ফি বিকাশের এজেন্ট(Agent) নাম্বার এর  মাধ্যমের পাঠানোর সময় যদি লিমিট-এক্সিড (Limit Exceed) দেখায় কিংবা কোন কারনে পাঠানো সম্ভব না হয় তবে দয়া করে পার্সোনাল(Personal) বিকাশ নাম্বার থেকে সেন্ড -মানি (Send Money)  অপশন থেকে পুনরায় টাকা পাঠানোর চেস্টা করুন। 
ধন্যবাদ</small> 
    </div>

</div>

<div class="row justify-content-center alert alert-info border10 shadow p-3">

	<h4 class="col-12">Activation</h4>

	<div class="form-group col-md-3 col-12">
		<label  for="ac_pin">Account Activation Pin</label>
		<input type="text" class="form-control" id="ac_pin" placeholder="Pin">
		<button id="resend_c" onclick="resend_code()" class="btn btn-link">Resend code in mobile<i class="mdi mdi-sync mdi-spin loading"></i> </button>
	</div>

	<div class="form-group col-md-3 col-12">
		<label  for="inlineFormInputGroup">Sender Number</label>
		<input type="number" class="form-control" id="sndr_num" placeholder="Sender Number">
	</div>

	<div class="form-group col-md-3 col-12">
		<label  for="inlineFormInputGroup">TrxID (Transection ID)</label>
		<input type="text" class="form-control" id="tran_id" placeholder="Transection ID">
	</div>

	<div class="form-group col-md-3 col-12">
		<label  for="inlineFormInputGroup">Rreferal ID</label>
		<input type="text" class="form-control" id="refer_id" placeholder="Transection ID">
	</div>

	<div class="form-group col-md-3 col-12 text-center">
		<button onclick="active_req(this)" type="button" class="btn btn-gradient-dark btn-rounded shadow">Send Request <i class="mdi mdi-chevron-right"></i><br><small>(Proceed Time 24hrs)</small><i style="display:none" class="mdi mdi-loading mdi-spin loading"></i></button>
		<div id="activ_rst"></div>
	</div>

</div>

<script>
function resend_code() {
		let id = '<?=$this->session->user_id?>';
		$.ajax({
			type: "post",
			url: "<?=base_url('ActivateCtrl/resend_code')?>",
			data: {id:id},
			dataType: "json",
			success: function (response) {
				if(response.success == 1){
					document.getElementById('resend_c').innerHTML += "<i class='mdi mdi-check'></i>";
					$(".loading").hide();
				}
			}
		});
	}
</script>
