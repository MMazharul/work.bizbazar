<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-success text-white mr-2">
			<i class="mdi mdi-briefcase-outline mdi-24px"></i>
		</span>
		Freelance Jobs
	</h3>
</div>


<div class="row justify-content-center">
	<div class="col-12 col-md-12 p-0">

		<?php
		use Faker\Provider\zh_CN\DateTime;
        use Illuminate\Support\Facades\Date;

foreach ($jobs as $job) : ?>
			<div class="card bg-white border20 p-3 mb-4">
				<div>
					<h4 class="mb-2"><a href="/BuyerWorkCtrl/job_view?i=<?= base64_encode($job->id) ?>" class="text-da"> <?= $job->title ?></a></h4>
					<div class="row">
						<div class="col-12 col-md-4">
							<small class="d-flex align-items-center flex-wrap mb-1">Category: &nbsp;
								<span class="badge badge-pill badge-outline-info "><?= $job->cat_name ?></span>
							</small>
						</div>
						<div class="col-12 col-md-8">
							<small class="d-flex align-items-center flex-wrap mb-1">
								<?php if ($job->skills != null && $job->skills != "") :
									$skills = explode(",", $job->skills); ?>
									Skills: &nbsp;
									<?php foreach ($skills as $skill) : ?>
										<span class="badge badge-pill badge-outline-success mr-2"><?= $skill ?></span>
									<?php endforeach; ?>
								<?php endif ?>

							</small>
						</div>
					</div>

					<p><b class="text-indigo"><i>Budget:</i> ৳ <?= $job->budget ?></b>
						<span class="float-right">
							<?php $dif = date_diff(date_create(date('Y-m-d')),date_create($job->exp_date));echo $dif->d > 0 && $dif->invert ==1 ? '<span class="badge badge-pill badge-danger"><i class="mdi mdi-calendar"></i> Expired</span><br>' : '' ?>
							<small>Exp: <?= nice_date($job->exp_date, 'd/M/y') ?></small>
							<br>
							<small>Posted: <?= nice_date($job->created_at, 'd/M/y') ?></small>
						</span>
					</p>
					<a><?= substr($job->job_desc, 0, 200) ?>
						<br><a href="/BuyerWorkCtrl/job_view?i=<?= base64_encode($job->id) ?>">See More...</a></p>

				</div>
			</div>
		<?php endforeach ?>

	</div>
</div>