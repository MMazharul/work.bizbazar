
<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-info text-white mr-2">
			<i class="fa fa-cube"></i>
		</span>
		 Flexiload Transaction
	</h3>

</div>
<div class="row">
	<div class="col-md-3 mb-2">
		<div class="p-2 border10 bg-white  text-dark">
			<div class="">
				<h5 class="font-weight-normal mb-3">Todays Transaction
					<i class="fa fa-calendar-check-o fa-3x float-right"></i>
				</h5>
				<h4 class="mb-5">৳ </h4>
				
			</div>
		</div>
	</div>
	<div class="col-md-3 mb-2">
		<div class="bg-white p-2 border10  text-dark">
			<div class="">
				<h5 class="font-weight-normal mb-3">Last 7 days Transaction
					<i class="fa fa-calendar-minus-o fa-3x float-right"></i>
				</h5>
				<h4 class="mb-5">৳ </h4>
				
			</div>
		</div>
	</div>
	<div class="col-md-3 mb-2">
		<div class="bg-white p-2 border10 text-dark">
			<div class="">
				<h5 class="font-weight-normal mb-3">Last 30 days Transaction
					<i class="fa fa-calendar-o fa-3x float-right"></i>
				</h5>
				<h4 class="mb-5">৳ </h4>
				<h6 class="card-text">Total Transection</h6>
			</div>
		</div>
	</div>
	<div class="col-md-3 mb-2">
		<div class="bg-white p-2 border10  text-dark">
			<div class="">
				<h5 class="font-weight-normal mb-3">Total Transaction
					<i class="fa fa-calendar-check-o fa-3x float-right"></i>
				</h5>
				<h4 class="mb-5">৳ </h4>
				
			</div>
		</div>
	</div>
	<div class="col-md-3 mb-2">
		<div class="bg-white p-2 border10  text-dark">
			<div class="">
				<h5 class="font-weight-normal mb-3">Bonus Earn
					<i class="fa fa-calendar-check-o fa-3x float-right"></i>
				</h5>
				<h4 class="mb-5">৳ </h4>

			</div>
		</div>
	</div>

	<div class="col-md-3 mb-2">
		<div class="bg-white p-2 border10  text-dark">
			<div class="">
				<h5 class="font-weight-normal mb-3">Total Balance
					<i class="fa fa-calendar-check-o fa-3x float-right"></i>
				</h5>
				<h4 class="mb-5">৳ <?=$user_blance->blance==null? '0': $user_blance->blance?></h4>
				
			</div>
		</div>
	</div>
</div>

