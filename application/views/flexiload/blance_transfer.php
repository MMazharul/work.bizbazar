<div >
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">Balance Transfer</h4>

			<form class="forms-sample" method="post" action="<?=base_url('setting/')?>">



				<div class="row">

					<div class="col-md-6">
						<div class="form-group">
							<label for="validity">Select Member</label>
							<select class="form-control" required>
								<option disabled selected>Select Member</option>
								<option value="gp">Grameen</option>
								<option value="blink">Banglalink</option>
								<option value="airtel">Airtel</option>
								<option value="robi">Robi</option>
								<option value="teletalk">Teletalk</option>
								<option value="gpst">GP Skitto</option>
							</select>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label for="exampleInputName1">Transfer Amount</label>
							<input type="number" class="form-control" name="" id="package_amount" placeholder="Transfer Amount" required>
						</div>
					</div>

				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="commission">Mobile No</label>
							<input type="number" class="form-control"  name="commission" id="commission" placeholder="Your Mobile Number" required>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label for="total_commission">Remaining Amount</label>
							<input type="number" class="form-control" name="total_commission" id="remaining_amount" placeholder="Remaining Amount"  readonly >
						</div>
					</div>

				</div>

				<button  class="btn btn-primary mr-2">Send</button>

			</form>
		</div>
	</div>
</div>
