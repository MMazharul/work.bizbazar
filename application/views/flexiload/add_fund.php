<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-primary text-white mr-2">
			<i class="mdi mdi-home"></i>
		</span>
		Payment Process
	</h3>
	<nav aria-label="breadcrumb">
		<ul class="breadcrumb">
			<li class="breadcrumb-item active" aria-current="page">
				<span></span>Steps
				<i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
			</li>
		</ul>
	</nav>
</div>

<div class="row justify-content-center">
	<div class="col-md-12 col-sm-12 text_center align-items-center card m-2 p-2">
<!--		<img src="--><?//=base_url('app-assets/images/bkash/BKash-Logo-English.jpg')?><!--" width="200px" alt="quick-earn.info bkash">-->
		<br>
		<div class="form-group row">
			<label class="col-sm-4 col-form-label">Payment Type</label>
			<div class="col-sm-4">
				<div class="form-check">
					<label class="form-check-label">
						<input type="radio" class="form-check-input" onclick="paymentMethod(this)"  name="bank" id="bank" value="1"/>
						Bank
						<i class="input-helper"></i></label>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-check">
					<label class="form-check-label">
						<input type="radio" class="form-check-input" onclick="paymentMethod(this)" name="bkash" id="bkash" value="0"/>
						Bkash
						<i class="input-helper"></i></label>
				</div>
			</div>
		</div>

		<div id="payment_type">


		</div>
		<br>

<!--		<h4 class="text-center">bKash <u></u> Number : <b> </b></h4>-->
		<small class="text-center p-2">Biz-Bazar এর Flexiload এর সকল পেমেন্ট প্যাকেজ থেকে অর্ডার করে আপনি বিকাশ অথবা Biz-Bazar এর ব্যাংক অ্যাকাউন্ট ডিপোজিট করে ব্যাংক সিলিপ টি আপলোড করবেন।ধন্যবাদ </small>
	</div>

</div>
<input type="hidden" value="<?=$package->id?>" id="package_id" name="package_id"/>

<div class="" id="payment_content">


</div>

<div class="row justify-content-center alert alert-info border10 shadow p-3">
	<div class="col-md-12 col-sm-12 text_center align-items-center card m-2 p-2">

		<br>

		<h4 class="text-center">1. Our Package Amount : <b><?=$package->package_amount?> Tk</b></h4>
		<h4 class="text-center">2. Package Commission : <b><?=$package->total_commission?> Tk</b></h4>
		<h4 class="text-center">3. Total Received Amount : <b><?=$package->total_commission+$package->package_amount?> Tk</b></h4>


	</div>

</div>


<script>

	function paymentMethod(e)
	{

		let classesToAdd = [ 'row', 'justify-content-center', 'alert','alert-info','border10','shadow','p-3'];

		let payment_content = document.getElementById("payment_content");

		payment_content.innerHTML = "";

		payment_content.classList.add(...classesToAdd);

		let paymentType = e.value;

		$.ajax({
			type: "post",
			url: "<?=base_url('/FlexiloadCtrl/payment_type')?>",
			dataType: "json",
			success: function (data) {

				if(paymentType==1)
				{
					document.getElementById("bkash").checked=false;
					e.checked=true;
					document.getElementById("payment_type").innerHTML="<h4 class=\"text-center\">Bank Name : <b>"+data.bank_name+"</b></h4>" +
							"<h4 class='text-center'>Branch Name : <b>"+data.branch_name+"</b></h4>"+
							"<h4 class='text-center'>Account Number : <b>"+data.account_number+"</b></h4>";
					payment_content.innerHTML="<div class=\"form-group col-md-4 col-12 \" id=\"sender_field\">"+
							"<label  for=\"inlineFormInputGroup\">Bank Account Number</label>"+
							"<input type=\"number\" class=\"form-control\" id=\"account_number\" placeholder=\"Account Number\">"+
							"<div id=\"field_error1\"></div></div>"+
							"<div class=\"form-group col-md-4 col-12\" >"+
							"<label  for=\"inlineFormInputGroup\">Bank Diposit Slip</label>"+
							"<input type=\"file\" onchange='uploadImage(this)' accept=\"image/*\" class=\"\" id=\"bank_slip\" style='margin-top: 1px' placeholder=\"Bank Deposit Slip\"><br><div id=\"field_error2\"></div><br><img  id=\"output\" width='200'></div>"+
							"<div class=\"form-group col-md-3 col-12 text-center \" id=\"submit_id\">"+
							"<button onclick=\"fund_req(this)\" type=\"button\" class=\"btn btn-gradient-dark btn-rounded shadow\">Send Request <i class=\"mdi mdi-chevron-right\"></i><br><small>(Proceed Time 24hrs)</small><i style=\"display:none\" class=\"mdi mdi-loading mdi-spin loading\"></i></button>"+
							"<div id=\"activ_rst\"></div>"+
							"</div>";
				}
				else{
					document.getElementById("bank").checked=false;

					e.checked=true;

					document.getElementById("payment_type").innerHTML="<h4 class=\"text-center\">Bkash Type : <b>"+data.bkash_type+"</b></h4>" +
							"<h4 class='text-center'>Bkash Number : <b>"+data.bkash_number+"</b></h4>";
					payment_content.innerHTML="<div class=\"form-group col-md-4 col-12 \" id=\"sender_field\">"+
							"<label  for=\"inlineFormInputGroup\">Sender Number</label>"+
							"<input type=\"number\" class=\"form-control\" id=\"sender_number\" placeholder=\"Sender Number\"  >"+
							"<div id=\"field_error1\"></div></div>"+
							"<div class=\"form-group col-md-4 col-12\" id=\"tran_field\">"+
							"<label  for=\"inlineFormInputGroup\">Sender Transaction ID</label>"+
							"<input type=\"text\" class=\"form-control\" id=\"tran_id\" placeholder=\"Transection ID\" ><div id=\"field_error2\"></div></div>"+
							"<div class=\"form-group col-md-3 col-12 text-center \" id=\"submit_id\">"+
							"<button onclick=\"fund_req(this)\" type=\"button\" class=\"btn btn-gradient-dark btn-rounded shadow\">Send Request <i class=\"mdi mdi-chevron-right\"></i><br><small>(Proceed Time 24hrs)</small><i style=\"display:none\" class=\"mdi mdi-loading mdi-spin loading\"></i></button>"+
							"<div id=\"activ_rst\"></div>"+
							"</div>";
				}
			}
		});

	}


	function fund_req() {

		let id = '<?=$this->session->user_id?>';

		let package_id = document.getElementById("package_id").value;
		let bank = document.getElementById("bank").checked;
		let payment_method = bank === true ? 'Bank' : 'Bkash';
		var formData = new FormData();
		formData.append('user_id',id);
		formData.append('package_id',package_id);
		formData.append('payment_method',payment_method);
		if(bank===true)
		{
			var bank_account = document.getElementById("account_number").value;
			var bank_slip  = $("#bank_slip").prop('files')[0];
			formData.append('bank_slip', bank_slip);
			formData.append('bank_account', bank_account);
		}
		else{
			var sender_number = document.getElementById("sender_number").value;
			var transaction_id = document.getElementById("tran_id").value;
			formData.append('sender_number', sender_number);
			formData.append('transaction_id',transaction_id);
		}

			$.ajax({
				type: "post",
				url: "<?=base_url('FlexiloadCtrl/fund_req_store')?>",
				data:formData,
				contentType : false,
				processData : false,

				dataType: "json",
				success: function (response) {

					if (response.success === true) {
						alert("success");

					}
					else
					{
						let error=response.messages;
						if(bank===true)
						{
							document.getElementById("field_error1").innerHTML=error.bank_account;
							document.getElementById("field_error2").innerHTML=error.bank_slip;
						}
						else{
							document.getElementById("field_error1").innerHTML=error.sender_number;
							document.getElementById("field_error2").innerHTML=error.transaction_id;
						}
					}
				}
			});

	}


	var uploadImage = function(event) {
		var image = document.getElementById('output');
		var file = document.getElementById('bank_slip');
		console.log(file.files[0]);
		image.src = URL.createObjectURL(file.files[0]);

	};

</script>
