<style>
	.row ul li{
		padding: 5px;
		margin: 0px;
	}
	.row ul{
		padding: 0px;
		margin: 0px;
		list-style: none;
	}
	.row ul li{
		margin-left: -67px;
	}
	.card{
		height: 330px;
	}


</style>


<div class="row ">
	<?php foreach ($packages as $package):?>
		<div class="card col-md-3 bg-gradient-dark card-img-holder shadow text-white m-2">
			<div class="card-body">
				<div class="col-md-12 col-sm-12 col-xs-12 ">
					<div class="title">

						<ul>
							<li><strong>BDT </strong><br><span><?=$package->package_amount?> ৳</span></li>
							<li><strong>All Operator </strong><br><span>(Robi,Gp,BL,Airtel,Teletalk)</span></li>
							<li><strong>Vadility : </strong> <?=$package->vadility?></li>
							<li><strong>Commission : </strong> <?=$package->total_commission?> ৳</li>
							<li><strong>Total Amount : </strong> <?=$package->package_amount+$package->total_commission?> ৳</li>
						</ul>
						<a class="btn btn-primary m-2 p-2" href="<?=base_url('FlexiloadCtrl/flexiload_fund_req?id='.$package->id)?>"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Buy Now</a>
					</div><br><br>
				</div>

			</div>
		</div>
	<?php endforeach;?>

</div>

