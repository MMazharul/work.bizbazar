<div >
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">Flexiload</h4>

			<form class="forms-sample" method="post" action="<?=base_url('setting/')?>">

				<div class="row">
					<div class="col-md-6">
						<div class="form-group row">
							<label class="col-sm-3 col-form-label">Number Type</label>
							<div class="col-sm-4">
								<div class="form-check">
									<label class="form-check-label">
										<input type="radio" class="form-check-input" name="status" id="membershipRadios1" value="1">
										Prepaid
										<i class="input-helper"></i></label>
								</div>
							</div>
							<div class="col-sm-5">
								<div class="form-check">
									<label class="form-check-label">
										<input type="radio" class="form-check-input" name="status" id="membershipRadios2" value="0">
										PostPaid
										<i class="input-helper"></i></label>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row">

					<div class="col-md-6">
						<div class="form-group">
							<label for="exampleInputName1">Mobile Number</label>
							<input type="number" class="form-control" name="" id="package_amount" placeholder="01xxxxxxxxx">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="validity">Operator</label>
							<select class="form-control">
								<option disabled selected>Select One</option>
								<option value="gp">Grameen</option>
								<option value="blink">Banglalink</option>
								<option value="airtel">Airtel</option>
								<option value="robi">Robi</option>
								<option value="teletalk">Teletalk</option>
								<option value="gpst">GP Skitto</option>
							</select>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="commission">Amount (Tk)</label>
							<input type="number" class="form-control"  name="commission" id="commission" placeholder="Amount">
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label for="total_commission">Remaining Amount</label>
							<input type="number" class="form-control" name="total_commission" id="remaining_amount" placeholder="Remaining Amount" >
						</div>
					</div>

				</div>
				<div class="row">

					<div class="col-md-6">
						<div class="form-group">
							<label for="total_commission">Remarks</label>
							<input type="number" class="form-control" name="total_commission" id="total_commission" placeholder="Short description" >
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label  for="flexipin">FlexiPin</label>
							<input type="password" id="flexipin" name="flexipin" class="form-control " required placeholder="flexipin">
						</div>
					</div>
				</div>



				<button  class="btn btn-primary mr-2">Submit</button>

			</form>
		</div>
	</div>
</div>
