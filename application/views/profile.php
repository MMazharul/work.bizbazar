<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-primary text-white mr-2">
			<i class="mdi mdi-home"></i>
		</span>
		User Profile
	</h3>
	
</div>


<div class="row">
	<div class="col-12 m-0 p-0">
		<img class="profile_cover shadow" src="<?= base_url('app-assets/images/covers/biz1.jpg') ?>" alt="">
	</div>

	<div class="col-12 d-flex justify-content-start">
		<input type="file" class="d-none" id="pic_input" onchange="document.getElementById('prfl_photo').src = window.URL.createObjectURL(this.files[0])">
		<div id="profile_pic_edit"><button class="btn btn-warning shadow btn-rounded btn-sm">Photo <i class="mdi mdi-image"></i> </button></div>
		<img id="prfl_photo" class="profile_img shadow" src="<?= base_url('app-assets/images/members/' . $user_data->picture) ?>" alt="">

		<?php if (!isset($_GET['user_id'])) : ?>
			<div id="profile_edit"><button onclick="edit_profile()" class="btn btn-outline-info shadow btn-rounded btn-sm ml-5">Edit <i class="mdi mdi-pencil"></i> </button></div>
		<?php endif ?>
		<div id="edit_close"><button onclick="edit_close()" class="btn btn-outline-danger shadow btn-rounded btn-sm ml-5"><i class="mdi mdi-close"></i> </button></div>
		<div id="save"><button onclick="save_prfile()" class="btn btn-success shadow btn-rounded btn-sm ml-5">Save <i class="mdi mdi-content-save"></i><i class="mdi mdi-spin mdi-reload loading"></i> </button></div>
		<div id="save"></div>
	</div>

	<div class="col-12 d-flex justify-content-start mt-3 ml-2">
		<h3 id="name"><?= $user_data->name ?></h3>
	</div>
	<?php if ($user_data->active_status == 1) : ?>
		<div class="col-12 d-flex justify-content-center">
			<small class="badge badge-info"><?= $user_data->total_refered ?> Refer</small>
		</div>

	<?php endif ?>

</div>

<div class="row">
	<div class="col-12 card shadow border10 p-3 mb-2 mt-2">
		<h5 class="text-gray">Freelance Worker Profile</h5>
		<small class="text-gray">Title</small>
		<h4><span id="title"><?= $profile == null ? 'Title Here' : $profile->title ?></span></h4>
		<small class="text-gray">Skills</small>
		<div id="skill-input" contenteditable="true" class="editable" style="display:none"><?= $profile == null ? 'Edit profile and add Skills' : $profile->skill ?></div>
		<div id="skills" class="mb-2 mt-2"></div>
		<small class="text-gray">Description</small>
		<div id="desc"><?= $profile == null ? 'Write Short Description about you' : $profile->description ?></div>
		<small class="text-gray mt-3">Work Example</small>
		<br>
		<div>
			<button class="btn btn-default p-1 d-flex align-items-center border10" data-toggle="modal" data-target="#add-work"><i class="mdi mdi-plus-circle mdi-36px text-gray"></i>Add Work Example</button>
		</div>
		<!-- Modal -->
		<div class="modal fade" id="add-work" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content bg-white border20">
					<form method="post" action="<?=base_url('/ProfileCtrl/add_portfolio')?>">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Add Previous Work Example</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label class="ml-2" for="title">Work Title</label>
								<input name="title" type="text" class="form-control" id="title" placeholder="Work Title">
							</div>
							<div class="form-group">
								<label class="ml-2" for="title">Description</label>
								<textarea class="form-control" name="desc" id="" cols="30" rows="5"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary border20" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary border20">Save</button>
						</div>
					</form>
				</div>
			</div>
		</div>

	</div>
</div>

<div class="row justify-content-center bg-white border10 shadow mt-3">

	<div class="col-md-3 m-2">
		<div class="bg-white border10 p-2 ">
			<div class="m-2"><i class="mdi mdi-rotate-3d"></i><b> Account Type : </b>
				<?php if ($user_data->active_status == 0) {
					echo '<span class="badge badge-pill badge-gradient-info mt-1">Free Acc</span>';
				} elseif ($user_data->active_status == 2) {
					echo '<span class="badge badge-pill badge-gradient-warning mt-1">Pending</span>';
				} elseif ($user_data->active_status == 3) {
					echo '<span class="badge badge-pill badge-gradient-danger mt-1">Blocked</span>';
				} else {
					echo '<span class="badge badge-pill badge-gradient-success mt-1">Active</span>';
				} ?>
			</div>
			<?php if ($user_data->active_status == 1 && !isset($_GET['user_id'])) : ?>
				<div class="m-2"><i class="mdi mdi-share"></i><b>Affiliate Refer ID : </b><span class="badge badge-pill bg-warning"><?= $user_data->own_refer_id ?></span></div>
				<div class="m-2"><i class="mdi mdi-share-variant"></i><b>Affiliate Refer Link : </b>
					<br>
					<div id="refer_link" class="bg-light p-2 border10"><small id="refer_link_copy" class="text-gray float-right m-1">copy</small><a href="https://biz-bazar.com/referer/<?= $user_data->own_refer_id ?>" target="_blank"><span id="r_link">https://biz-bazar.com/referer/<?= $user_data->own_refer_id ?></span></a></div>
				</div>
			<?php endif ?>

		</div>

		<?php if (!isset($_GET['user_id'])) : ?>
			<div class="card mt-3 border10 shadow-sm">
				<a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
					<div class="card-header bg-white border10">
						<i class="mdi mdi-lock-outline"></i> Change Password <i class="mdi mdi-chevron-down"></i>
					</div>
				</a>
				<div id="collapseTwo" class="collapse" data-parent="#accordion">
					<div class="card-body p-2">

						<div class="form-group text-center">
							<label for="old_pass"><b>Old Password</b></label>
							<input type="password" class="form-control" id="old_pass" placeholder="Old Password">
						</div>

						<div class="form-group text-center">
							<label for="pass"><b>New Password</b></label>
							<input type="password" class="form-control" id="pass" placeholder="New Password">
						</div>

						<div class="form-group text-center">
							<label for="c_pass"><b>Confirm New Password</b></label>
							<input type="password" class="form-control" id="c_pass" placeholder="Confirm New Password">
						</div>

						<div class="form-group text-center">
							<button onclick="change_pass()" class="btn btn-sm btn-info btn-rounded">Save</button>
						</div>
						<div class="text-center m-0 p-0" id="response"></div>
					</div>
				</div>
			</div>
		<?php endif ?>

	</div>

	<div class="col-md-4 m-2">
		<div class="bg-white border10 p-2">

			<div class="m-2"><i class="mdi mdi-cellphone-iphone"></i><b> Mobile : </b><span id="mob">0<?= $user_data->mobile_num ?></span></div>

			<div class="m-2"><i class="mdi mdi-gender-male-female"></i><b> Gender : </b>
				<select id="gender" class="select" disabled>
					<option <?= $user_data->gender == 1 ? "selected" : null ?> value="1" id="">Male</option>
					<option <?= $user_data->gender == 0 ? "selected" : null ?> value="0" id="">Female</option>
				</select>
			</div>

			<div class="m-2"><i class="mdi mdi-email-outline"></i><b> Email : </b><span id="email"><?= $user_data->email ?></span></div>

			<?php if (!isset($_GET['user_id'])) : ?>
				<div class="m-2"><i class="mdi mdi-map-marker-circle"></i><b> Adress : </b><span id="adress"><?= $user_data->adress ?></span></div>

				<div class="m-2"><i class="mdi mdi-credit-card"></i><b> National ID : </b><span id="nid"><?= $user_data->national_id ?></span></div>
			<?php endif ?>

			<div class="m-2"><i class="mdi mdi-calendar-today"></i><b> Joined In : </b><?= nice_date($user_data->acc_active_date, "d-M-Y") ?></div>
		</div>
	</div>

	<div class="col-md-4 m-2">
		<div class="bg-white border10 p-2">
			<div class="m-2"><i class="mdi mdi-coin"></i><b> Balance <i class="mdi mdi-chevron-down"></i> </b></div>

			
			<?php /*  <div class="m-2"><i class="mdi mdi-worker"></i><b> Job Earned : </b><?= number_format($user_data->work_earn_amount, 2) ?></div> */ ?>

			<?php /* <div class="m-2"><i class="mdi mdi-wifi"></i><b> Refer Commission Earned : </b><?= number_format($user_data->refer_work_earn_amount, 2) ?></div> */ ?>

			<div class="m-2"><i class="mdi  mdi-lightbulb-outline"></i><b> Refer Bonus Earned : </b><?= $user_data->refer_earn_amount ?></div>

			<div class="m-2"><i class="mdi mdi-cash-100"></i><b> Affiliate Sell Commission : </b><?= number_format($user_data->other_earn_amount, 2) ?></div>

			<div class="m-2"><i class="mdi mdi-cash-multiple"></i><b> Total Balance : </b><?= number_format($user_data->total_bal, 2) ?></div>

		</div>

	</div>
</div>

<script>
	'use strict';
	document.getElementById('skill-input').innerHTML.split(',').map(itm => {
		document.getElementById('skills').innerHTML += `<span class="badge badge-info badge-pill ml-2">${itm}</span>`
	});

	function edit_profile() {
		$("#name,#email,#adress,#nid,#title,#desc").attr("contentEditable", true).addClass("editable");
		$("#gender").attr("disabled", false);
		$("#profile_pic_edit,#save,#skill-input").css("display", "block");
		$("#profile_edit,#skills").css("display", "none");
		$("#edit_close").css("display", "block");
	}

	function edit_close() {
		$("#name,#mob,#email,#adress,#nid,#title,#desc").attr("contentEditable", false).removeClass("editable");
		$("#gender").attr("disabled", true);
		$("#profile_pic_edit,#save,#skill-input").css("display", "none");
		$("#profile_edit,#skills").css("display", "block");
		$("#edit_close").css("display", "none");
		document.getElementById('skills').innerHTML = '';
		document.getElementById('skill-input').innerHTML.split(',').map(itm => {
			document.getElementById('skills').innerHTML += `<span class="badge badge-info badge-pill ml-2">${itm}</span>`
		});

	}

	$('#profile_pic_edit').click(function() {
		$('#pic_input').trigger('click');
	});


	$("#pic_input").on("change", function() {
		var file = this.files[0];
		console.log(file.size);

	});

	function save_prfile() {
		let formData = new FormData();
		let img = document.getElementById("pic_input").files[0];

		let name = $("#name").html();
		let mob = $("#mob").html();
		let gender = $("#gender").val();
		let email = $("#email").html();
		let adress = $("#adress").html();
		let nid = $("#nid").html();

		let title = $("#title").html();
		let skills = $("#skill-input").html();
		let desc = $("#desc").html();

		formData.append("photo", img);
		formData.append("name", name);
		formData.append("mob", mob);
		formData.append("gender", gender);
		formData.append("email", email);
		formData.append("adress", adress);
		formData.append("nid", nid);
		formData.append("title", title);
		formData.append("skills", skills);
		formData.append("desc", desc);

		$.ajax({
			type: "post",
			url: "https://" + window.location.hostname + "/ProfileCtrl/profile_edit",
			data: formData,
			processData: false,
			contentType: false,
			success: function(response) {
				$("#name,#mob,#email,#adress,#nid,#title,#desc").attr("contentEditable", false).removeClass("editable");
				$("#gender").attr("disabled", true);
				$("#profile_pic_edit,#save,#skill-input").css("display", "none");
				$("#profile_edit,#skills").css("display", "block");
				$("#edit_close").css("display", "none");
			}
		});

	}

	$("#refer_link_copy").on("click", () => {
		var range = document.getSelection().getRangeAt(0);
		range.selectNode(document.getElementById("r_link"));
		window.getSelection().addRange(range);
		document.execCommand("copy");
		$("#refer_link_copy").html("copied");

		setTimeout(() => {
			$("#refer_link_copy").html("copy");
		}, 2000);
	});


	function change_pass() {
		let old_pass = $("#old_pass").val();
		let pass = $("#pass").val();
		let c_pass = $('#c_pass').val();

		let data = {
			old_pass: old_pass,
			pass: pass,
			c_pass: c_pass
		};

		if (!old_pass == "" && !pass == "" && !c_pass == "") {

			if (pass == c_pass) {
				$.ajax({
					type: "post",
					url: location.protocol + "//" + window.location.hostname + "/ProfileCtrl/change_pass",
					data: data,
					dataType: "json",
					success: function(response) {
						if (response.hasOwnProperty('success')) {
							$("#old_pass").val("");
							$("#pass").val("");
							$('#c_pass').val("");
							setTimeout(() => {
								$('.collapse').collapse('hide');
							}, 1000);

						}
						$("#response").html(response.response);
						setTimeout(() => {
							$("#response").html('');
						}, 2000);
					}
				});
			} else {
				$("#response").html('<span class="p-2 border10 bg-danger">Password Not Matched</span>');
				setTimeout(() => {
					$("#response").html('');
				}, 2000);
			}
		}
	}
</script>