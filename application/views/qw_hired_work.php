<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-info text-white mr-2">
			<i class="mdi mdi-briefcase-outline mdi-24px"></i>
		</span>
		Hired Works
	</h3>
</div>


<div class="row justify-content-center">
	<div class="col-12 col-md-12 p-0">

		<?php foreach ($hired_works as $hire) : ?>
			<div class="card border20 bg-white shadow p-3 mb-3">
				<div>
					<h2><a href="/BuyerWorkCtrl/contact_view?i=<?= base64_encode($hire->id) ?>" class="text-dark"><?= $hire->title ?></a></h2>
					<small>-<?= nice_date($hire->created_at,'m-d-y')?></small><br>
					<b class="text-info"><span>Budget:</span> ৳ <?= $hire->budget ?></b>
					<br>
					<b class="text-blue"><span>Bid Rate:</span> ৳ <?= $hire->amount ?></b>
					<br>
					<p>
						<?php if($hire->status==0): ?>
						<u>Contract Running...</u>
						<?php endif ?>
					</p>

				</div>
			</div>
		<?php endforeach ?>

	</div>
</div>