<?php 
$data = get_user_data($this->session->user_id);
if($data->active_status == 2){?>
<div class="row">
	<div class="col-12 bg-gradient-success justify-content-center text-center text-white border10 shadow p-5 m-2">
		<div class="row">
			<div class="col-12 col-md-8 col-sm-12 text-center">
			<h4 >Dear User,<br>Your Activation Request has been sent, Account will activate soon <br> Please Wait for Account Activation. <br>Thank You.</h4>
			<b><a href="<?=base_url("SupportCtrl")?>">Have any Questions ? Ask Here<i class="mdi mdi-link"></i> </a></b>
			</div>
			<div class="col-md-4 col-sm-12 align-self-center justify-content-center">
				<i class="mdi mdi-autorenew mdi-spin mdi-48px text-white"></i>
			</div>
		</div>
		
	</div>
</div>
<?php } else { ?>
	<div class="row">
	<div class="col-12 alert alert-warning justify-content-center text-center border10 shadow p-4">
		<h4>Upgrade Membership and enjoy refar bonus</h4>
			<a href="<?= base_url() ?>/ActivateCtrl" class="btn btn-warning btn-rounded shadow">Go Premium Now <i class="mdi mdi-checkbox-marked-circle"></i></a>
		<br>
		<br>
		
		<b><a href="<?=base_url("SupportCtrl")?>">Have any Questions ? Ask Here<i class="mdi mdi-link"></i> </a></b>
		<br>
		<p></p>
	</div>
</div>
<?php } ?>