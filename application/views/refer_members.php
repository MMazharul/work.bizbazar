
<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-primary text-white mr-2">
			<i class="mdi mdi-share-variant"></i>
		</span>
		Refer Members
	</h3>
</div>
 
<div class="row">
	<div class="col-md-12 grid-margin p-0 ">
		<div class="card border10 ">
			<div class="card-body p-3">
				<h4 class="card-title">All Members You Refered</h4>
				<div class="table-responsive">
					<table class="table display" id="datatable">
						<thead>
							<tr>
								<th style="width:1px">
									#
								</th>
								<th style="min-width:115px">
									Members
								</th>
								
								<th>
									Rank
								</th>
								<th style="width:80px">
									Your<br>Percentage
								</th>
								<th>
									Status
								</th>
								<th>
									Join in
								</th>
							</tr>
						</thead>
						<tbody>
						<?php $serial=1; foreach ($referMembers as $member):?>
						<tr>
						<td><?=$serial++?></td>
						<td><a href="<?=base_url("ProfileCtrl/profile_view?user_id=".$member->id)?>"><img class="user_pic_table" src="<?= base_url().'app-assets/images/members/'; echo $member->picture === NULL ? 'avater_6.png' : $member->picture ?>" alt="member-img">  &nbsp &nbsp<?= $member->name ?></a></td>
						<td><?= $member->reputation_milestone_name ?> &nbsp &nbsp <img class="rank_pic_table" src="<?= base_url()."app-assets/images/level/". $member->reputation_milestone_icon ?>" alt="rank-img"></td>
						<td><?=$member->referer_percentage?>%</td>
						<td><span class='badge badge-pill <?php if($member->active_status == "Inactive"){echo "badge-gradient-danger";} elseif($member->active_status == "Pending"){echo "badge-gradient-warning";} else{echo "badge-gradient-success";}; ?>'><?=$member->active_status?></span></td>
						<td><?=nice_date($member->acc_active_date,"d-M-y")?></td>
						</tr>
						<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-danger text-white mr-2">
			<i class="mdi mdi-share-variant"></i>
		</span>
		Inactive Refer Members
	</h3>
</div>

<div class="col-12 grid-margin p-0 m-0">
		<div class="card border10 p-0 m-0">
			<div class="card-body p-3 m-0">
				<h4 class="card-title">Inactive Refered Members <i class="mdi mdi-account-multiple "></i> </h4>
				<div class="table-responsive">
					<table id="datatable" class="table display dataTable table-hover">
						<thead>
							<tr>
								<th>#</th>
								<th>
									Member
								</th>
								<th>Status</th>
								<th>
									Join in
								</th>
							</tr>
						</thead>
						<tbody>
						<?php $serial=1;foreach($inctive_referMembers as $member):?>
						<tr>
							<td><?=$serial++?></td>
							<td><a href="<?=base_url("ProfileCtrl/profile_view?user_id=".$member->id)?>"><img src="<?=base_url("app-assets/images/members/".$member->picture)?>" alt=""> <?= $member->name ?></a> </td>
							<td class="d-flex align-items-center">
							<span class="badge badge-pill <?php if($member->status=='Active'){echo "badge-success";}else if($member->status=='Pending'){echo "badge-warning";}else{echo "badge-danger";} ?>"><?=$member->status?></span> &nbsp &nbsp <button onclick="resend_cr(this)" id="<?=$member->id?>" class="btn btn-success btn-rounded btn-sm -sm ">Resend Credetials <i class="mdi mdi-sync mdi-spin loading"></i> </button>
							<a href="<?=base_url("ReferMembersCtrl/remove_member?user_id=".$member->id)?>" onclick="return confirm('Are You Sure to DELETE this Members Account ?')" class="ml-5 btn-icon-append -sm btn btn-sm btn-rounded btn-danger float-right">Remove This Account<i class="mdi mdi-delete-forever"></i></a>
							</td>
							<td><?=nice_date($member->acc_active_date,"d-M-y")?></td>
						</tr>
						<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<script>
	function resend_cr(e) {
		if(confirm('Resend Users data in his mobile ?')){
		let id = e.id;
		$.ajax({
			type: "post",
			url: "<?=base_url('ActivateCtrl/resend_credential')?>",
			data: {id:id},
			dataType: "json",
			success: function (response) {
				if(response.success == 1){
					document.getElementById(e.id).innerHTML += "<i class='mdi mdi-check'></i>";
					$(".loading").hide();
				}
			}
		});
		}
	}
	</script>