<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-primary text-white mr-2">
			<i class="mdi mdi-worker"></i>
		</span>
		Jobs
	</h3>
	<nav aria-label="breadcrumb">
		<ul class="breadcrumb">
			<li class="breadcrumb-item active" aria-current="page">
				<span></span>Overview
				<i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
			</li>
		</ul>
	</nav>
</div>

<div class="row">
	<div class="col-md-12 grid-margin stretch-card shadow p-0 m-0">

		<div class="card border10">
			<div class="card-body p-3">
				<?php if (!$job_limit == null && $job_allow->job_allow) { ?>
					<tr>
					<td><b class="text-danger">প্রিয় গ্রাহক, যারা মাত্র ৩ টি ব্লগ লিঙ্ক পাচ্ছেন তারা এখুনি ১ টি রেফার করলে পাবেন ৮ টি ব্লগ লিঙ্ক, এবং মিনিমাম ১ টি রেফার করলে সবসময়ের জন্য পাবেন ১০ টি ব্লগ লিঙ্ক </b></td>
					<td></td>
					</tr>
				<?php } ?>
				<!--<h4 class="card-title">Available Jobs (Per Job <span class="text-info">৳ <?= "" /*$job_click_amnt[0]->earn_per_link*/?>/=</span> BDT)</h4>-->

				<div class="table-responsive">
					<table class="table display" id="datatable">
						<br>
						কিভাবে জবের কাজ করতে হবে তার ভিডিও টিউটোরিয়াল দেখতে <a href="https://www.youtube.com/watch?v=iJuvVFaQnTM&feature=youtu.be" target="_blank"><b>এখানে ক্লিক করুন</b></a>
						<br>
						<br>
						<thead>
							<tr>
								<th style="width:200px">
									Jobs ( Time :<span id="j_time"></span>)
								</th>
								<th>

								</th>
							</tr>
						</thead>
						<tbody>
							<?php if ($job_allow->job_allow) {
								$serial = 1;
								foreach ($jobs as $job) { ?>
									<tr>
										<td id="<?= $job->id ?>" disabled="disabled" onclick="link_opener(this)" data-link="<?= $job->link ?>" style="cursor: pointer;"><b>Blog Article <?= $serial++ ?></b></td>
										<td></td>
									</tr>
								<?php }
						} ?>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>


<script>
	let referer_id = '<?= $_SESSION['referer_id'] ?>';
	let job_earn = '<?= $_SESSION['job_earn'] ?>';
	let referer_earn = '<?= $_SESSION['referer_earn'] ?>';
</script>


<script src="<?= base_url() ?>app-assets/js/ajax/task.js"></script>

<?php if ($job_allow->job_allow) : ?>
	<script>
		let session_time = "<?= nice_date($job_allow->job_allow_time, 'M d, Y H:i:s') ?>";

		var countDownDate = new Date(session_time).getTime();

		var x = setInterval(function() {

			var now = new Date().getTime();

			var distance = countDownDate - now;

			var days = Math.floor(distance / (1000 * 60 * 60 * 24));
			var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
			var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
			var seconds = Math.floor((distance % (1000 * 60)) / 1000);

			document.getElementById("j_time").innerHTML = hours + "h " +
				minutes + "m " + seconds + "s ";

			if (distance < 0) {
				clearInterval(x);
				document.getElementById("j_time").innerHTML = " Over";
			}
		}, 1000);
	</script>

<?php endif ?>

<!-- Global site tag (gtag.js) - Google Analytics quick earn job-->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-133743383-1"></script>
<script>
	window.dataLayer = window.dataLayer || [];

	function gtag() {
		dataLayer.push(arguments);
	}
	gtag('js', new Date());

	gtag('config', 'UA-133743383-1');
</script>