<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-info text-white mr-2">
			<i class="mdi mdi-briefcase-outline mdi-24px"></i>
		</span>
		Contact Details
	</h3>
</div>

<div class="row justify-content-center">
	<div class="col-12 col-md-12 p-0">
		<div class="card bg-white shadow p-3 mb-3 border10">
			<div>
				<h4><?= $job[0]->title ?></h4>
				<small>Posted: <?= nice_date($job[0]->created_at, 'Y-m-d') ?></small>
				<br>
				<small>Expire: <?= $job[0]->exp_date == null ? 'Not Assaign': nice_date($job[0]->exp_date, 'Y-m-d') ?></small>
				<br>
				<br>
				<div>Category - <b><?= $job[0]->cat_name ?></b></div>
				<br>
				<div>Required Skills :
					<?php $skills = explode(",", $job[0]->skills);
					foreach ($skills as $skill) { ?>
						<span class="badge badge-info badge-pill"><?= $skill ?></span>
					<?php } ?>
				</div>
				<br>
				<div>Budget - <b><?= $job[0]->budget ?> TK</b></div>
				<div>Your Bid - <b><?= $hired->amount ?> TK</b></div>

				<div class="collapse" id="collapse-detail">
					<br>
					<b>Brief:</b>
					<p><?= $job[0]->job_desc ?></p>

					<br>
					<?php if ($job[0]->files != null && $job[0]->files != "") :
						$files = explode(",", $job[0]->files); ?>
						<b>Additional Attached Files:</b>
						<br>
						<ul>
							<?php foreach ($files as $file) : ?>
								<li><a href="https://q-worker.quick-earn.info/files/<?= $file ?>" target="_blank"><u><?= $file ?></u></a><br></li>
							<?php endforeach;
					endif ?>
					</ul>
				</div>

			</div>

			<div class="text-primary mt-3" style="cursor:pointer" data-toggle="collapse" data-target="#collapse-detail" aria-expanded="false" aria-controls="collapse-detail"><b>See More...</b></div>
			<br>
			<div>Payment Due : <?= $hired->amount-$hired->paid ?></div>
			<br>
			<h4>Contact Conversation</h4>
			<hr class="mt-0">
			<div id="chats-thread" class="card bg-light p-3 border-0 border20">
				<div class="row">
					<div class="col-12 col-md-6">
						<div class="d-flex align-items-center">
							<img class="bid-profile-img" src="https://<?= $_SERVER['HTTP_HOST'] ?>/app-assets/images/members/<?= $_SESSION['picture'] ?>" alt="profile image">
							<input id="mem_picture" type="hidden" value="<?= $_SESSION['picture'] ?>">
							<div class="d-flex" style="flex-direction:column">
								&nbsp; You
								<small class="text-gray ">&nbsp; Worker</small>
							</div>
						</div>
						<div class="bg-white p-2 mt-2" id="chat" contenteditable="true"></div>
						<input id="hired_id" type="hidden" value="<?= $hired->id ?>">
						<button id="send-btn" class="btn btn-info btn-sm btn-rounded float-right shadow-none">Send <i class="mdi mdi-send"></i></button>
					</div>
				</div>

				<div id="recent_chat"></div>

				<?php foreach ($chats as $chat) :
					if ($chat->clients_id == null) : ?>
						<div class="row mt-2">
							<div class="col-12 col-md-6">
								<div class="d-flex align-items-center justify-content-between">
									<div class="d-flex align-items-center">
										<img class="bid-profile-img" src="https://<?= $_SERVER['HTTP_HOST'] ?>/app-assets/images/members/<?= $_SESSION['picture'] ?>" alt="profile image">
										<div class="d-flex" style="flex-direction:column">
											&nbsp; You
											<small class="text-gray ">&nbsp; Worker</small>
										</div>
									</div>
									<div><small class="text-gray"><?= nice_date($job[0]->created_at, 'h:i d-M') ?></small></div>
								</div>
								<div class="border20 bg-white p-2 mt-2"><?= $chat->chat ?></div>
							</div>
						</div>
					<?php else : ?>
						<div class="row justify-content-end mt-2">
							<div class="col-12 col-md-6">
								<div class="d-flex align-items-center justify-content-between">
									<div><small class="text-gray"><?= nice_date($job[0]->created_at, 'h:i d-M') ?></small></div>
									<div class="d-flex align-items-center">
										<div class="d-flex text-right" style="flex-direction:column">
											<?= $hired->c_name ?> &nbsp;
											<small class="text-gray ">Client &nbsp;</small>
										</div>
										<img class="bid-profile-img" src="https://<?= $_SERVER['HTTP_HOST'] ?>/app-assets/images/members/avater_2.png" alt="profile image">
									</div>
								</div>
								<div class="border20 bg-white p-2 text-right mt-2"><?= $chat->chat ?></div>
							</div>
						</div>
					<?php endif;
			endforeach ?>

			</div>
		</div>

	</div>
</div>


<script>
	function edit_bid() {
		$('#bid_amnt,#bid_cv').removeAttr('readonly');
		$('#save_btn,#cancel_btn').removeClass("d-none");
	}

	function cancel_() {
		$('#bid_amnt,#bid_cv').attr('readonly', true);
		$('#save_btn,#cancel_btn').addClass("d-none");
	}
</script>
<script type="text/javascript" src="<?= base_url() ?>/app-assets/js/ajax/contact.js"></script>