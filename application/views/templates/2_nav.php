<!-- user data get -->
<?php $user_data = get_user_data($this->session->user_id);
	  $notifications = notifications($this->session->user_id);
	  $notify_count = notify_count($this->session->user_id) ?>

<nav class="navbar nav-bar-gr default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
	<div class="text-center bg-transparent navbar-brand-wrapper d-flex align-items-center justify-content-center">
		<a class="navbar-brand brand-logo" href="<?=base_url("/")?>"><img id="icon" src="<?= base_url() ?>app-assets/images/biz-bazar.png" alt="logo" /> <img id="text" src="<?= base_url() ?>app-assets/images/biz-bazar-logo.png" alt="logo" /></a>
		<a class="navbar-brand brand-logo-mini" href="<?=base_url("/")?>"><img src="<?= base_url() ?>app-assets/images/biz-bazar.png"
			 alt="logo" /></a>
	</div>
	<div class="navbar-menu-wrapper d-flex align-items-stretch">
		<!-- <div class="search-field d-none d-md-block">
			<form class="d-flex align-items-center h-100" action="#">
				<div class="input-group">
					<div class="input-group-prepend bg-transparent">
						<i class="input-group-text border-0 mdi mdi-magnify"></i>
					</div>
					<input type="text" class="form-control bg-transparent border-0" placeholder="Search projects">
				</div>
			</form>
		</div> -->
		<ul class="navbar-nav navbar-nav-right">
			
			<li class="nav-item d-none d-lg-block full-screen-link">
				<input type="search" class="form-control border-0" placeholder="Search...">
			</li>
			
			
			<li class="nav-item dropdown">
				<a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
					<b><i class="fa fa-bell-o" style="color:black;font-weight:700"></i></b>
					<?=$notify_count > 0 ? '<span class="count-symbol bg-danger"></span>' : "" ?>
					
				</a>
				<div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
					<h6 class="p-3 mb-0">Notifications<small class="float-right"><a onclick="all_notify_read()" href="#" class=""><i class="mdi mdi-eye-outline "></i> mark all as read</a></small></h6>
					
					<div class="dropdown-divider"></div>
					<?php foreach($notifications as $notify):?>
					<a href="<?=base_url().$notify->link.'?notify_id='.$notify->id?>" class="dropdown-item preview-item <?=$notify->viewed == 0 ? "alert-info" : ""?>">
						<div class="preview-thumbnail">
								<?=$notify->icon?>
						</div>
						<div class="preview-item-content d-flex align-items-start flex-column justify-content-center">
							<div style="width:100%"><h6 class="preview-subject font-weight-normal mb-1"><?=$notify->title?>&nbsp<small class="float-right text-secondary"><time datetime="<?=$notify->created_at?>"><?=time_ago($notify->created_at)?></time></small></h6></div>
							<p class="text-gray ellipsis mb-0">
								<?=$notify->notification_txt?>
							</p>
						</div>
					</a>
					<div class="dropdown-divider"></div>
					<?php endforeach ?>
					
					<a href="<?=base_url("NotifyCtrl")?>"><h6 class="p-3 mb-0 text-center">See all notifications</h6></a>
				</div>
			</li>


			<li class="nav-item nav-profile dropdown">
				<a class="nav-link dropdown-toggle" id="profileDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
					<div class="nav-profile-img">
						<img src="<?= base_url() ?>app-assets/images/members/<?=$user_data->picture?>" alt="image">
					</div>
					<div class="nav-profile-text">
						<p class="mb-1  text-dark"><b><?=$user_data->name?></b></p>
					</div>
				</a>
				<div class="dropdown-menu navbar-dropdown" aria-labelledby="profileDropdown">
					<a class="dropdown-item" href="<?=base_url('ProfileCtrl')?>">
						<i class="fa fa-user mr-2"></i>
						Profile
					</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="<?=base_url()?>LoginRegCtrl/logout">
						<i class="fa fa-sign-out mr-2 "></i>
						Logout
					</a>
				</div>
			</li>
			
		</ul>
		<button class="navbar-toggler text-darker  navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
			<span class="mdi mdi-menu"></span>
		</button>
	</div>
</nav>


<div class="container-fluid page-body-wrapper">
