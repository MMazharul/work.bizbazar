
<!DOCTYPE html>

<html lang="en">

	<head>

		<!-- Required meta tags -->

		<meta charset="utf-8">

		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<title>Biz Bazar | E-Commerce | Best Online Shopping in Bangladesh</title>

		<meta name="description"
		content="Discover the latest trend of online shopping in Bangladesh. Here you can buy T-shirt, Polo shirt, Pants, Watches, Salwar kameez, Sarees, Jewelry, Bags and so forth." />
		<meta name="keywords"
		content="Online Shopping in Bangladesh, Online Shopping bd, Bangladesh Online Shopping, Online Shop bd, Online Shop in Bangladesh, Online Shopping in Chittagong" />
		<meta property="og:title" content="Simple & Hassle-free Online Shopping in Bangladesh" />
		<meta property="og:description"
		content="Discover the latest trend of online shopping in Bangladesh. Here you can buy T-shirt, Polo shirt, Pants, Watches, Salwar kameez, Sarees, Jewelry, Bags and so forth." />

		<meta name="author" content="Mohd-Arif-Uddin">
		<meta name="theme-color" content="#ffe000">
		<!-- plugins:css -->

		<link rel="stylesheet" href="<?= base_url() ?>app-assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
		<link rel="stylesheet" href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome-font-awesome.min.css">

		<link rel="stylesheet" href="<?= base_url() ?>app-assets/vendors/css/vendor.bundle.base.css">

		<!-- endinject -->

		<!-- inject:css -->

		<link rel="stylesheet" href="<?= base_url() ?>app-assets/css/style.css">

		<link rel="stylesheet" href="<?= base_url() ?>app-assets/css/custom.css">

		<link rel="stylesheet" href="<?= base_url() ?>app-assets/css/datatable.css">

		<script src="<?= base_url() ?>app-assets/js/jquery.3.3.1.js"></script>

		<!-- endinject -->

		<link rel="shortcut icon"  type="image/png" href="<?= base_url() ?>app-assets/images/favicon.png" />

	</head>





	<body>

		<div class="container-scroller">

