  <script>

  const user_id = "<?= $this->session->user_id ?>";


  $("form").submit(function() {
		$(this).submit(function() {
			return false;
		});
		return true;
	});

  var $loading = $('.loading');
    $loading.hide();
    $(document).ajaxStart(function () {
        $loading.show();
        $("button,a").attr("disabled",true).css('pointer-events','none');
    	})
    	.ajaxStop(function () {
        $loading.hide();
        $("button,a").attr("disabled",false).css('pointer-events','auto');
    	});
  </script>

  <script src="<?= base_url() ?>app-assets/vendors/js/vendor.bundle.base.js"></script>

  <script src="<?= base_url() ?>app-assets/vendors/js/vendor.bundle.addons.js"></script>
  <script src="<?= base_url() ?>app-assets/js/off-canvas.js"></script>

  <script src="<?= base_url() ?>app-assets/js/misc.js"></script>

  <script src="<?= base_url() ?>app-assets/js/dashboard.js"></script>

  <script src="<?= base_url() ?>app-assets/js/ajax/login.js"></script>

  <script src="<?= base_url() ?>app-assets/js/ajax/activation.js"></script>

  <script src="<?= base_url() ?>app-assets/js/datatable.js"></script>

<script type="text/javascript">

    $(document).ready(function() {

    $('#datatable,dataTable').DataTable();

    } );

    function all_notify_read() {
      if(confirm("Mark All Notification as Read ?")){
        $.ajax({
          type: "post",
          url: "<?=base_url('NotifyCtrl/markall_notify_read')?>",
          data: "data",
          dataType: "json",
          success: function (response) {
            if(response.success==1){
                $(".dropdown-item").removeClass('alert-info');
                $(".count-symbol").hide();
            }
          }
        });
      }
    }

    
</script> 

  <!-- End custom js for this page-->

<!-- This Project is Developed 
Developer : mohd-arif-uddin (individually)
release date : 1-1-2019
 -->
</body>



</html>