<!-- user data get -->
<?php $user_data = get_user_data($this->session->user_id) ?>


<nav class="sidebar sidebar-offcanvas" id="sidebar">
	<ul class="nav">
		<li class="nav-item nav-profile">
			<a href="#" class="nav-link">
				<div class="nav-profile-image">
					<img src="<?= base_url() ?>app-assets/images/members/<?=$user_data->picture?>" alt="profile">
					<span class="login-status online"></span>
					<!--change to offline or busy as needed-->
				</div>
				
				<div class="nav-profile-text d-flex flex-column">
					<span class="font-weight-bold mb-2"><?=$user_data->name?></span>
					<?php if($user_data->active_status == 0){
						echo '<div class="badge badge-pill badge-gradient-danger mt-1">Inactive</div>';
					 } elseif ($user_data->active_status == 2) {
						echo '<div class="badge badge-pill badge-gradient-warning mt-1">Pending</div>';
					} else{?>
					<span class="text-gray text-small"><?=$user_data->reputation_milestone_name?></span>
					<span class="text-gray text-small">Refer ID : <?=$user_data->own_refer_id?></span>
					<span class="text-gray text-small">Balance : <?=number_format($user_data->balance,2)?></span>
					<?php }?>	
				</div>
				
				
				<?php if($user_data->active_status == 1){ ?>
					<div class="d-flex flex-column align-items-center">
							<img class="ml-2" src="<?=base_url()?>/app-assets/images/level/<?=$user_data->reputation_icon?>" width="30px" alt="">
							<img class="ml-2 mt-2" src="<?=base_url()?>/app-assets/images/level/<?=$user_data->milestone_icon?>" height="18px" alt="">
						</div>
						<?php } ?>
						
					</a>
		</li>
		<li class="nav-item mt-2">
			<a class="nav-link" href="<?=base_url()?>DashboardCtrl">
				<i class="fa fa-home menu-icon  ml-0 pr-3"></i>
				<span class="menu-title mr-auto"><b>Home</b></span>
			</a>
		</li>
		
		<li class="nav-item">
			<a class="nav-link" href="<?= $user_data->active_status==1 ? base_url('JobsCtrl') : base_url("InactivePage")?>">
				<span class="menu-title"><b>PPC Work</b>
				<?php if($user_data->active_status == 0){
						echo '<div class="badge badge-pill badge-gradient-danger mt-1">Disabled</div>';
					 } elseif ($user_data->active_status == 2) {
						echo '<div class="badge badge-pill badge-gradient-success mt-1">Active Soon</div>';
					} ?>
				</span>
				<i class="mdi mdi-worker mdi-24px  menu-icon"></i>
			</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" data-toggle="collapse" href="#affiliate" aria-expanded="false" aria-controls="ui-basic">
				<i class="fa fa-cube  menu-icon  ml-0 pr-3"></i>
				<span class="menu-title d-flex align-items-center mr-auto"><b>Affiliate Marketing</b></span>
				<i class="menu-arrow"></i>
			</a>
			<div class="collapse" id="affiliate">
				<ul class="nav flex-column sub-menu">
					<li class="nav-item"> <a class="nav-link" href="<?=base_url()?>ProductSellCtrl"><i class="fa fa-link menu-icon ml-0 mr-2"></i> Link</a></li>
					<li class="nav-item"> <a class="nav-link" href="<?= base_url('ProductSellCtrl/pdt_sell_history') ?>"><i class="mdi mdi-worker menu-icon ml-0 mr-2"></i> Sell Report </a></li>
				</ul>
			</div>
		</li>
        <!-- Vendor Product Navigation Start From Here -->
		<li class="nav-item">
			<a class="nav-link" data-toggle="collapse" href="#product-sell" aria-expanded="false" aria-controls="ui-basic">
				<i class="fa fa-briefcase  menu-icon  ml-0 pr-3"></i>
				<span class="menu-title d-flex align-items-center mr-auto"><b>Product Sell</b></span>
				<i class="menu-arrow"></i>
			</a>
			<div class="collapse" id="product-sell">
				<ul class="nav flex-column sub-menu">
					<li class="nav-item"> <a class="nav-link" href="<?= base_url('VendorCtrl/products') ?>"><i class="mdi mdi-worker menu-icon ml-0 mr-2"></i> All Products </a></li>					
					<li class="nav-item"> <a class="nav-link" href="<?= base_url('VendorCtrl/product')?>"><i class="fa fa-search menu-icon ml-0 mr-2"></i> Add New Product </a></li>
					<li class="nav-item"> <a class="nav-link" href="<?= base_url('VendorCtrl/categories') ?>"><i class="mdi mdi-worker menu-icon ml-0 mr-2"></i> Categories </a></li>
					<li class="nav-item"> <a class="nav-link" href="<?= base_url('VendorCtrl/orders') ?>"><i class="mdi mdi-worker menu-icon ml-0 mr-2"></i> Orders </a></li>
				</ul>
			</div>
		</li>
        <!-- Vendor Product Navigation Start From Here -->        

		<li class="nav-item">
			<a class="nav-link" data-toggle="collapse" href="#buyer-work" aria-expanded="false" aria-controls="ui-basic">
				<i class="fa fa-briefcase  menu-icon  ml-0 pr-3"></i>
				<span class="menu-title d-flex align-items-center mr-auto"><b>Freelance Jobs</b></span>
				<i class="menu-arrow"></i>
			</a>
			<div class="collapse" id="buyer-work">
				<ul class="nav flex-column sub-menu">
					<li class="nav-item"> <a class="nav-link" href="<?= base_url('BuyerWorkCtrl')?>"><i class="fa fa-search menu-icon ml-0 mr-2"></i> Find Job</a></li>
					<li class="nav-item"> <a class="nav-link" href="<?= base_url('BuyerWorkCtrl/hired_contacts') ?>"><i class="mdi mdi-worker menu-icon ml-0 mr-2"></i> Your Hired Job </a></li>
				</ul>
			</div>
		</li>

		<li class="nav-item">
			<a class="nav-link" data-toggle="collapse" href="#flexiload" aria-expanded="false" aria-controls="ui-basic">
				<i class="fa fa-phone  menu-icon  ml-0 pr-3"></i>
				<span class="menu-title d-flex align-items-center mr-auto"><b>Flexiload Business</b></span>
				<i class="menu-arrow"></i>
			</a>
			<div class="collapse" id="flexiload">
				<ul class="nav flex-column sub-menu">
					<li class="nav-item"> <a class="nav-link" href="<?= base_url('FlexiloadCtrl/singleLoadForm')?>"><i class="mdi mdi-phone-forward menu-icon ml-0 mr-2"></i>Single Flexiload</a></li>
					<li class="nav-item"> <a class="nav-link" href="<?= base_url('FlexiloadCtrl/buy_package') ?>"><i class="mdi mdi-cart-outline menu-icon ml-0 mr-2"></i> Buy Package </a></li>
					<li class="nav-item"> <a class="nav-link" href="<?= base_url('FlexiloadCtrl/blance') ?>"><i class="fa fa-money menu-icon ml-0 mr-2"></i> Flexiload Blance </a></li>
					<li class="nav-item"> <a class="nav-link" href="<?= base_url('FlexiloadCtrl/blance_transfer') ?>"><i class="mdi mdi-transfer menu-icon ml-0 mr-2"></i>Blance Transfer</a></li>
				</ul>
			</div>
		</li>

		<li class="nav-item">
			<a class="nav-link" data-toggle="collapse" href="#reports" aria-expanded="false" aria-controls="ui-basic">
				<i class="fa fa-bar-chart  menu-icon ml-0 pr-3"></i>
				<span class="menu-title"><b>History</b></span>
				<i class="menu-arrow"></i>
			</a>
			<div class="collapse" id="reports">
				<ul class="nav flex-column sub-menu">
					<li class="nav-item"> <a class="nav-link" href="<?=base_url()?>ReportsCtrl/job_earn_report">PPC Work<i class="mdi mdi-worker menu-icon"></i> </a></li>
				<li class="nav-item"> <a class="nav-link" href="<?=base_url()?>ReportsCtrl/refer_earn_report">Refer Earn <i class="mdi mdi-share menu-icon"></i> </a></li>
				<li class="nav-item"> <a class="nav-link" href="<?=base_url()?>ReportsCtrl/refer_bounus_report">Refer Bonus <i class="mdi mdi-coin menu-icon"></i> </a></li>
					<li class="nav-item"> <a class="nav-link" href="<?=base_url()?>ReportsCtrl/bounus_earn_report">Affiliate <i class="fa fa-share-alt menu-icon"></i> </a></li>
				<li class="nav-item"> <a class="nav-link" href="<?=base_url()?>ReportsCtrl/balance_deduct">Balance Removed <i class="mdi mdi-coin menu-icon"></i> </a></li>
				</ul>
			</div>
		</li>

		<li class="nav-item">
			<a class="nav-link" data-toggle="collapse" href="#get-paid" aria-expanded="false" aria-controls="ui-basic">
				<i class="fa fa-money ml-0 pr-3 menu-icon"></i>
				<span class="menu-title"><b>Get Paid</b></span>
				<i class="menu-arrow"></i>
			</a>
			<div class="collapse" id="get-paid">
				<ul class="nav flex-column sub-menu">
					<li class="nav-item"> <a class="nav-link" href="<?= base_url('WithdrawCtrl') ?>">Withdraw Request &nbsp <i class="mdi mdi-message-text-outline menu-icon"></i> </a></li>
					<li class="nav-item"> <a class="nav-link" href="<?= base_url('WithdrawCtrl/withdraw_histry')?>">Withdraw History &nbsp<i class="mdi mdi-history menu-icon"></i> </a></li>
				</ul>
			</div>
		</li>
		
		<li class="nav-item">
			<a class="nav-link" href="<?=base_url()?>ReferMembersCtrl">
					<i class="fa fa-share mdi-24px menu-icon ml-0 mr-2"></i>
				<span class="menu-title"><b>Refer Members</b></span>
			</a>
		</li>

		<li class="nav-item">
			<a class="nav-link" href="<?=base_url()?>BusinessPlanCtrl">
				<span class="menu-title"><b>Business Plan</b></span>
				<i class="mdi mdi-binoculars mdi-24px menu-icon"></i>
			</a>
		</li>
		
		
		<li class="nav-item">
			<a class="nav-link" data-toggle="collapse" href="#ent" aria-expanded="false" aria-controls="ui-basic">
				<span class="menu-title d-flex align-items-center"><b>Entertainment</b><span class="badge badge-pill badge-gradient-warning mt-1">New</span></span>
				<i class="menu-arrow"></i>
				<i class="mdi mdi-football mdi-24px  menu-icon"></i>
			</a>
			<div class="collapse" id="ent">
				<ul class="nav flex-column sub-menu">
					<li class="nav-item"> <a class="nav-link" href="<?= base_url('EntCtrl/cric')?>">Cricket<i class="mdi mdi-football-helmet menu-icon"></i> </a></li>
				</ul>
			</div>
		</li>
		
	    <br>
		<br>
		<li class="nav-item ">
			<a style="border-top:1px solid lightgrey" class="nav-link" href="<?=base_url("SupportCtrl")?>">
					<i class="fa fa-question-circle menu-icon ml-0 mr-2"></i>
					<span class="menu-title "><b>Help & Support</b></span>
			</a>
		</li>

	</ul>
</nav>


<!-- main panel -->
<div class="main-panel">


	<!-- wrappper start -->
	<div class="content-wrapper">
