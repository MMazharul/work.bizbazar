<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-info text-white mr-2">
			<i class="mdi mdi-help-circle-outline mdi mdi-24px"></i>
		</span>
		Need Help & Support ?
	</h3>
</div>


<div class="row">
	<div class="col-12">
		If You have any quiestions about our Company,<br>
		Company Policies and any kind of support you need,
		<br>
		Or have find any kind of problems in your account.
		<br>
		<br>
		Just Drop Your Messege below.
		<br>

		<?php if(isset($_SESSION['fls_msg'])){ ?>
			<div class="alert alert-info">
				<?=$_SESSION['fls_msg']?>
			</div>
		<?php }?>
		<br>
		<form action="/supportCtrl/mail_send" method="post">
			<div class="row">
				<div class="col-md-7 col-12">
					<br>
					<label for="">Name:</label>
					<input name="name" type="text" class="form-control" placeholder="Your Name..." required>
				</div>
				<br>
				<div class="col-md-7 col-12">
					<br>
					<label for="">Mobile:</label>
					<input name="mobile" type="text" class="form-control" placeholder="Mobile" readonly value="0<?= $mob ?>" required>
				</div>

				<div class="col-md-7 col-12">
					<br>
					Email:
					<input name="mail" type="email" class="form-control" placeholder="Your Email..." required>
				</div>
				<br>
				<div class="col-md-7 col-12">
					<br>
					Message:
					<textarea name="message" class="form-control" name="" id="" cols="30" rows="10"></textarea>
				</div>
				<div class="col-md-7 col-12">
					<br>
					<button type="submit" class="btn btn-primary">Send</button>
				</div>
			</div>

		</form>
		<img class="mt-3 border10" src="<?= base_url("app-assets/images/support.png") ?>" width="300px" alt="">
	</div>
</div>







<!-- Load Facebook SDK for JavaScript -->
<!-- <div id="fb-root"></div>
<script>
	window.fbAsyncInit = function () {
		FB.init({
			appId: '767194846951675',
			autoLogAppEvents: true,
			xfbml: true,
			version: 'v2.11'
		});
	};

	(function (d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s);
		js.id = id;
		js.src = "<?= "" /* base_url('app-assets/js/facebook.customerChat.js') */?>";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script> -->

<!-- Your customer chat code -->
<!-- <div class="fb-customerchat" attribution=setup_tool page_id="554874118365388" theme_color="#00b8ff">
</div> -->