<div class="row">
	<div class="col-12 bg-gradient-success justify-content-center text-center text-white border10 shadow p-5 m-2">
		<div class="row">
			<div class="col-12 col-md-8 col-sm-12 text-center">
			<h4 >Dear User,<br>Your Activation Request has been sent, Account will activate soon <br> Please Wait for Account Activation. <br>Thank You.</h4>
			</div>
			<div class="col-md-4 col-sm-12 align-self-center justify-content-center">
				<i class="mdi mdi-autorenew mdi-spin mdi-48px text-white"></i>
			</div>
		</div>
		
	</div>
</div>