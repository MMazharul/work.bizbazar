<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-info text-white mr-2">
			<i class="fa fa-cubes"></i>
		</span>
		Affiliate Product Sell History
	</h3>
</div>


<div class="row">
	<div class="col-md-12 grid-margin stretch-card p-0 m-0">

		<div class="card border10">
			<div class="card-body p-3">
				<div class="table-responsive">
					<table class="table table-borderless table-striped table-hover " id="datatable">
						<thead>
							<tr>
								<th>#</th>
								<th>Product Code</th>
								<th>Price</th>
								<th>Income</th>
								<th>Date</th>
							</tr>
						</thead>
						<tbody>
							<?php $serial = 1;
							foreach ($all_income as $income) { ?>
								<tr>
									<td><?= $serial++ ?></td>
									<td><?= $income->product_code ?></td>
									<td><?= $income->price ?></td>
									<td><?= $income->income ?></td>
									<td><?= nice_date($income->created_at, 'd-m-y') ?></td>
								</tr>
							<?php } ?>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
