<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-danger text-white mr-2">
			<i class="mdi mdi-cash-multiple"></i>
		</span>
		Cash Withdraw
	</h3>
</div>

<div class="row p-0 ">
	<div class="col-md-12 border10 m-0 p-0">
		<div class="card bg-white text-dark border10">
			<div class="card-body">

				<h5 class="font-weight-normal mb-3">Your Current Balance
					<i class="mdi mdi-chart-line mdi-36px float-right"></i>
				</h5>

				<p>Job Earn :&nbsp ৳ <span class="font_medium_lg">
				<?= number_format($total_job_earn->total_earn,2) ?></span></p>


				<p>Refer Bonus Earn :&nbsp ৳ <span class="font_medium_lg">
						<?= number_format($refer_bonus_earn->refer_earn_amount,2) ?></span></p>



				<p>Affiliate Earn :&nbsp ৳ <span class="font_medium_lg">
						<?= number_format($other_earn->other_earn_amount,2) ?></span></p>
				<p>Total Balance :&nbsp ৳ <span class="font_medium_lg">
						<?= number_format($current_total->total,2) ?></span></p>

				<small><u>Note:</u><br>
					■ Minimum Cash-Out Amount <b>500 BDT</b>.<br>
					</small>

				<br>
				<br>
				<br>

				<?php $alw=0; if($withdraw_allow->withdraw_allow){ $alw=1; }?>
				<?= $withdraw_pending ? "<h4 class='text-danger'>Your One Withdraw Request is Pending, You Can Request Again After Proceed this request. </h4>" :"" ?>
				<h5>Cash-Out</h5>
				<br>
		

				<form id="withdraw" method="post">
					<div class="row d-flex align-items-center">
						<div class="col-sm-12 col-md-4 form-group">
							<label for="rcv_num">&nbsp Receive Number</label>
							<input <?= !$withdraw_pending ?: 'disabled'?> name="rcv_num" type="number" min="1000000000" class="form-control" id="rcv_num" placeholder="Receive Number" required>
						</div>

						<?php if($alw === 1):?>
						<div class="col-sm-12 col-md-4 form-group <?=$alw === 1 ? '' : 'd-none'?>">
							<label for="amnt">&nbsp Job Earn Amount Only <br> (2% bKash fee will Charge)</label>
							<input <?= !$withdraw_pending ?: 'disabled'?> name="amnt" type="Number" min="1000" max="<?= number_format($work_earn->work_earn_amount,0,",","") ?>" class="form-control" id="<?=$alw ? 'amnt':''?>" placeholder="Job Earn Amount"
							 >
						</div>
						<?php endif ?>
						
						<div class="col-sm-12 col-md-4 form-group <?=$alw ? 'd-none':''?>">
							<label for="amnt">Amount<br> (2% bKash fee will Charge)</label>
							<input <?= !$withdraw_pending ?: 'disabled'?> name="amnt" type="Number" min="500" max="<?= number_format($refer_earn->refer_work_earn_amount+$refer_bonus_earn->refer_earn_amount+$other_earn->other_earn_amount,0,",","") ?>" class="form-control" id="<?=$alw==0 ? 'amnt':''?>" placeholder="Refer / Bonus / Other Earn Amount"
							 >
						</div>
						<div class="col-sm-12 col-md-4 form-group">
							<label for="pass">&nbsp Password</label>
							<input <?= !$withdraw_pending ?: 'disabled'?>  name="pass" type="Password" class="form-control" id="pass" placeholder="Password" required>
						</div>

						<h5 class="col-12">Payment Method</h5>

						<div class=" d-none col-sm-6 col-md-3  form-check form-check-light ml-4">
							<label class="form-check-label mr-3" for="agent">&nbsp bKash Agent Number
								<input <?= !$withdraw_pending ?: 'disabled'?> type="radio" name="pay_method" value="bkash-agent" class="form-check-input" id="agent" required>
							</label>
						</div>

						<div class="col-sm-6 col-md-3 form-check form-check-light ml-4">
							<label class="form-check-label" for="personal">&nbsp bKash Personal Number
								<input <?= !$withdraw_pending ?: 'disabled'?> type="radio" name="pay_method" value="bkash-personal" class="form-check-input" id="personal" required>
							</label>
						</div> 

						<div id="snd" class="align-self-center ml-4">
							<button <?= !$withdraw_pending ?: 'disabled'?> class="btn btn-gradient-light btn-rounded btn-block" type="submit">Send <i class="mdi mdi-sync mdi-spin loading"></i> </button>
							
							<div id="conf">
							
							</div>
						</div>

						<div id="loader" class="ml-2"><i class="mdi mdi-spin mdi-loading mdi-36px"></i></div>
						<div id="result" class="ml-2"></div>


					</div>
				</form>

			</div>
		</div>
	</div>
</div>



<script type="text/javascript" src="<?=base_url('app-assets/js/ajax/withdraw.js')?>">
</script>
