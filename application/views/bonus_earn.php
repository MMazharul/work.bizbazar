
<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-info text-white mr-2">
			<i class="fa fa-cube"></i>
		</span>
		Affiliate Income Highlights
	</h3>
	
</div>
<div class="row">
	<div class="col-md-3 mb-2">
		<div class="p-2 border10 bg-white  text-dark">
			<div class="">
				<h5 class="font-weight-normal mb-3">Todays Earn
					<i class="fa fa-calendar-check-o fa-3x float-right"></i>
				</h5>
				<h4 class="mb-5">৳ <?= $today->total_earn==null ? "0" : $today->total_earn ?></h4>
				<h6 class="card-text">Total Transection <?= $today->total_job?></h6>
			</div>
		</div>
	</div>
	<div class="col-md-3 mb-2">
		<div class="bg-white p-2 border10  text-dark">
			<div class="">
				<h5 class="font-weight-normal mb-3">Last 7 days Earn
					<i class="fa fa-calendar-minus-o fa-3x float-right"></i>
				</h5>
				<h4 class="mb-5">৳ <?= $svn_day->total_earn==null ? "0" : $svn_day->total_earn ?></h4>
				<h6 class="card-text">Total Transection <?=$svn_day->total_job?></h6>
			</div>
		</div>
	</div>
	<div class="col-md-3 mb-2">
		<div class="bg-white p-2 border10 text-dark">
			<div class="">
				<h5 class="font-weight-normal mb-3">Last 30 days Earn
					<i class="fa fa-calendar-o fa-3x float-right"></i>
				</h5>
				<h4 class="mb-5">৳ <?= $thirty_day->total_earn==null ? "0" : $thirty_day->total_earn ?></h4>
				<h6 class="card-text">Total Transection <?=$thirty_day->total_job?></h6>
			</div>
		</div>
	</div>
	<div class="col-md-3 mb-2">
		<div class="bg-white p-2 border10  text-dark">
			<div class="">
				<h5 class="font-weight-normal mb-3">Total Earn
					<i class="fa fa-calendar-check-o fa-3x float-right"></i>
				</h5>
				<h4 class="mb-5">৳ <?= $total->total_earn==null ? "0" : $total->total_earn ?></h4>
				<h6 class="card-text">Total Transection <?=$thirty_day->total_job?></h6>
			</div>
		</div>
	</div>
</div>

