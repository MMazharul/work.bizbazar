<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-info text-white mr-2">
			<i class="fa fa-cube fa-4x"></i>
		</span>
		Affiliate Link
	</h3>
</div>

<div class="card border10 row p-3">
	<div class="row justify-content-center">
		<div class="col-md-4 col-10">
			<a class="btn btn-info" href="http://localhost:3000/referer/<?= $user_id + 1000?>" target="_blank"> GO Affiliate Link 🔗</a>
		</div>
	</div>
	<br>
	<h6>Your Affiliate Code : <span class="alert alert-info border10 p-2"><?= $user_id + 1000 ?></span></h6>
	<br>
	<label>Your Affiliate Link to Share others and earn</label>
	<div class="input-group col-md-7 col-11	">
		<br>
		<input id="link_inp" type="text" class="form-control" readonly value="http://localhost:3000/referer/<?= $user_id + 1000 ?>">
		<div class="col-12 col-md-3 text-center">
			<button id="link_cpy" onclick="copy_to_clip(this)" class="btn btn-outline-info btn-rounded" type="button">Copy Affiliate Link</button>
		</div>
	</div>
	<br>
	<h6 class="text-gray">GET Selling Commission by sharing this link or your Affiliate ID</h6>
</div>

<script>
	function copy_to_clip() {
		document.getElementById('link_inp').select();
		document.execCommand('copy');

		document.getElementById('link_cpy').innerHTML = 'Copied !';
		setTimeout(() => {
			document.getElementById('link_cpy').innerHTML = 'Copy Affiliate Link'
		}, 2000);
	}
</script>
