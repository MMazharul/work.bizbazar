<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes" />
		<meta http-equiv="x-ua-compatible" content="ie=edge" />
		<title>Quick-Earn | Best Freelance Site | Best Outsourcing Site in Bangladesh</title>
		<link rel="icon" type="image/png" href="<?= base_url() ?>app-assets/welcome_page_asset/img/16px.png"
			sizes="16x16" />
		<link rel="icon" type="image/png" href="<?= base_url() ?>app-assets/welcome_page_asset/img/32px.png"
			sizes="32x32" />
		<link rel="icon" type="image/png" href="<?= base_url() ?>app-assets/welcome_page_asset/img/48px.png"
			sizes="96x96" />
		<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet" />
		<!-- Font Awesome -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
		<!-- Bootstrap core CSS -->
		<!-- <link href="<?= base_url() ?>app-assets/welcome_page_asset/css/bootstrap.min.css" rel="stylesheet" /> -->
		<!-- Materiaasset/l Design Bootstrap -->
		<!-- <link href="<?= base_url() ?>app-assets/welcome_page_asset/css/mdb.min.css" rel="stylesheet" /> -->
		<!-- Your cuasset/stom styles (optional) -->
		<link href="<?= base_url() ?>app-assets/welcome_page_asset/css/style.min.css" rel="stylesheet" />
		<meta name="theme-color" content="#00b8ff">
		
	</head>

	<body>
		<header>

			<nav>
				<div class="link-group">
					<a class="link font-weight-bold" href="">HOME</a>
					<a class="link font-weight-bold" href="#about-us">ABOUT</a>
					<a class="link font-weight-bold" href="#contact">CONTACT</a>
				</div>

				<a href=""><img id="logo-img-quick"
						src="<?= base_url() ?>app-assets/welcome_page_asset/img/quick.png" /></a>
				<!-- <a href=""><img class="logo-img" src="asset/img/QE.png"/></a> -->
				<a href=""><img class="logo-img"
						src="<?= base_url() ?>app-assets/welcome_page_asset/img/500px.gif" /></a>
				<a href=""><img id="logo-img-earn"
						src="<?= base_url() ?>app-assets/welcome_page_asset/img/earn.png" /></a>

				<div class="right align-self-center">
					<button id="sign-in" data-toggle="modal" data-target="#modal"
						class="btn btn-md btn-outline-info round z-depth-0 ">
						Sign In
					</button>
					<button id="sign-up" class="btn btn-md btn-info round z-depth-0" data-toggle="modal"
						data-target="#modalReg">
						Sign Up
					</button>
					<button id="bar" class="btn btn-outline-info z-depth-0">
						<i class="fa fa-bars fa-2x"></i>
					</button>
				</div>
			</nav>

			<div id="sidebar">
				<div id="close-btn">
					<a href="#" class="link"><i class="fa fa-close fa-2x"></i></a>
				</div>
				<hr class="hr-dark" />

				<a class="font-weight-bold " href="">HOME</a>
				<a class="font-weight-bold " href="#about-us">ABOUT</a>
				<a class="font-weight-bold " href="#contact">CONTACT</a>

				<div style="margin: auto auto -360px auto; bottom: 0px">
					<button class="btn btn-sm btn-outline-info round z-depth-0" data-toggle="modal"
						data-target="#modalReg">
						Sign Up
					</button>
				</div>

				<div class="media-link">
					<a class="font-weight-bold " href=""><i class="fa fa-facebook link fa-2x"></i></a>
					<a class="font-weight-bold " href=""><i class="fa fa-twitter link fa-2x"></i></a>
					<a class="font-weight-bold " href=""><i class="fa fa-linkedin link fa-2x"></i></a>
					<a class="font-weight-bold " href=""><i class="fa fa-youtube-play link fa-2x"></i></a>
				</div>
			</div>
		</header>

		<section>
			<div class="overflow">

				<div id="scene" class="bg-img">
					<div class="layer overflow"><img class="overflow" height="540px"
							src="<?= base_url() ?>app-assets/welcome_page_asset/img/bg/0.png" /></div>
					<div class="layer overflow"><img id="layer1" class="overflow" height="540px"
							src="<?= base_url() ?>app-assets/welcome_page_asset/img/bg/1.png" /></div>
					<div class="layer overflow"><img id="layer2" class="overflow" height="540px"
							src="<?= base_url() ?>app-assets/welcome_page_asset/img/bg/2.png" /></div>
					<div class="layer overflow"><img id="layer2-copy" style="margin-left:74%" class="overflow"
							height="540px" src="<?= base_url() ?>app-assets/welcome_page_asset/img/bg/2.png" /></div>
					<div class="layer overflow"><img id="layer3" class="overflow" height="540px"
							src="<?= base_url() ?>app-assets/welcome_page_asset/img/bg/3.png" /></div>
					<div class="layer overflow"><img id="layer4" class="overflow" height="540px"
							src="<?= base_url() ?>app-assets/welcome_page_asset/img/bg/4.png" /></div>
					<div class="layer overflow"><img id="layer5" class="overflow" height="540px"
							src="<?= base_url() ?>app-assets/welcome_page_asset/img/bg/5.png" /></div>
					<div class="layer overflow"><img id="layer6" class="overflow" height="540px"
							src="<?= base_url() ?>app-assets/welcome_page_asset/img/bg/6.png" /></div>
					<div class="layer overflow"><img id="layer12" class="overflow layer12" height="540px"
							src="<?= base_url() ?>app-assets/welcome_page_asset/img/bg/12.png" /></div>
					<div class="overflow"><img id="layer12-copy" style="position:absolute;left:1365px"
							class="overflow layer12" height="540px"
							src="<?= base_url() ?>app-assets/welcome_page_asset/img/bg/12.png" /></div>
					<div class="layer overflow"><img id="layer7" class="overflow" height="540px"
							src="<?= base_url() ?>app-assets/welcome_page_asset/img/bg/7.png" /></div>
					<div class="overflow"><img id="layer8" class="overflow" height="540px"
							src="<?= base_url() ?>app-assets/welcome_page_asset/img/bg/8.png" /></div>
					<div class="layer overflow"><img id="layer9" class="overflow" height="540px"
							src="<?= base_url() ?>app-assets/welcome_page_asset/img/bg/9.png" /></div>
					<div class="overflow"><img id="layer10" class="overflow" height="540px"
							src="<?= base_url() ?>app-assets/welcome_page_asset/img/bg/10.png" /></div>
					<div class="layer overflow"><img id="layer11" class="overflow" height="540px"
							src="<?= base_url() ?>app-assets/welcome_page_asset/img/bg/11.png" /></div>
					<div class="overflow"><img id="dlr" class="overflow" height="540px"
							src="<?= base_url() ?>app-assets/welcome_page_asset/img/bg/dlr.png" /></div>
					<div class="overflow"><img id="dlr2" class="overflow" height="540px"
							src="<?= base_url() ?>app-assets/welcome_page_asset/img/bg/dlr.png" /></div>
					<div class="overflow"><img id="dlr3" class="overflow" height="540px"
							src="<?= base_url() ?>app-assets/welcome_page_asset/img/bg/dlr3.png" /></div>

					<div id="heading">
						Easy to Earn <br />
						from <b class="font-weight-bold text-info">Anywhere</b>
					</div>
				</div>


				<!-- <img class="bg-img" src="<?= base_url() ?>app-assets/welcome_page_asset/img/bg.svg" alt="" /> -->
			</div>


			<br />
			<br />
			<br />
			<div class="text-center txt-dp-blue">
				<h4 class="font-weight-bold">HOW IT WORKS</h4>
				<p>3 Simple Steps</p>
				<div class="hr-sm"></div>
			</div>

			<div class="container">
				<div class="row justify-content-center">
					<div class="col-md-4 col-sm-12 col-xl-4 col-12">
						<div class="svg-icon">
							<img src="<?= base_url() ?>app-assets/welcome_page_asset/img/10-networking.png"
								alt="Create Account" />
							<p class="text-center">
								Create an acoount and pay our service fee
							</p>
						</div>
					</div>
					<div class="col-md-4 col-sm-12 col-xl-4 col-12">
						<div class="svg-icon">
							<img src="<?= base_url() ?>app-assets/welcome_page_asset/img/3-vision.png"
								alt="Watch Adds" />
							<p class="text-center">Click and Watch Adds</p>
						</div>
					</div>
					<div id="about-us"></div>
					<div class="col-md-4 col-sm-12 col-xl-4 col-12">
						<div class="svg-icon">
							<img src="<?= base_url() ?>app-assets/welcome_page_asset/img/35-money.png	"
								alt="Make Money" />
							<p class="text-center">Get Paid</p>
						</div>
					</div>
				</div>
			</div>

			<br />
			<br />
			<h4 class="text-center txt-dp-blue font-weight-bold">ABOUT US</h4>
			<div class="hr-sm"></div>
			<br />
			<br />

			<div class="container">
				<div class="row justify-content-center align-items-center">
					<div class="col-md-6 col-12">
						<p class="text-center  txt-dp-blue">
							<span class="text-info">Quick Earn</span> is a digital advertising
							platform and digital working marketplace for mass people. It is
							specially designed for Bangladeshi citizens to earn money by
							completing some easy steps. We also promote here online start up
							and corporate business advertisement on demand at cheap rate. All
							of our members take part in promotional work to promote and spread
							up each and every advertising campaign rapidly.
						</p>
						<p class="text-center txt-dp-blue">
							Digital Working Opportunities With
							<span class="text-info">Quick Earn</span> working platform, huge
							people can involve here to make some extra money by doing some
							work with us. All are able to take part to promote our promotional
							campaigns in an easy way. We pay our members for completing every
							successful adverting campaign. Our technology allows huge people
							to do our specially designed work every day. People can earn money
							here from their direct job and also from Network Marketing or our
							affiliation program. It’s a vast working opportunity for
							unemployed, student, educated, half-educated, less salary holder
							and so on to generate some extra money by utilizing their leisure
							time with his internet connected smart phone, tab, laptop or PC.
						</p>
					</div>
					<div class="col-md-6 col-12 ">
						<img class="about-img" src="<?= base_url() ?>app-assets/welcome_page_asset/img/aboutus.jpg"
							alt="quick earn about-us" />
					</div>
				</div>
			</div>

			<br />
			<br />
			<br />
			<br />
			<h4 class="text-center txt-dp-blue font-weight-bold">WE ACCEPT</h4>
			<div class="hr-sm"></div>
			<br />
			<div class="sprt" style="overflow:hidden">
				<img class="support" src="<?= base_url() ?>app-assets/welcome_page_asset/img/master-card.png"
					alt="Quick-earn payment master" />
				<img class="support" src="<?= base_url() ?>app-assets/welcome_page_asset/img/visa.png"
					alt="Quick-earn payment " />
				<img class="support" src="<?= base_url() ?>app-assets/welcome_page_asset/img/BKash.png"
					alt="Quick-earn payment visa" />
				<img class="support" src="<?= base_url() ?>app-assets/welcome_page_asset/img/download.png"
					alt="Quick-earn payment " />
				<img class="support" src="<?= base_url() ?>app-assets/welcome_page_asset/img/download.png"
					alt="Quick-earn payment " />
				<div id="contact"></div>
			</div>

			<br />
			<br />
			<br />
			<br />
			<h4 class="text-center txt-dp-blue font-weight-bold">CONTACT US</h4>
			<div class="hr-sm"></div>
			<br />

			<div class="container">
				<div class="row justify-content-center align-items-center">
					<div class="col-12 col-md-6 ">
						<h6 class=" txt-dp-blue font-weight-bold">Corporate Contact</h6>
						<p class=" txt-dp-blue">
							You can see the dreams are bigger than the fears, tell us about
							your simplicity and work together. Drop us a message and we will
							get back as soon as possible
						</p>
						<h6 class=" txt-dp-blue font-weight-bold">Advertise with us</h6>
						<p class=" txt-dp-blue">
							We will promote your business effectively on behalf of you at
							cheap rate, Our specially designed advertising campaign will lead
							you more conversions. You will get an intellectual advertising
							opportunity with signing up as a proud advertising partner.
						</p>
					</div>
					<div class="col-12 col-md-6 ">
						<!-- Material form contact -->
						<div class="card round z-depth-0 hover-shadow">
							<!-- Card content -->
							<div class="card-body px-lg-5 pt-0">
								<!-- Form -->
								<form class="text-center" style="color: #757575;">
									<!-- Name -->
									<div class="md-form mt-3">
										<input type="text" id="materialContactFormName" class="form-control" />
										<label for="materialContactFormName">Name</label>
									</div>

									<!-- E-mail -->
									<div class="md-form">
										<input type="email" id="materialContactFormEmail" class="form-control" />
										<label for="materialContactFormEmail">E-mail</label>
									</div>

									<!-- Message -->
									<div class="md-form">
										<textarea type="text" id="materialContactFormMessage"
											class="form-control md-textarea" rows="3"></textarea>
										<label for="materialContactFormMessage">Message</label>
									</div>

									<!-- Send button -->
									<button
										class="btn round btn-outline-info btn-rounded btn-block z-depth-0 my-4 waves-effect"
										type="submit">
										Send
									</button>
								</form>
								<!-- Form -->
							</div>
						</div>
						<!-- Material form contact -->
					</div>
				</div>
			</div>

		</section>
		<br>
		<h4 class="text-center">SUPPORTS</h4>
		<div class="sprt" style="overflow:hidden">
			<img class="support" src="<?=base_url('app-assets/images/support/bing-cert.png')?>" height="60px" alt="">
			<img class="support" src="<?=base_url('app-assets/images/support/ga-cert.png')?>" alt="">
			<img class="support" src="<?=base_url('app-assets/images/support/google-cert.png')?>" height="60px" alt="">
			<img class="support" src="<?=base_url('app-assets/images/support/hubspot-cert.png')?>" height="60px" alt="">
			<img class="support" src="<?=base_url('app-assets/images/support/trusted.png')?>" height="70px" alt="">

		</div>

		<!-- Modal: Login Form -->
		<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
			aria-hidden="true">
			<div id="login_modal" class="modal-dialog cascading-modal" role="document">
				<!-- Content -->
				<div class="modal-content round" style="z-index:-2;overflow:hidden">
					<div class="modal-cls">
						<span class="txt-dp-blue font-weight-bold">SIGN IN</span>
						<button required type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>

					<img class="modal-bg-img" style="z-index:-1"
						src="<?= base_url() ?>app-assets/welcome_page_asset/img/isometric-dribbble.png" alt=""
						width="417px" />
					<!-- Body -->
					<div class="modal-body mb-1">

						<form id="login_form" method="post">
							<div class="md-form form-sm mb-5">
								<i class="fa fa-mobile prefix"></i>
								<input required id="login-mobile-no" name="login_number" type="number"
									class="form-control form-control-sm validate" />
								<label data-error="x" data-success="✓" for="login-mobile-no">Your Mobile</label>
							</div>

							<div class="md-form form-sm mb-4">
								<i class="fa fa-lock prefix"></i>
								<input required id="login-password" name="login_password" type="password"
									class="form-control form-control-sm validate" />
								<label data-error="x" data-success="✓" for="login-password">Your password</label>
							</div>
							<!-- loading -->
							<div class="text-center loading"><i class="fa fa-spinner fa-spin fa-3x text-info"></i></div>

							<div class="text-center mt-2">
								<button id="login_submit" type="submit" class="btn btn-info round">
									Sign in <i class="fa fa-sign-in ml-1"></i>
								</button>
							</div>
						</form>
					</div>
					<!-- Footer -->
					<div class="modal-footer justify-content-center">
						<div class="options text-center">
							<span id="login_form_error"></span> <br />
							<p>Forgot <span onclick="forget_pass()" style="cursor: pointer;"
									class="blue-text">Password?</span></p>
						</div>
					</div>
				</div>
				<!-- /.Content -->
			</div>
		</div>
		<!-- Modal: Login  Form -->

		<!-- Modal: Register Form -->
		<div class="modal fade" id="modalReg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
			aria-hidden="true">
			<div class="modal-dialog  modal-lg" role="document">
				<!-- Content -->
				<div class="modal-content round">
					<div class="modal-cls">
						<span class="txt-dp-blue font-weight-bold">SIGN UP</span>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>

					<form action="login.php" id="register_form">
						<!-- Body -->
						<div class="modal-body mb-1">
							<img class="modal-bg-img"
								src="<?= base_url() ?>app-assets/welcome_page_asset/img/register.png" width="627px"
								alt="" />


							<div class="row">
								<div class="col-10 col-md-6">
									<div class="md-form form-sm mb-5">
										<i class="fa fa-user prefix"></i>
										<input required name="reg_name" type="text" id="reg_name"
											class="form-control form-control-sm validate" />
										<label data-error="x" data-success="✓" for="modalLRInput10">Your Name</label>
									</div>
								</div>
								<div class="col-10 col-md-6">
									<div class="md-form form-sm mb-5">
										<i class="fa fa-mobile-phone prefix"></i>
										<input required name="reg_number" type="number" id="reg_num"
											class="form-control form-control-sm validate" />
										<label data-error="x" data-success="✓" for="modalLRInput1">Your Mobile</label>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-10 col-md-6">
									<div class="md-form form-sm mb-5">
										<i class="fa fa-lock prefix"></i>
										<input required name="reg_password" type="password" id="reg_password"
											class="form-control form-control-sm validate" />
										<label data-error="x" data-success="✓" for="modalLRInput">Your Password</label>
									</div>
								</div>
								<div class="col-10 col-md-6">
									<div class="md-form form-sm mb-5">
										<i class="fa fa-lock prefix"></i>
										<input required name="reg_conpass" type="password" id="reg_conpass"
											class="form-control form-control-sm validate" />
										<label data-error="x" data-success="✓" for="aa">Your Confirm Password</label>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-10 col-md-6">
									<div class="md-form form-sm mb-5">
										<i class="fa fa-id-card prefix"></i>
										<input required name="reg_referer" type="number" id="reg_referer"
											<?= isset($_GET['refer_id']) ? "readonly" : "" ?>
											value="<?= isset($_GET['refer_id']) ? $_GET['refer_id'] : null ?>"
											class="form-control form-control-sm validate" />
										<label data-error="x" data-success="✓" for="reg_referer">Reffaral ID</label>
									</div>
								</div>
								<div class="col-10 col-md-6">
									<div class="md-form form-sm mb-5">
										<i class="fa fa-envelope prefix"></i>
										<input required name="reg_email" type="email" id="reg_email"
											class="form-control form-control-sm validate" />
										<label data-error="x" data-success="✓" for="reg_email">Your Email</label>
									</div>
								</div>
							</div>

							<div class="md-form form-sm mb-5">
								<i class="fa fa-id-card-o prefix"></i>
								<input required name="reg_nid" type="number" id="reg_nid"
									class="form-control form-control-sm validate" />
								<label data-error="x" data-success="✓" for="reg_nid">Your NID Number</label>
							</div>

							<div class="text-center mt-2">
								<!-- loading -->
								<div class="text-center loading"><i class="fa fa-spinner fa-spin fa-2x text-info"></i>
								</div>
								<br>
								<button type="submit" class="btn btn-info round">
									Sign Up <i class="fa fa-sign-in ml-1"></i>
								</button>
								<br>
								<br>
								<div><span id="register_form_error"></span></div>
							</div>

						</div>
					</form>

				</div>
				<!-- /.Content -->
			</div>
		</div>
		<!-- Modal: Register Form -->

		<br />
		<br />
		<br />
		<footer>
			<img id="windmil" src="<?= base_url() ?>app-assets/welcome_page_asset/img/windmill.png"
				alt="quick-earn footer img" />
			<img style="width:100%" src="<?= base_url() ?>app-assets/welcome_page_asset/img/footer.png" alt="" />

			<div id="footer">
				<div class="footer-media">
					<a class="font-weight-bold " href="https://www.facebook.com/Quick-Earn-554874118365388/"
						target="_blank"><i class="fa fa-facebook"></i></a>
					<a class="font-weight-bold " href="https://twitter.com/QuickEarn_info" target="_blank"><i
							class="fa fa-twitter  "></i></a>
					<a class="font-weight-bold " href=""><i class="fa fa-linkedin "></i></a>
					<a class="font-weight-bold " href=""><i class="fa fa-youtube-play "></i></a>
				</div>

				<div id="footer-link">
					<small>© Copyrights 2018 | All Right Reserved |
						<a href="<?= base_url() ?>app-assets/welcome_page_asset/terms.php">Terms & Conditions</a> | <a
							href="<?= base_url() ?>app-assets/welcome_page_asset/privacy.php">Privacy</a></small>
				</div>
			</div>
		</footer>

		<!-- SCRIPTS -->
		<!-- JQuery -->
		<script type="text/javascript" src="<?= base_url() ?>app-assets/welcome_page_asset/js/jquery-3.3.1.min.js">
		</script>
		<!-- Bootstrap tooltips -->
		<script type="text/javascript" src="<?= base_url() ?>app-assets/welcome_page_asset/js/popper.min.js"></script>
		<!-- Bootstrap core JavaScript -->
		<script type="text/javascript" src="<?= base_url() ?>app-assets/welcome_page_asset/js/bootstrap.min.js">
		</script>
		<!-- MDB core JavaScript -->
		<script type="text/javascript" src="<?= base_url() ?>app-assets/welcome_page_asset/js/mdb.min.js"></script>
		<script type="text/javascript" src="<?= base_url() ?>app-assets/welcome_page_asset/js/myscript.js"></script>
		<script type="text/javascript" src="<?= base_url() ?>app-assets/js/ajax/login.js" type="text/javascript">
		</script>

		<script>
			var $loading = $('.loading');
			$(document).ajaxStart(function () {
					$loading.show();
				})
				.ajaxStop(function () {
					$loading.hide();
				});

		</script>



	</body>

</html>
