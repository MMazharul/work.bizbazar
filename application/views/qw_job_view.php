<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-info text-white mr-2">
			<i class="mdi mdi-briefcase-outline mdi-24px"></i>
		</span>
		Buyer Work Details
	</h3>
</div>

<div class="row justify-content-center">
	<div class="col-12 col-md-12 p-0">
		<div class="card bg-white shadow p-3 mb-3 border10">
			<div>
				<h4><?= $job[0]->title ?></h4>
				<small>Posted: <?= nice_date($job[0]->created_at, 'Y-m-d') ?></small>
				<br>
				<small>Expire: <?= $job[0]->exp_date == null ? 'Not Assaign': nice_date($job[0]->exp_date, 'Y-m-d') ?></small>
				<?php $dif = date_diff(date_create(date('Y-m-d')),date_create($job[0]->exp_date));echo $dif->d > 0 && $dif->invert ==1 ? '<span class="badge badge-pill badge-danger ml-2"><i class="mdi mdi-calendar"></i> Expired</span><br>' : '' ?>
				<br>
				<br>
				<div>Category - <b><?= $job[0]->cat_name ?></b></div>
				<br>
				<div>Required Skills :
					<?php $skills = explode(",", $job[0]->skills);
					foreach ($skills as $skill) { ?>
						<span class="badge badge-info badge-pill"><?= $skill ?></span>
					<?php } ?>
				</div>
				<br>
				<div>Budget - <b><?= $job[0]->budget ?> TK</b></div>

				<br>
				<b>Brief:</b>
				<p><?= $job[0]->job_desc ?></p>

				<br>
				<?php if ($job[0]->files != null && $job[0]->files != "") :
					$files = explode(",", $job[0]->files); ?>
					<b>Additional Attached Files:</b>
					<br>
					<ul>
						<?php foreach ($files as $file) : ?>
							<li><a href="https://admin/files/<?= $file ?>" target="_blank"><u><?= $file ?></u></a><br></li>
						<?php endforeach;
				endif ?>
				</ul>
			</div>

			<br>
			<div><span class="p-3"><b>Bids</b></span></div>
			<hr>
			<?php
			if ($hire_or_not != null) :
				echo "<b>You are already hired for this job</b>";
			endif
			?>
			<?php if ($own_bid == null && $hire_or_not == null && $dif->d >0 && $dif->invert ==0) : ?>
				<div class="bg-light border10 p-2" style="max-width:500px">
					<form method="post" action="/BuyerWorkCtrl/save_bid">
						<div class="d-flex">
							<img class="bid-profile-img" src="https://admin.Biz-Bazar.com/app-assets/images/members/<?= $_SESSION['picture'] ?>" alt="profile image">
							<div class="d-flex" style="flex-direction:column">
								&nbsp; <?= $_SESSION['user_name'] ?>
								<small class="text-gray ">&nbsp; Worker</small>
								&nbsp;
							</div>
						</div>
						<div class="">
							<input type="hidden" name="job_id" value="<?= $job[0]->id ?>">
							<input class="form-control dense mb-2 border-right-0" name="rate" type="number" placeholder="Your Bid Rate...">
							<textarea class="form-control border10 mb-2" style=resize:auto" name="cv" id="" rows="3" placeholder="Cover Letter..."></textarea>
							<button onclick="return confirm('Are you sure ?')" class="btn btn-inverse-info btn-rounded btn-block">Bid</button>
						</div>
					</form>
				</div>
			<?php else:?>
			<div>Job Expired</div>
			<?php endif ?>

			<?php if ($own_bid != null && $hire_or_not == null) : ?>
				<div class="bg-light border10 p-2" style="max-width:500px">
					<form method="post" action="/BuyerWorkCtrl/edit_bid">
						<div class="d-flex justify-content-between">
							<div class="d-flex">
								<img class="bid-profile-img" src="https://admin.Biz-Bazar.com/app-assets/images/members/<?= $_SESSION['picture'] ?>" alt="profile image">
								<div class="d-flex" style="flex-direction:column">
									&nbsp; <?= $_SESSION['user_name'] ?>
									<small class="text-gray ">&nbsp; Worker</small>
									&nbsp;
								</div>
							</div>
							<div>
								<button type="button" class="btn btn-inverse-secondary btn-rounded p-2  " id="menu" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-menu text-gray"></i></button>
								<div class="dropdown-menu navbar-dropdown border20 shadow-sm p-2" aria-labelledby="menu">
									<button onclick="edit_bid()" type="button" class="dropdown-item" href="<?= base_url('ProfileCtrl') ?>">
										<i class="mdi mdi-pencil mr-2 text-success"></i>
										Edit</button>
									<div class="dropdown-divider"></div>
									<a onclick="return confirm('Sure to Delete Bid ?')" class="dropdown-item" href="<?= base_url() ?>BuyerWorkCtrl/del_bid?b_id=<?= base64_encode($own_bid[0]->id) ?>">
										<i class="mdi mdi-delete mr-2 text-danger"></i>
										Delete
									</a>
								</div>
							</div>
						</div>
						<div class="">
							<input type="hidden" name="job_id" value="<?= $job[0]->id ?>">
							<input type="hidden" name="bid_id" value="<?= $own_bid[0]->id ?>">
							<input readonly id="bid_amnt" value="<?= $own_bid[0]->amount ?>" class="form-control dense mb-2 border-right-0" name="rate" type="number" placeholder="Your Bid Rate...">
							<textarea readonly class="form-control border10 mb-2" style="resize:auto" name="cv" id="bid_cv" rows="3" placeholder="Cover Letter..."><?= $own_bid[0]->cv ?></textarea>
							<button id="save_btn" onclick="return confirm('Are you sure ?')" type="submit" class="btn btn-inverse-info btn-rounded btn-block d-none">Bid</button>
							<button id="cancel_btn" onclick="cancel_()" type="button" class="btn btn-inverse-danger btn-rounded btn-block d-none">Cancel</button>
						</div>
					</form>
				</div>
			<?php endif ?>


			<br>
			<b class="p-3">Other Bids (<?= count($bids) ?>)</b>
			<hr>

			<?php foreach ($bids as $bid) : ?>
				<div class="bg-light border10 shadow-sm p-2 mb-2" style="max-width:500px">
					<div class="row">
						<div class="d-flex col-8">
							<img class="bid-profile-img" src="https://admin.Biz-Bazar.com/app-assets/images/members/<?= $bid->picture ?>" alt="profile image">
							<div class="d-flex" style="flex-direction:column">
								&nbsp; <?= $bid->name ?>
								<small class="text-gray ">&nbsp; Worker</small>
							</div>
						</div>
						<div class="col-4 align-self-center">
							<?= $bid->amount ?> TK
						</div>
					</div>
				</div>
			<?php endforeach ?>
		</div>

	</div>
</div>


<script>
	function edit_bid() {
		$('#bid_amnt,#bid_cv').removeAttr('readonly');
		$('#save_btn,#cancel_btn').removeClass("d-none");
	}

	function cancel_() {
		$('#bid_amnt,#bid_cv').attr('readonly', true);
		$('#save_btn,#cancel_btn').addClass("d-none");
	}
</script>