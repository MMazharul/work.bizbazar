<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-danger text-white mr-2">
			<i class="mdi mdi-binoculars"></i>
		</span>
		Business Plan
	</h3>
	<nav aria-label="breadcrumb">
		<ul class="breadcrumb">
			<li class="breadcrumb-item active" aria-current="page">
				<span></span>Overview
				<i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
			</li>
		</ul>
	</nav>
</div>

<div class="bg-gradient-warning text-black text-center border10 shadow p-2 m-2">
	<h4> <i class="mdi mdi-share-variant"></i> Level-1 Refer Bonus <b>৳ <?=$refer_bonus->refer_bonus?> Tk</b> . 1st Referer 
	<hr>
	 Level-2 Refer Bonus <b>৳ <?=$refer_bonus->sec_lvl?> Tk</b> . 2nd Referer
	<br>
	 Level-3 Refer Bonus <b>৳ <?=$refer_bonus->trd_lvl?> Tk</b> . 3rd Referer
	<br>
	 Level-4 Refer Bonus  <b>৳ <?=$refer_bonus->for_lvl?> Tk</b> . 4th Referer
	<br>
	 Level-5 Refer Bonus  <b>৳ <?=$refer_bonus->fif_lvl?> Tk</b> . 5th Referer 
	</h4>
</div>

<div class="row">
	<!-- //bronze -->
	<div class="col-sm-12 col-md-auto card align-items-center shadow border10 p-3 m-1">
		<img class="trophy_head shadow-sm p-1 border10" src="<?= base_url()?>/app-assets/images/level/bronze.svg" alt="">
		<h2>Bronze</h2>
		<hr class="hr-light">
		<h4 class="mt-2">Refer <b class="text-success">
				<?=$brnz_1->total_refer_member?></b> Member</h4>
		<span class="text-gray">
			| <br>
			| <br>
			| <br>
			| <br>
			| <br>
		</span>
		<img class="trophy_lvl mt-3 shadow-sm p-1 border10" src="<?= base_url()."/app-assets/images/level/".$brnz_1->reputation_milestone_icon?>"
		 alt="">
		<h4>
			<?=$brnz_1->reputation_milestone_name?>
		</h4>
		<b class="badge badge-pill badge-gradient-success font_medium_lg shadow-sm">+
			<?=$brnz_1->percentage?> % </b>
		<p>Earn from your Refered Member</p>
		<span class="text-gray">
			| <br>
			| <br>
			| <br>
			| <br>
			| <br>
		</span>
		<h4 class="mt-2">Refer <b class="text-primary">
				<?=$brnz_2->total_refer_member?></b> Member</h4>
		<span class="text-gray">
			| <br>
			| <br>
			| <br>
			| <br>
			| <br>
		</span>
		<img class="trophy_lvl mt-3 shadow-sm p-1 border10" src="<?= base_url()."/app-assets/images/level/".$brnz_2->reputation_milestone_icon?>"
		 alt="">
		<h4>
			<?=$brnz_2->reputation_milestone_name?>
		</h4>
		<b class="badge badge-pill badge-gradient-primary font_medium_lg shadow-sm">+
			<?=$brnz_2->percentage?> %</b>
		<p>Earn from your Refered Member</p>
		<span class="text-gray">
			| <br>
			| <br>
			| <br>
			| <br>
			| <br>
		</span>
		<h4 class="mt-2">Refer <b class="text-warning">
				<?=$brnz_3->total_refer_member?></b> Member</h4>
		<span class="text-gray">
			| <br>
			| <br>
			| <br>
			| <br>
			| <br>
		</span>
		<img class="trophy_lvl mt-3 shadow-sm p-1 border10" src="<?= base_url()."/app-assets/images/level/".$brnz_3->reputation_milestone_icon?>"
		 alt="">
		<h4>
			<?=$brnz_3->reputation_milestone_name?>
		</h4>
		<b class="badge badge-pill badge-gradient-warning font_medium_lg shadow-sm">+
			<?=$brnz_3->percentage?> %</b>
		<p>Earn from your Refered Member</p>
		<span class="text-gray">
			| <br>
			| <br>
			| <br>
			| <br>
			| <br>
		</span>
		<h4 class="mt-2">Refer <b class="text-danger">
				<?=$brnz_4->total_refer_member?></b> Member</h4>
		<span class="text-gray">
			| <br>
			| <br>
			| <br>
			| <br>
			| <br>
		</span>
		<img class="trophy_lvl mt-3 shadow-sm p-1 border10" src="<?= base_url()."/app-assets/images/level/".$brnz_4->reputation_milestone_icon?>"
		 alt="">
		<h4>
			<?=$brnz_4->reputation_milestone_name?>
		</h4>
		<b class="badge badge-pill badge-gradient-danger font_medium_lg shadow-sm">+
			<?=$brnz_4->percentage?>%</b>
		<p>Earn from your Refered Member</p>
	</div>
	<!-- //silver -->
	<div class="col-sm-12 col-md-auto card align-items-center shadow border10 p-3 m-1">
		<img class="trophy_head shadow-sm p-1 border10" src="<?= base_url()?>/app-assets/images/level/silver.svg" alt="">
		<h2>Silver</h2>
		<hr class="hr-light">
		<h4 class="mt-2">Refer <b class="text-success">
				<?=$silvr_1->total_refer_member?></b> Member</h4>
		<span class="text-gray">
			| <br>
			| <br>
			| <br>
			| <br>
			| <br>
		</span>
		<img class="trophy_lvl mt-3 shadow-sm p-1 border10" src="<?= base_url()."/app-assets/images/level/".$silvr_1->reputation_milestone_icon?>"
		 alt="">
		<h4>
			<?=$silvr_1->reputation_milestone_name?>
		</h4>
		<b class="badge badge-pill badge-gradient-success font_medium_lg shadow-sm">+
			<?=$silvr_1->percentage?> % </b>
		<p>Earn from your Refered Member</p>
		<span class="text-gray">
			| <br>
			| <br>
			| <br>
			| <br>
			| <br>
		</span>
		<h4 class="mt-2">Refer <b class="text-primary">
				<?=$silvr_2->total_refer_member?></b> Member</h4>
		<span class="text-gray">
			| <br>
			| <br>
			| <br>
			| <br>
			| <br>
		</span>
		<img class="trophy_lvl mt-3 shadow-sm p-1 border10" src="<?= base_url()."/app-assets/images/level/".$silvr_2->reputation_milestone_icon?>"
		 alt="">
		<h4>
			<?=$silvr_2->reputation_milestone_name?>
		</h4>
		<b class="badge badge-pill badge-gradient-primary font_medium_lg shadow-sm">+
			<?=$silvr_2->percentage?> %</b>
		<p>Earn from your Refered Member</p>
		<span class="text-gray">
			| <br>
			| <br>
			| <br>
			| <br>
			| <br>
		</span>
		<h4 class="mt-2">Refer <b class="text-warning">
				<?=$silvr_3->total_refer_member?></b> Member</h4>
		<span class="text-gray">
			| <br>
			| <br>
			| <br>
			| <br>
			| <br>
		</span>
		<img class="trophy_lvl mt-3 shadow-sm p-1 border10" src="<?= base_url()."/app-assets/images/level/".$silvr_3->reputation_milestone_icon?>"
		 alt="">
		<h4>
			<?=$silvr_3->reputation_milestone_name?>
		</h4>
		<b class="badge badge-pill badge-gradient-warning font_medium_lg shadow-sm">+
			<?=$silvr_3->percentage?> %</b>
		<p>Earn from your Refered Member</p>
		<span class="text-gray">
			| <br>
			| <br>
			| <br>
			| <br>
			| <br>
		</span>
		<h4 class="mt-2">Refer <b class="text-danger">
				<?=$silvr_4->total_refer_member?></b> Member</h4>
		<span class="text-gray">
			| <br>
			| <br>
			| <br>
			| <br>
			| <br>
		</span>
		<img class="trophy_lvl mt-3 shadow-sm p-1 border10" src="<?= base_url()."/app-assets/images/level/".$silvr_4->reputation_milestone_icon?>"
		 alt="">
		<h4>
			<?=$silvr_4->reputation_milestone_name?>
		</h4>
		<b class="badge badge-pill badge-gradient-danger font_medium_lg shadow-sm">+
			<?=$silvr_4->percentage?>%</b>
		<p>Earn from your Refered Member</p>
	</div>
	<!-- //gold -->
	<div class="col-sm-12 col-md-auto card align-items-center shadow border10 p-3 m-1">
		<img class="trophy_head shadow-sm p-1 border10" src="<?= base_url()?>/app-assets/images/level/gold.svg" alt="">
		<h2>Gold</h2>
		<hr class="hr-light">
		<h4 class="mt-2">Refer <b class="text-success">
				<?=$gld_1->total_refer_member?></b> Member</h4>
		<span class="text-gray">
			| <br>
			| <br>
			| <br>
			| <br>
			| <br>
		</span>
		<img class="trophy_lvl mt-3 shadow-sm p-1 border10" src="<?= base_url()."/app-assets/images/level/".$gld_1->reputation_milestone_icon?>"
		 alt="">
		<h4>
			<?=$gld_1->reputation_milestone_name?>
		</h4>
		<b class="badge badge-pill badge-gradient-success font_medium_lg shadow-sm">+
			<?=$gld_1->percentage?> % </b>
		<p>Earn from your Refered Member</p>
		<span class="text-gray">
			| <br>
			| <br>
			| <br>
			| <br>
			| <br>
		</span>
		<h4 class="mt-2">Refer <b class="text-primary">
				<?=$gld_2->total_refer_member?></b> Member</h4>
		<span class="text-gray">
			| <br>
			| <br>
			| <br>
			| <br>
			| <br>
		</span>
		<img class="trophy_lvl mt-3 shadow-sm p-1 border10" src="<?= base_url()."/app-assets/images/level/".$gld_2->reputation_milestone_icon?>"
		 alt="">
		<h4>
			<?=$gld_2->reputation_milestone_name?>
		</h4>
		<b class="badge badge-pill badge-gradient-primary font_medium_lg shadow-sm">+
			<?=$gld_2->percentage?> %</b>
		<p>Earn from your Refered Member</p>
		<span class="text-gray">
			| <br>
			| <br>
			| <br>
			| <br>
			| <br>
		</span>
		<h4 class="mt-2">Refer <b class="text-warning">
				<?=$gld_3->total_refer_member?></b> Member</h4>
		<span class="text-gray">
			| <br>
			| <br>
			| <br>
			| <br>
			| <br>
		</span>
		<img class="trophy_lvl mt-3 shadow-sm p-1 border10" src="<?= base_url()."/app-assets/images/level/".$gld_3->reputation_milestone_icon?>"
		 alt="">
		<h4>
			<?=$gld_3->reputation_milestone_name?>
		</h4>
		<b class="badge badge-pill badge-gradient-warning font_medium_lg shadow-sm">+
			<?=$gld_3->percentage?> %</b>
		<p>Earn from your Refered Member</p>
		<span class="text-gray">
			| <br>
			| <br>
			| <br>
			| <br>
			| <br>
		</span>
		<h4 class="mt-2">Refer <b class="text-danger">
				<?=$gld_4->total_refer_member?></b> Member</h4>
		<span class="text-gray">
			| <br>
			| <br>
			| <br>
			| <br>
			| <br>
		</span>
		<img class="trophy_lvl mt-3 shadow-sm p-1 border10" src="<?= base_url()."/app-assets/images/level/".$gld_4->reputation_milestone_icon?>"
		 alt="">
		<h4>
			<?=$gld_4->reputation_milestone_name?>
		</h4>
		<b class="badge badge-pill badge-gradient-danger font_medium_lg shadow-sm">+
			<?=$gld_4->percentage?>%</b>
		<p>Earn from your Refered Member</p>
	</div>
	<!-- //platinum -->
	<div class="col-sm-12 col-md-auto card align-items-center shadow border10 p-3 m-1">
		<img class="trophy_head shadow-sm p-1 border10" src="<?= base_url()?>/app-assets/images/level/platinum.svg" alt="">
		<h2>Platinum</h2>
		<hr class="hr-light">
		<h4 class="mt-2">Refer <b class="text-success">
				<?=$pltnm_1->total_refer_member?></b> Member</h4>
		<span class="text-gray">
			| <br>
			| <br>
			| <br>
			| <br>
			| <br>
		</span>
		<img class="trophy_lvl mt-3 shadow-sm p-1 border10" src="<?= base_url()."/app-assets/images/level/".$pltnm_1->reputation_milestone_icon?>"
		 alt="">
		<h4>
			<?=$pltnm_1->reputation_milestone_name?>
		</h4>
		<b class="badge badge-pill badge-gradient-success font_medium_lg shadow-sm">+
			<?=$pltnm_1->percentage?> % </b>
		<p>Earn from your Refered Member</p>
		<span class="text-gray">
			| <br>
			| <br>
			| <br>
			| <br>
			| <br>
		</span>
		<h4 class="mt-2">Refer <b class="text-primary">
				<?=$pltnm_2->total_refer_member?></b> Member</h4>
		<span class="text-gray">
			| <br>
			| <br>
			| <br>
			| <br>
			| <br>
		</span>
		<img class="trophy_lvl mt-3 shadow-sm p-1 border10" src="<?= base_url()."/app-assets/images/level/".$pltnm_2->reputation_milestone_icon?>"
		 alt="">
		<h4>
			<?=$pltnm_2->reputation_milestone_name?>
		</h4>
		<b class="badge badge-pill badge-gradient-primary font_medium_lg shadow-sm">+
			<?=$pltnm_2->percentage?> %</b>
		<p>Earn from your Refered Member</p>
		<span class="text-gray">
			| <br>
			| <br>
			| <br>
			| <br>
			| <br>
		</span>
		<h4 class="mt-2">Refer <b class="text-warning">
				<?=$pltnm_3->total_refer_member?></b> Member</h4>
		<span class="text-gray">
			| <br>
			| <br>
			| <br>
			| <br>
			| <br>
		</span>
		<img class="trophy_lvl mt-3 shadow-sm p-1 border10" src="<?= base_url()."/app-assets/images/level/".$pltnm_3->reputation_milestone_icon?>"
		 alt="">
		<h4>
			<?=$pltnm_3->reputation_milestone_name?>
		</h4>
		<b class="badge badge-pill badge-gradient-warning font_medium_lg shadow-sm">+
			<?=$pltnm_3->percentage?> %</b>
		<p>Earn from your Refered Member</p>
		<span class="text-gray">
			| <br>
			| <br>
			| <br>
			| <br>
			| <br>
		</span>
		<h4 class="mt-2">Refer <b class="text-danger">
				<?=$pltnm_4->total_refer_member?></b> Member</h4>
		<span class="text-gray">
			| <br>
			| <br>
			| <br>
			| <br>
			| <br>
		</span>
		<img class="trophy_lvl mt-3 shadow-sm p-1 border10" src="<?= base_url()."/app-assets/images/level/".$pltnm_4->reputation_milestone_icon?>"
		 alt="">
		<h4>
			<?=$pltnm_4->reputation_milestone_name?>
		</h4>
		<b class="badge badge-pill badge-gradient-danger font_medium_lg shadow-sm">+
			<?=$pltnm_4->percentage?>%</b>
		<p>Earn from your Refered Member</p>
	</div>
</div>

<br>
<br>
<div class="row">
	<div class="col-md-12 grid-margin stretch-card p-0 m-0">
		<div class="card border10 shadow">
			<div class="card-body p-2">
				<h3 class="text-center"><span class="text-danger"><i class="mdi mdi-diamond"></i> Pro</span> Levels</h3>
				<h4 class="text-center"><u>In <span class="text-danger">Pro</span> Level You Will Get Get Same Commission From All of Your Refer Members</u></h4>
				<div class="table-responsive">
					<table class="table display table-hover">
						<thead>
							<tr>
								<th class="text-center">Level Name</th>
								<th class="text-center">Total Refer User</th>
								<th class="text-center">Income Percentage</th>
							</tr>
						</thead>
						<tbody>
							<tr class="text-center">
								<td><img src="<?=base_url("app-assets/images/level/".$s_sprt->reputation_milestone_icon)?>"  style="border-radius:0px;height:45px;" alt=""><b><?=$s_sprt->reputation_milestone_name?></b></td>
								<td><?=$s_sprt->total_refer_member?></td>
								<td><?=$s_sprt->percentage?>%</td>
							</tr>
							<tr class="text-center">
								<td><img src="<?=base_url("app-assets/images/level/".$c_cnct->reputation_milestone_icon)?>"  style="border-radius:0px;height:45px;" alt=""> <b><?=$c_cnct->reputation_milestone_name?></b></td>
								<td><?=$c_cnct->total_refer_member?></td>
								<td><?=$c_cnct->percentage?>%</td>
							</tr>
							<tr class="text-center">
								<td><img src="<?=base_url("app-assets/images/level/".$b_build->reputation_milestone_icon)?>" style="border-radius:0px;height:45px;"  alt=""> <b><?=$b_build->reputation_milestone_name?></b></td>
								<td><?=$b_build->total_refer_member?></td>
								<td><?=$b_build->percentage?>%</td>
							</tr>
							<tr class="text-center">
								<td><img src="<?=base_url("app-assets/images/level/".$u_leader->reputation_milestone_icon)?>" style="border-radius:0px;height:45px;"  alt=""> <b><?=$u_leader->reputation_milestone_name?></b></td>
								<td><?=$u_leader->total_refer_member?></td>
								<td><?=$u_leader->percentage?>%</td>
							</tr>
							<tr class="text-center">
								<td><img src="<?=base_url("app-assets/images/level/".$legend->reputation_milestone_icon)?>"  style="border-radius:0px;height:45px;" alt=""> <b><?=$legend->reputation_milestone_name?></b></td>
								<td><?=$legend->total_refer_member?></td>
								<td><?=$legend->percentage?>%</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<br>

<div class="row">
	<div class="col-md-12 grid-margin stretch-card p-0 m-0">
		<div class="card border10 shadow">
			<div class="card-body p-2">
				<h3 class="text-center"><span class="text-danger"><i class="mdi mdi-calendar"></i>30</span> Days Challenges</h3>
				<div class="table-responsive">
					<table class="table display table-hover">
						<thead>
							<tr>
								<th class="text-center"><b>Challenges</b></th>
							</tr>
						</thead>
						<tbody>
							<tr>
							<td><h5 class="text-center">Refer <span class="badge badge-pill badge-dark">350</span> Active User in <span class="badge badge-pill badge-danger">30</span> Days and <span class="badge badge-pill text-dark badge-warning">WIN 50,000 BDT</span> Cash Bonus</h5></td></tr>
							</tr>

							<tr><td><h5 class="text-center">Refer <span class="badge badge-pill badge-dark">700</span> Active User in <span class="badge badge-pill badge-danger">30</span> Days and <span class="badge badge-pill text-dark badge-warning">WIN 85,000 BDT</span> Cash Bonus</h5></td></tr>
							<tr><td><h5 class="text-center">Refer <span class="badge badge-pill badge-dark">1200</span> Active User in <span class="badge badge-pill badge-danger">30</span> Days and <span class="badge badge-pill text-dark badge-warning">WIN 120,000 BDT</span> Cash Bonus</h5></td></tr>
							<tr><td><h4 class="text-center"><b>Life Time Plan</b></h4></td></tr>
							<tr><td><h5 class="text-center">Refer <span class="badge badge-pill badge-dark">500</span> Active User in <span class="badge badge-pill badge-danger">30</span> Days and <span class="badge badge-pill text-dark badge-warning">Monthly Salary 8,000 BDT</span> Cash Bonus</h5></td></tr>
							<tr><td><h5 class="text-center">Refer <span class="badge badge-pill badge-dark">1000</span> Active User in <span class="badge badge-pill badge-danger">30</span> Days and <span class="badge badge-pill text-dark badge-warning">Monthly Salary 15,000 BDT</span> Cash Bonus</h5></td></tr>
							<tr><td><h5 class="text-center">Refer <span class="badge badge-pill badge-dark">1500</span> Active User in <span class="badge badge-pill badge-danger">30</span> Days and <span class="badge badge-pill text-dark badge-warning">Monthly Salary 20,000 BDT</span> Cash Bonus</h5></td></tr>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>