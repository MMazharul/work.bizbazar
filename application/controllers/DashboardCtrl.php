<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class DashboardCtrl extends CI_Controller

{

	public function index()
	{

		if (chk_login()) {
			$this->load->model('Report_m');
			$this->load->model('Dashboard_m');

			$query = $this->db->query("SELECT round((Join_fee*referer_earn_percentage/100)) as refer_bonus,round((Join_fee*2nd_lvl_percentage/100)) as sec_lvl,round((Join_fee*3rd_lvl_percentage/100)) as trd_lvl,round((Join_fee*4th_lvl_percentage/100)) as for_lvl,round((Join_fee*5th_lvl_percentage/100)) as fif_lvl FROM `join_fee_income`");
        	$referer_bonus = $query->result();

			$user_id = $this->session->user_id;
			$job_ern = $this->Report_m->get_job_earn($user_id,0);
			$refer_bonus = $this->Report_m->refer_bonus_earn($user_id,0);
			$refer_ern = $this->Report_m->get_refer_work_earn($user_id,0);
			$otr_ern = $this->Report_m->other_bonus_earn($user_id,0);

			$total_ern = $this->Report_m->get_all_earn($user_id);
			$total_widrw = $this->Dashboard_m->get_total_withdraw($user_id);

			$refer_members = $this->Dashboard_m->last_5_refer($user_id);
			
			$job_time_session = $this->Dashboard_m->job_time_session();

			$last_withdraw = $this->Dashboard_m->recent_withdraw($user_id);

			$notices = $this->Dashboard_m->get_notice();

			$user_data = get_user_data($user_id);
			

			$data['job_ern'] = $job_ern[0];
			$data['refer_bonus'] = $refer_bonus[0];
			$data['refer_ern'] = $refer_ern[0];
			$data['otr_ern'] = $otr_ern[0];
			$data['user_data'] = $user_data;
			$data['total_ern'] = $total_ern[0];
			$data['total_withdraw'] = $total_widrw[0];
			$data['refer_members'] = $refer_members;
			$data['job_time_session'] = $job_time_session[0];
			$data['last_withdraws'] = $last_withdraw;
			$data['notices'] = $notices;
			$data['referer_bonus'] = $referer_bonus[0];
			$status = $this->Users_m->get_user_activation_status($user_id);



			$this->load->view('templates/1_head.php');

			$this->load->view('templates/2_nav.php');

			$this->load->view('templates/3_sidebar_menu_left.php');



			$status[0]->active_status == 0 ? $this->load->view('activate_dialogue.php') : null;

			$status[0]->active_status == 2 ? $this->load->view('activate_waiting_dialogue.php') : null;

			$this->load->view('dashboard',$data); //4_dynamic



			$this->load->view('templates/5_footer.php');

			$this->load->view('templates/6_script_end.php');


		//	$this->output->cache(10);
		}

	}



}

