<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class WithdrawCtrl extends CI_Controller
{

    public function index()
    {
        chk_login();
        $this->load->model('Withdraw_m');
        $user_id = $this->session->user_id;
        $current_total = $this->Withdraw_m->get_total_amnt($user_id);
        $work_earn = $this->Withdraw_m->get_work_ern_amnt($user_id);
        $refer_earn = $this->Withdraw_m->get_refer_ern_amnt($user_id);
        $refer_bonus_earn = $this->Withdraw_m->get_refer_bonus_amnt($user_id);
        $other_earn = $this->Withdraw_m->get_othr_ern_amnt($user_id);
        $all_withdraw = $this->Withdraw_m->get_all_withdraw($user_id);
        $withdraw_allow = $this->Withdraw_m->withdraw_allow();
        $withdraw_pending = $this->Withdraw_m->check_withdraw_rqst_pending($user_id);
        $total_withdraw = $this->Withdraw_m->total_withdrawn($user_id);
		$total_job_earn = $this->Withdraw_m->get_job_earn($user_id,99999);
        $data['current_total'] = $current_total[0];
        $data['work_earn'] = $work_earn[0];
        $data['refer_earn'] = $refer_earn[0];
        $data['refer_bonus_earn'] = $refer_bonus_earn[0];
        $data['other_earn'] = $other_earn[0];
        $data['all_withdraw'] = $all_withdraw;
        $data['withdraw_allow'] = $withdraw_allow;
        $data['withdraw_pending'] = $withdraw_pending >0 ? 1 :0;
        $data['total_withdraw'] = $total_withdraw;
        $data['total_job_earn']=$total_job_earn;

        $this->load->view('templates/1_head.php');
        $this->load->view('templates/2_nav.php');
        $this->load->view('templates/3_sidebar_menu_left.php');

        $this->load->view('withdraw', $data); //4_dynamic

        $this->load->view('templates/5_footer.php');
        $this->load->view('templates/6_script_end.php');
    }

    public function withdraw_conf()
    {
        chk_login();
        if (isset($_POST['rcv_num'])) {
            $pass = $this->input->post('pass');
            $amnt = $this->input->post('amnt');
            $this->load->model('Withdraw_m');
            $conf_pin =rand(1234,9999);
            $reslt = $this->Withdraw_m->chek_pass_balnc($this->session->user_id);
            if (password_verify($pass, $reslt[0]->password)) {
                $this->load->library('Sms_send');
                $this->sms_send->send_sms($reslt[0]->mobile_num,"Confirmation PIN: ".$conf_pin." . Your Withdraw ".$amnt." Tk Confirmation Message. Biz-Bazar");
                $this->session->conf_id = $conf_pin;
                echo json_encode(array('conf'=>'<br><div class="form-group"><label for="amnt">&nbsp 4 Digit Confirmation PIN sent in your mobile number</label><input type="Number" class="form-control" id="conf_pin" placeholder="Confirmation PIN">
                </div><button onclick="conf_snd(this)" class="btn btn-gradient-info btn-rounded btn-block" type="button">Send Request <i class="mdi mdi-chevron-right"></i><br><small>(Proceed Time 3-4 days)</small><i class="mdi mdi-sync mdi-spin loading"></i></button>'));
            }
            else {
                echo json_encode(array('conf' => '<br><span class="alert alert-danger round">Something Went Wrong ! Try again</span>'));
            }
        }
    }

    public function withdraw_req()
    {
        chk_login();

        if (isset($_POST['rcv_num'])) {

            $user_id = $this->session->user_id;
            $rcv_num = preg_replace('/(880|0|88)/A', '', $this->input->post('rcv_num'));
            $amnt = $this->input->post('amnt');
            $pass = $this->input->post('pass');
            $conf_pin = $this->input->post('conf_pin');
            $pm = $this->input->post('pay_method');

            $this->load->model('Withdraw_m');
            $reslt = $this->Withdraw_m->chek_pass_balnc($user_id);
            if (password_verify($pass, $reslt[0]->password) && $reslt[0]->total_bal >= $amnt && $this->session->conf_id==$conf_pin) {
                $this->Withdraw_m->send_withdraw_req($user_id, $rcv_num, $amnt, $pm);
              echo json_encode(array('result' => '<br><span class="alert alert-success round">Successfully Sent Request</span>','success'=>1));
            
            } else {
                echo json_encode(array('result' => '<br><span class="alert alert-danger round">Something Went Wrong ! Try again</span>'));
            }
        }
    }

    public function withdraw_histry()
    {
        chk_login();
        $this->load->model('Withdraw_m');
        $all_withdraw = $this->Withdraw_m->get_all_withdraw($this->session->user_id);
        $total_withdraw = $this->Withdraw_m->total_withdrawn($this->session->user_id);

        $data['total_withdraw'] = $total_withdraw;
        $data['all_withdraw'] = $all_withdraw;

        $this->load->view('templates/1_head.php');
        $this->load->view('templates/2_nav.php');
        $this->load->view('templates/3_sidebar_menu_left.php');

        $this->load->view('withdraw_history', $data); //4_dynamic

        $this->load->view('templates/5_footer.php');
        $this->load->view('templates/6_script_end.php');
    }
}

