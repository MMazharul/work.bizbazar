<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class ProfileCtrl extends CI_Controller

{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
    }

    public function index()

    {

        chk_login();

        $this->load->model('Users_m');

        $user_data = $this->Users_m->get_user_all_data($this->session->user_id);
        $profile = $this->db->get_where('users_profile', ['user_id' => $this->session->user_id]);
        $data['profile'] = null;
        if(isset($profile->result()[0])){
            $data['profile'] = $profile->result()[0];
        }
        
        $data['user_data'] = $user_data[0];

        $this->load->view('templates/1_head.php');

        $this->load->view('templates/2_nav.php');

        $this->load->view('templates/3_sidebar_menu_left.php');

        $this->load->view('profile', $data); //4_dynamic

        $this->load->view('templates/5_footer.php');

        $this->load->view('templates/6_script_end.php');
    }

    public function profile_view()
    {

        chk_login();

        if (isset($_GET['user_id'])) {

            $this->load->model('Users_m');

            $user_data = $this->Users_m->get_user_all_data($this->input->get("user_id"));

            $data['user_data'] = $user_data[0];

            $this->load->view('templates/1_head.php');

            $this->load->view('templates/2_nav.php');

            $this->load->view('templates/3_sidebar_menu_left.php');

            $this->load->view('profile', $data); //4_dynamic

            $this->load->view('templates/5_footer.php');

            $this->load->view('templates/6_script_end.php');
        } else {
            redirect("/");
        }
    }

    public function profile_edit()
    {
     $user_id = $this->session->user_id;
        if (isset($_POST['name'])) {
            $this->load->model("Users_m");
            $name = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 ]/', ' ', urldecode(html_entity_decode(strip_tags($_POST['name']))))));
            $mob = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 ]/', ' ', urldecode(html_entity_decode(strip_tags($_POST['mob']))))));
            $gender = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 ]/', ' ', urldecode(html_entity_decode(strip_tags($_POST['gender']))))));
            $email = trim(strip_tags($_POST['email']));
            $adress = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 ]/', ' ', urldecode(html_entity_decode(strip_tags($_POST['adress']))))));
            $nid = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 ]/', ' ', urldecode(html_entity_decode(strip_tags($_POST['nid']))))));
            $title = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 ]/', ' ', urldecode(html_entity_decode(strip_tags($_POST['title']))))));
            $skills = html_entity_decode(strip_tags($_POST['skills']));
            $desc = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 ]/', ' ', urldecode(html_entity_decode(strip_tags($_POST['desc']))))));

            if (!empty($_FILES)) {
                $picture = $this->session->user_id . ".png";
                $config['upload_path'] = './app-assets/images/members/';
                $config['allowed_types']    = 'gif|jpg|png';
                $config['file_name']        =   $picture;
                $config['overwrite']        =   true;
                $this->load->library('upload', $config);
                $this->upload->data();
                @unlink('./app-assets/images/members/' . $picture);

                if ($this->upload->do_upload('photo')) {
                    $this->Users_m->update_profile_with_pic($name, $gender, $email, $adress, $nid, $picture, $user_id);
                    print_r($this->upload->data());
                }
                print_r($this->upload->display_errors());
            }


            $checkExistanece = $this->db->get_where('users_profile', ['user_id' => $this->session->user_id]);
            if ($checkExistanece->result() == null) {
                $this->db->insert('users_profile', ['user_id' => $user_id, 'title' => $title , 'description' => $desc, 'skill' => $skills]);
            } else {
                $this->db->update('users_profile', ['title' => $title , 'description' => $desc, 'skill' => $skills] , "user_id = '$user_id'");
            }

            $this->Users_m->update_profile_no_pic($name, $gender, $email, $adress, $nid, $this->session->user_id);
        }
    }

    public function change_pass()
    {
        if (isset($_POST['c_pass'])) {
            $old_pass = $this->input->post("old_pass");
            $pass = $this->input->post("pass");
            $c_pass = $this->input->post("c_pass");

            if ($pass === $c_pass) {
                $this->load->model('Users_m');
                $user_pass = $this->Users_m->get_user_pass($this->session->user_id);
                if (password_verify($old_pass, $user_pass[0]->password)) {
                    $new_pass = password_hash($pass, PASSWORD_DEFAULT);
                    $this->Users_m->update_pass($this->session->user_id, $new_pass);
                    echo (json_encode(array("response" => '<span class="p-1 border10 bg-success">Successfully,Updated Password!</span>', "success" => "1")));
                } else {
                    echo (json_encode(array("response" => '<span class="p-1 border10 bg-danger">Old Password Not Matched</span>')));
                }
            } else {
                echo (json_encode(array("response" => '<span class="p-2 border10 bg-danger">Password Not Matched</span>')));
            }
        }
    }

    public function add_portfolio()
    {
        print_r($this->input->post('title'));
    }
}
