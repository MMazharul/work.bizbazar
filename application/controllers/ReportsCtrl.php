<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class ReportsCtrl extends CI_Controller

{



    public function job_earn_report()

    {

        chk_login();

        $this->load->model('Report_m');

        $user_id = $this->session->user_id;

        $today = $this->Report_m->get_job_earn($user_id,0);

        $last_7_day = $this->Report_m->get_job_earn($user_id,7);

        $last_30_day = $this->Report_m->get_job_earn($user_id,30);

        $total = $this->Report_m->get_job_earn($user_id,99999);



        $data['today'] = $today[0];

        $data['svn_day'] = $last_7_day[0];

        $data['thirty_day'] = $last_30_day[0];

        $data['total'] = $total[0];

        

        $this->load->view('templates/1_head.php');

        $this->load->view('templates/2_nav.php');

        $this->load->view('templates/3_sidebar_menu_left.php');



        $this->load->view('job_earn_report',$data); //4_dynamic



        $this->load->view('templates/5_footer.php');

        $this->load->view('templates/6_script_end.php');



    }



    public function refer_bounus_report()

    {

        $this->load->model('Report_m');

        $user_id = $this->session->user_id;

        $today = $this->Report_m->refer_bonus_earn($user_id,0);

        $last_7_day = $this->Report_m->refer_bonus_earn($user_id,7);

        $last_30_day = $this->Report_m->refer_bonus_earn($user_id,30);

        $total = $this->Report_m->refer_bonus_earn($user_id,99999);

        $data['today'] = $today[0];

        $data['svn_day'] = $last_7_day[0];

        $data['thirty_day'] = $last_30_day[0];

        $data['total'] = $total[0];

         

        $this->load->view('templates/1_head.php');

        $this->load->view('templates/2_nav.php');

        $this->load->view('templates/3_sidebar_menu_left.php');



        $this->load->view('refer_bonus_earn',$data); //4_dynamic



        $this->load->view('templates/5_footer.php');

        $this->load->view('templates/6_script_end.php');



    }



    public function refer_earn_report()

    {

        $this->load->model('Report_m');

        $user_id = $this->session->user_id;

        $today = $this->Report_m->get_refer_work_earn($user_id,0);

        $last_7_day = $this->Report_m->get_refer_work_earn($user_id,7);

        $last_30_day = $this->Report_m->get_refer_work_earn($user_id,30);

        $total = $this->Report_m->get_refer_work_earn($user_id,99999);

        $data['today'] = $today[0];

        $data['svn_day'] = $last_7_day[0];

        $data['thirty_day'] = $last_30_day[0];

        $data['total'] = $total[0];

         

        $this->load->view('templates/1_head.php');

        $this->load->view('templates/2_nav.php');

        $this->load->view('templates/3_sidebar_menu_left.php');



        $this->load->view('refer_work_earn_report',$data); //4_dynamic



        $this->load->view('templates/5_footer.php');

        $this->load->view('templates/6_script_end.php');



    }



    public function bounus_earn_report()

    {

        $this->load->model('Report_m');

        $user_id = $this->session->user_id;

        $today = $this->Report_m->other_bonus_earn($user_id,0);

        $last_7_day = $this->Report_m->other_bonus_earn($user_id,7);

        $last_30_day = $this->Report_m->other_bonus_earn($user_id,30);

        $total = $this->Report_m->other_bonus_earn($user_id,99999);

        $data['today'] = $today[0];

        $data['svn_day'] = $last_7_day[0];

        $data['thirty_day'] = $last_30_day[0];

        $data['total'] = $total[0];

         

         

        $this->load->view('templates/1_head.php');

        $this->load->view('templates/2_nav.php');

        $this->load->view('templates/3_sidebar_menu_left.php');



        $this->load->view('bonus_earn',$data); //4_dynamic



        $this->load->view('templates/5_footer.php');

        $this->load->view('templates/6_script_end.php');



    }

    public function balance_deduct()
    {
        if(isset($_GET['notify_id'])){
            $this->load->model('Notify_m');
            $this->Notify_m->seen_notify($_GET['notify_id']);  
        }
        
        $this->load->model('Report_m');
        $data['deduct_his']=$this->Report_m->get_balance_deduct_his($this->session->user_id);

        $this->load->view('templates/1_head.php');

        $this->load->view('templates/2_nav.php');

        $this->load->view('templates/3_sidebar_menu_left.php');

        $this->load->view('balance_removed',$data); //4_dynamic

        $this->load->view('templates/5_footer.php');

        $this->load->view('templates/6_script_end.php');
    }



}
