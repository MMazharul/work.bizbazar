<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');



class FlexiloadCtrl extends CI_Controller
{
	public function __construct() {
		parent::__construct();

		$this->load->model('common_m');
	}

	public function buy_package()
	{
		chk_login();
		$data['packages']=$this->common_m->select_with_where('flexiload_package','*','status=1');

		$this->load->view('templates/1_head.php');

		$this->load->view('templates/2_nav.php');

		$this->load->view('templates/3_sidebar_menu_left.php');

		$this->load->view('flexiload/buy_package.php',$data); //4_dynamic

		$this->load->view('templates/5_footer.php');

		$this->load->view('templates/6_script_end.php');
	}

	public function singleLoadForm()
	{
		chk_login();

		$this->load->view('templates/1_head.php');

		$this->load->view('templates/2_nav.php');

		$this->load->view('templates/3_sidebar_menu_left.php');

		$this->load->view('flexiload/single_flexiload.php'); //4_dynamic

		$this->load->view('templates/5_footer.php');

		$this->load->view('templates/6_script_end.php');
	}

	public function flexiload_store()
	{
		$this->load->library('loadapi');
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div style="font-weight:bold;display: inline-block;padding-right:5px;">', '</div>');
		$this->form_validation->set_rules('amount', 'Amount', 'required');
		$this->form_validation->set_rules('number_type', 'Number Type', 'required');
		$this->form_validation->set_rules('phone_number	', 'Mobile Number*', 'required');
		$this->form_validation->set_rules('operator	', 'Operator', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			return redirect('');
		}
		else
		{
			$this->load->view('formsuccess');
		}


		//$api_info=$this->loadapi->load_api('8801868135578',$number_type,$operator,$amount);
		//$api_info=$this->loadapi->load_api('8801868135578',1,'robi',10);
		//$data= json_decode($api_info);
		$success_code="445900";
		//echo $data->code;
		//echo $data->load_id;

	}

	public function blance()
	{
		chk_login();
		$user_id=$_SESSION['user_id'];

		$user_blance=$this->common_m->getRow("flexiload_balance",'blance','user_id',$user_id);

		$this->load->view('templates/1_head.php');

		$this->load->view('templates/2_nav.php');

		$this->load->view('templates/3_sidebar_menu_left.php');

		$this->load->view('flexiload/blance.php',compact('user_blance')); //4_dynamic

		$this->load->view('templates/5_footer.php');

		$this->load->view('templates/6_script_end.php');
	}
	public function blance_transfer()
	{
		chk_login();

		$this->load->view('templates/1_head.php');

		$this->load->view('templates/2_nav.php');

		$this->load->view('templates/3_sidebar_menu_left.php');

		$this->load->view('flexiload/blance_transfer.php'); //4_dynamic

		$this->load->view('templates/5_footer.php');

		$this->load->view('templates/6_script_end.php');
	}

	public function flexiload_fund_req()
	{
		chk_login();

		$id=$this->input->get('id');
		if(isset($id))
		{
			$data['package']=$this->common_m->select_with_where("flexiload_package",'id,package_amount,total_commission',"id='$id' and status=1");
			if($data['package']!==null)
			{
				$data['package']=$data['package'][0];

				$this->load->view('templates/1_head.php');

				$this->load->view('templates/2_nav.php');

				$this->load->view('templates/3_sidebar_menu_left.php');

				$this->load->view('flexiload/add_fund',$data); //4_dynamic

				$this->load->view('templates/5_footer.php');

				$this->load->view('templates/6_script_end.php');

			}
			else{
				redirect('/VendorCtrl/products','refresh');
			}
		}

	}
	public function fund_req_store()
	{
		chk_login();
		$this->load->library('sms_send');
		$user=$this->common_m->getRow('users_login','mobile_num','user_id',$_POST['user_id']);
		$msg="Dear Customer,Our Flexiload fund add to process time 2-3 hours please just wait and confirmation. Thank You for Biz-Bazar";

		$this->load->library('form_validation');

		foreach ($_POST as $key=>$value)
		{
			$this->form_validation->set_rules($key, $key, 'required');

		}
		if(!empty($_FILES["bank_slip"]["name"]))
		{
			$tmp_name = $_FILES["bank_slip"]["tmp_name"];
			$name = $_FILES["bank_slip"]["name"];
			move_uploaded_file($tmp_name,'uploads/payment_slip/'.$name);
			$_POST['bank_slip']=$name;
		}

		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

		$data = array('success' => false, 'messages' => array());

		if ($this->form_validation->run() == true) {
			$data['success'] = true;
			$this->common_m->insert('flexiload_fund_req', $this->input->post());
			$user == true ?  $this->sms_send->send_sms($user->mobile_num,$msg) : 'error';

		}
		else{
			foreach ($_POST as $key => $value)
			{
				  $data['messages'][$key] = form_error($key);
			}
		}
		echo json_encode($data);

	}

	public function payment_type()
	{
		$data['payment']=$this->common_m->select_all('flexiload_payment_info','*');

		$data['payment']=$data['payment'][0];

		echo json_encode($data['payment']);
	}




}
