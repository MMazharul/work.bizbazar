<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');



class ProductSellCtrl extends CI_Controller

{

    public function __construct($config = 'rest')
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        parent::__construct();
    }

    public function index()
    {
        chk_login();    

        $this->load->model('Affiliate_m');
        
        $data['all_income']=$this->Affiliate_m->all_income_history($this->session->user_id);
        $data['user_id'] = $this->session->userdata('user_id');

        $this->load->view('templates/1_head.php');

        $this->load->view('templates/2_nav.php');

        $this->load->view('templates/3_sidebar_menu_left.php');

        $this->load->view('pdt_sell',$data); //4_dynamic

        $this->load->view('templates/5_footer.php');

        $this->load->view('templates/6_script_end.php');
    }

    public function set_income()
    {
        $this->load->model('Affiliate_m');
        $this->Affiliate_m->add_affiliate_income($_GET['ui'],$_GET['pc'],$_GET['price'],  0  ,$_GET['income']);
        $this->Affiliate_m->add_affiliate_income_intotal($_GET['ui'],$_GET['income']);
        echo json_encode("success");
    }

    public function pdt_sell_history()
    {
        chk_login();    

        $this->load->model('Affiliate_m');
        
        $data['all_income']=$this->Affiliate_m->all_income_history($this->session->user_id);
        $data['user_id'] = $this->session->userdata('user_id');

        $this->load->view('templates/1_head.php');

        $this->load->view('templates/2_nav.php');

        $this->load->view('templates/3_sidebar_menu_left.php');

        $this->load->view('pdt_history',$data); //4_dynamic

        $this->load->view('templates/5_footer.php');

        $this->load->view('templates/6_script_end.php');
    }
}
