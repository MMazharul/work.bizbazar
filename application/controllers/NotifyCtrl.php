<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class NotifyCtrl extends CI_Controller

{



    public function index()

    {

        chk_login();

        if(isset($_GET['notify_id'])){
            $id = $this->input->get('notify_id');
            $this->db->query("UPDATE `notifications` SET viewed = 1 WHERE id = '$id'");
        }

        $user_id = $this->session->user_id;

        $query = $this->db->query("SELECT * from notifications WHERE target = (SELECT active_status FROM users_detail WHERE id = '$user_id') or target=3 or user_id = '$user_id' ORDER  by created_at DESC ");
        $rs = $query->result();

        $data['notifications']= $rs;


        $this->load->view('templates/1_head.php');

        $this->load->view('templates/2_nav.php');

        $this->load->view('templates/3_sidebar_menu_left.php');


        $this->load->view('notification',$data); //4_dynamic



        $this->load->view('templates/5_footer.php');

        $this->load->view('templates/6_script_end.php');



    }

    public function markall_notify_read()
    {
        $this->load->model('Notify_m');
        $this->Notify_m->seen_all_notify($this->session->user_id);
        echo json_encode(array("success"=>1));
    }


}