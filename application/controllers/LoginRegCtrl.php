<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE"); */

class LoginRegCtrl extends CI_Controller

{

	public function __construct($config = 'rest')
    {
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Headers: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        parent::__construct();
	}
	

	public function index(){
		$this->load->view('templates/1_head.php');
		$this->load->view('blocked'); //4_dynamic
		$this->load->view('templates/5_footer.php');
		$this->load->view('templates/6_script_end.php');
	}
	
	public function sign_in(){
		
		if (isset($_POST['mob']) && isset($_POST['pass'])) {
			$mob = $this->input->post('mob');
			$pass = $this->input->post('pass');
			/* get login credential */
			$rs = $this->Login_reg_m->get_login_creadentials($mob);
			if ($rs != null) {
				if (password_verify($pass, $rs[0]->password)) {
					$array = array(
						'login' => true,
						'user_name' => $rs[0]->name,
						'picture' => $rs[0]->picture,
						'user_id' => $rs[0]->user_id,
						'referer_id' => $rs[0]->referer_id,
						'job_earn' => $rs[0]->job_income,
						'referer_earn'=> $rs[0]->job_income*$rs[0]->referer_percentage /100,
						'adress' =>  $rs[0]->adress
					);
					$encrypt = base64_encode(json_encode($array));
					$log_encrypt = base64_encode(json_encode(['mob' => $mob,'pass' => $pass]));
					$rs[0]->active_status == 0 ? $this->session->activation = 0 : null;
					if ($rs[0]->active_status == 3) {
						echo json_encode(array("success" => 3));
					}
					else{
						$this->session->set_userdata($array);
						if ($this->session->has_userdata('login')) {
							echo json_encode(array("success" => 1,'_token'=>$encrypt,'name' => $rs[0]->name,'s' => $rs[0]->user_id,'log' => $log_encrypt));
						}
					}
				} else {
					echo json_encode(array("success" => 0));
				}
			} else {
				echo json_encode(array("success" => 7));
			}
		} else {
			echo json_encode(array("success" => 0));
		}
	}

	public function sign_up(){
		if (isset($_POST['name']) && isset($_POST['mob']) && isset($_POST['pass']) && isset($_POST['pass_con'])) {
			$this->load->helper(array('form'));
			$this->load->library('Sms_send');
			$this->load->library('form_validation');
			$this->form_validation->set_rules('name', 'Username', 'trim|required|min_length[3]');
			$this->form_validation->set_rules('mob', 'Mobile', 'trim|numeric|required|max_length[14]|min_length[9]');
			$this->form_validation->set_rules('pass', 'Password', 'required|min_length[4]');
			$this->form_validation->set_rules('pass_con', 'Password Confirmation', 'required|min_length[4]|matches[pass]');
			$this->form_validation->set_rules('refarer', 'Refer ID', 'trim');
			$this->form_validation->set_rules('mail', 'Email', 'trim|valid_email');
			$this->form_validation->set_rules('nid', 'National ID', 'trim|min_length[8]');
			if ($this->form_validation->run() == true) {
				$name = $this->input->post('name');
				$mob = preg_replace('/(880|0|88)/A', '', $this->input->post('mob'));
				$pass = password_hash($this->input->post('pass'), PASSWORD_DEFAULT);

				//$refarer = $this->input->post('refarer');
				$refarer = 1001;
				$mail = $this->input->post('mail');
				$nid = $this->input->post('nid');
				$db_last_unique_id = $this->Login_reg_m->get_unique_id();
				$username = trim(str_replace(' ', '', $name)) . $db_last_unique_id;
				$activation_pin = rand(11, 99) . chr(rand(98, 121)) . rand(111, 999);
				$own_refer_id = 1000 + $db_last_unique_id;
				$pic = "avater_" . rand(0, 10) . ".png";
				$refarer_id = $this->Login_reg_m->get_referer_id($refarer);
				if ($this->Login_reg_m->check_referer($refarer)) {
					if ($this->Login_reg_m->check_mobile($mob)) {
				
						$user_detail = array(
							'id' => $db_last_unique_id,
							'name' => $name,
							'gender' => null,
							'adress' => null,
							'activation_pin' => $activation_pin,
							'refer_from' => $refarer,
							'national_id' => null,
							'picture' => $pic,
							'active_status' => 0,
							'own_refer_id' => $own_refer_id,
							'acc_active_date' => null,
						);
						$user_login = array(
							"user_id" => $db_last_unique_id,
							"mobile_num" => $mob,
							"email" => $mail,
							"password" => $pass,
							"username" => $username
						);
						/* save to db users_detail table */
						$this->db->insert('users_detail', $user_detail);
						/* save to db user_login table */
						$this->db->insert('users_login', $user_login);
						$this->db->insert('total_balance', ['user_id' => $db_last_unique_id]);
						/* send sms with activation code */
						//$this->sms_send->send_sms($mob, "Welcome to Quick-Earn. To activate your account, pay our service fee and give your activation code. Your Mobile : 0" . $mob . " Secret password : " . $this->input->post('pass') . " Activation code is: " . $activation_pin . "  Please,Login Here: https://quick-earn.info");
						//set admin notify
						//$this->load->model("Notify_m");
						//$this->Notify_m->set_notify_admin("New Member", "Quick-Earn Got A New Member", "MemberCtrl/all_members", '<div class="preview-icon bg-info"><i class="mdi mdi-account-plus"></i></div>');
						//$this->Notify_m->set_notify_user("New Member", "A New Member Added in Your Reference", "ReferMembersCtrl", $refarer_id, '<div class="preview-icon bg-info"><i class="mdi mdi-account-plus"></i></div>');
						/* set session */
						$session_data = array(
							'login' => true,
							'user_id' => $db_last_unique_id,
							'activation' => 0,
							'referer_id' => $refarer_id
						);
						$this->session->set_userdata($session_data);
						if (1) {
							$log_encrypt = base64_encode(json_encode(['mob' => $mob,'pass' => $pass]));
							echo json_encode(array("success" => 1,
												   "name" => $name,
													 "log" => $log_encrypt,
													 "_token" => base64_encode("nothing-now")));
						}
					
					} else {
						echo (json_encode(array("success" => "Mobile Invalid !")));
					}
				} else {
					$pass_hash = password_hash($pass, PASSWORD_DEFAULT);
					$this->Login_reg_m->update_pass($user_mobile, $pass_hash);
					$rs = $this->Login_reg_m->get_login_creadentials($mob);
					if ($rs != null) {
						$array = array(
							'login' => true,
							'user_name' => $rs[0]->name,
							'picture' => $rs[0]->picture,
							'user_id' => $rs[0]->user_id,
							'referer_id' => $rs[0]->referer_id,
							'job_earn' => $rs[0]->job_income,
							'referer_earn'=> $rs[0]->job_income*$rs[0]->referer_percentage /100
						);
						$encrypt = base64_encode(json_encode($array));
						$log_encrypt = base64_encode(json_encode(['mob' => $mob,'pass' => $pass]));
					}
					echo json_encode(array("success" => 1,'_token'=>$encrypt,'name' => $rs[0]->name,'s' => $rs[0]->user_id,'log' => $log_encrypt));
				}
			} else {
				echo (json_encode(array("success" => validation_errors('<div class="alert alert-danger round">', '</div>'))));
			}
		} else {
			echo (json_encode(array("success" => 0)));
		}
	}

	public function forget_pass_user(){
		$this->load->library('Sms_send');
		$status = 0;
		if (isset($_POST['mobile'])) {
			$mob = preg_replace('/(880|0|88)/A', '', $this->input->post('mobile'));;
			if ($this->Login_reg_m->check_mobile($mob) == false) {
				$cod = $this->session->code = rand(111111, 999999);
				$this->sms_send->send_sms($mob, "Dear Biz-Bazar Member,Your Password reset code is: " . $cod);
				$status = 1;
			}
		}
		echo (json_encode(array("success" => $status)));
	}



	public function chk_pass_reset_code()
	{
		$status = 0;
		if ($this->session->code == $this->input->post('cod')) {
			$status = 1;
		}
		echo (json_encode(array("success" => $status)));
	}



	public function reset_new_pass()
	{

		$status = 0;

		$user_mobile = $this->input->post('mobile');

		$new_pass = $this->input->post('pass');

		if ($user_mobile != null && $new_pass != null) {

			$pass_hash = password_hash($new_pass, PASSWORD_DEFAULT);

			$this->Login_reg_m->update_pass($user_mobile, $pass_hash) ? $status = 1 : null;

		}

		echo (json_encode(array("success" => $status)));

	}



	public function logout()
	{
		logout();
	}


	public function quick_trade_login()
	{
		
		if (isset($_POST['mob']) && isset($_POST['pass'])) {
			$mob = $this->input->post('mob');
			$pass = $this->input->post('pass');
			/* get login credential */
			$rs = $this->Login_reg_m->get_login_creadentials($mob);
			if ($rs != null) {
				if (password_verify($pass, $rs[0]->password)) {
					$array = array(
						'login' => true,
						'user_name' => $rs[0]->name,
						'picture' => $rs[0]->picture,
						'user_id' => $rs[0]->user_id,
						'referer_id' => $rs[0]->referer_id,
						'job_earn' => $rs[0]->job_income,
						'referer_earn'=> $rs[0]->job_income*$rs[0]->referer_percentage /100
					);
					//$encrypt = base64_encode(json_encode($array));
					$rs[0]->active_status == 0 ? $this->session->activation = 0 : null;
					if ($rs[0]->active_status == 3) {
						echo json_encode(array("success" => 3));
					}
					else{
						$this->session->set_userdata($array);
						if ($this->session->has_userdata('login')) {
							
							header("Location: http://work.bizbazar.test/DashboardCtrl");
						}
					}
				} else {
					echo json_encode(array("success" => 0));
				}
			} else {
				echo json_encode(array("success" => 7));
			}
		} else {
			echo json_encode(array("success" => 0));
		}
	}

}

