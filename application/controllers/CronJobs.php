<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CronJobs extends CI_Controller
{

    public function reset_job_session()
    {
        $this->load->database();
        $this->db->query("TRUNCATE `jobs_viewed`");
        $this->db->query("UPDATE `work_earn_plan_distribution` SET `job_session_time`=CURRENT_TIMESTAMP + INTERVAL 12 hour WHERE id = 1");
        $this->db->query("INSERT INTO `cron_run` (`id`, `Time`) VALUES (NULL, CURRENT_TIMESTAMP)");
        $this->db->query("UPDATE `config` SET`job_allow`=1,`job_allow_time`=CURRENT_TIMESTAMP + INTERVAL 60 MINUTE WHERE id =1");

    }

    public function job_time_reset()
    {
        $this->load->database();
        $this->db->query("UPDATE `config` SET`job_allow`=0 WHERE id =1");
    }

}