<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');



class ActivateCtrl extends CI_Controller

{

    public function index()
    {
        chk_login();

        $query = $this->db->query("SELECT * FROM `join_fee_income` WHERE id = 1");
        $rs = $query->result();

        $user_id = $this->session->user_id;
        $q = $this->db->query("SELECT * FROM `activation_reqst` WHERE status=2 and user_id= '$user_id'");
        $s = $this->db->query("SELECT * FROM `users_detail` WHERE active_status=1 and id= '$user_id'");
        $r = $q->result();
        $active_status = $s->result();
        if (!$r == null || !$r == null && !$active_status == null) {
            redirect(base_url());
        } else {
            $data['join_fee_mobile'] = $rs[0];


            $this->load->view('templates/1_head.php');

            $this->load->view('templates/2_nav.php');

            $this->load->view('templates/3_sidebar_menu_left.php');

            $this->load->view('activate', $data); //4_dynamic

            $this->load->view('templates/5_footer.php');

            $this->load->view('templates/6_script_end.php');
        }


    }


    public function active_reqst()
    {

        $this->load->model('Users_m');

        if (isset($_POST['usr_id'])) {

             // update referer
            if(isset($_POST['refer_id'])){
                $this->Users_m->set_user_referer($this->input->post('refer_id'), $this->input->post('usr_id'));
            }

            $data = array(

                "user_id" => $this->input->post('usr_id'),

                "sender_num" => $this->input->post('sender_num'),

                "transection_id" => $this->input->post('tran_id'),

                'status' => 2

            );

            $pin = $this->input->post('pin');



            if ($this->Users_m->chek_activation_pin($data['user_id'], $pin)) {

                //insert activation req table

                $this->db->insert('activation_reqst', $data);

                //update user active status

                $this->Users_m->update_user_activ_status_for_waiting($data['user_id']);

                //update session

                $this->session->activation = 2;

                echo json_encode(array("success" => 1));

            } else {

                echo json_encode(array("success" => "Wrong Activation Pin !"));

            }

        }

    }

    public function resend_credential()
    {
        if (isset($_POST['id'])) {
            $this->load->model('Users_m');
            $user = $this->Users_m->get_user_mobile($_POST['id']);

            $this->load->library('Sms_send');
            $this->sms_send->send_sms($user->mobile_num, "Hello " . $user->name . ". Your Biz-Bazar Mobile Number is: 0" . $user->mobile_num . " and Activation Code: " . $user->activation_pin . "  login here : https://quick-earn.info");
            echo json_encode(array("success" => 1));
        }
    }

    public function resend_code()
    {
        if (isset($_POST['id'])) {
            $this->load->model('Users_m');
            $user = $this->Users_m->get_user_mobile($_POST['id']);

            $this->load->library('Sms_send');
            $this->sms_send->send_sms($user->mobile_num, "Hello " . $user->name . ". Your Biz-Bazar Activation Code: " . $user->activation_pin);
            echo json_encode(array("success" => 1));
        }
    }


    public function mailsend()
    {
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'rf@gmail.com', // change it to yours
            'smtp_pass' => '', // change it to yours
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => true
        );
        $this->load->library('email', $config);

        $this->email->from('arifunctg@gmail.com', 'Your Name');
        $this->email->to('quickearnltd@gmail.com');

        $this->email->subject('Email Test');
        $this->email->message('Testing the email class.');
        
        if ($this->email->send()) {
            echo 'Email sent.';
        } else {
            show_error($this->email->print_debugger());
        }
    }

}
