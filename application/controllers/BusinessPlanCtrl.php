<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class BusinessPlanCtrl extends CI_Controller

{



    public function index()

    {

        chk_login();

        $query = $this->db->query("SELECT round((Join_fee*referer_earn_percentage/100)) as refer_bonus,round((Join_fee*2nd_lvl_percentage/100)) as sec_lvl,round((Join_fee*3rd_lvl_percentage/100)) as trd_lvl,round((Join_fee*4th_lvl_percentage/100)) as for_lvl,round((Join_fee*5th_lvl_percentage/100)) as fif_lvl FROM `join_fee_income`");
        $refer_bonus = $query->result();

        function get_rank_detail($rep_lvl, $milestn_lvl)

        {

            $ci = get_instance();

            $ci->db->select("*");

            $ci->db->from("refer_earn_plan_distribution");

            $ci->db->where(array("reputation_lvl" => $rep_lvl, "milestone_lvl" => $milestn_lvl));

            $q = $ci->db->get();

            return $q->result();

        }



        $brnz_1 = get_rank_detail(1, 1);

        $brnz_2 = get_rank_detail(1, 2);

        $brnz_3 = get_rank_detail(1, 3);

        $brnz_4 = get_rank_detail(1, 4);



        $sil_1 = get_rank_detail(2, 1);

        $sil_2 = get_rank_detail(2, 2);

        $sil_3 = get_rank_detail(2, 3);

        $sil_4 = get_rank_detail(2, 4);



        $gld_1 = get_rank_detail(3, 1);

        $gld_2 = get_rank_detail(3, 2);

        $gld_3 = get_rank_detail(3, 3);

        $gld_4 = get_rank_detail(3, 4);



        $pltnm_1 = get_rank_detail(4, 1);

        $pltnm_2 = get_rank_detail(4, 2);

        $pltnm_3 = get_rank_detail(4, 3);

        $pltnm_4 = get_rank_detail(4, 4);
        
        $s_sprt = get_rank_detail(5, 0);
        $c_cnct = get_rank_detail(6, 0);
        $b_build = get_rank_detail(7, 0);
        $u_leader = get_rank_detail(8, 0);
        $legend = get_rank_detail(9, 0);


        $data['refer_bonus'] = $refer_bonus[0];


        $data['brnz_1'] = $brnz_1[0];

        $data['brnz_2'] = $brnz_2[0];

        $data['brnz_3'] = $brnz_3[0];

        $data['brnz_4'] = $brnz_4[0];



        $data['silvr_1'] = $sil_1[0];

        $data['silvr_2'] = $sil_2[0];

        $data['silvr_3'] = $sil_3[0];

        $data['silvr_4'] = $sil_4[0];



        $data['gld_1'] = $gld_1[0];

        $data['gld_2'] = $gld_2[0];

        $data['gld_3'] = $gld_3[0];

        $data['gld_4'] = $gld_4[0];



        $data['pltnm_1'] = $pltnm_1[0];

        $data['pltnm_2'] = $pltnm_2[0];

        $data['pltnm_3'] = $pltnm_3[0];

        $data['pltnm_4'] = $pltnm_4[0];

        $data['s_sprt']= $s_sprt[0];
        $data['c_cnct']= $c_cnct[0];
        $data['b_build']= $b_build[0];
        $data['u_leader']= $u_leader[0];
        $data['legend']= $legend[0];




        $this->load->view('templates/1_head.php');

        $this->load->view('templates/2_nav.php');

        $this->load->view('templates/3_sidebar_menu_left.php');



        $this->load->view('business_plan', $data); //4_dynamic



        $this->load->view('templates/5_footer.php');

        $this->load->view('templates/6_script_end.php');



    }



}