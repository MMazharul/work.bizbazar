<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ReferMembersCtrl extends CI_Controller

{



    public function index()

    {

        chk_login();


        if(isset($_GET['notify_id'])){
            $this->load->model('Notify_m');
            $this->Notify_m->seen_notify($_GET['notify_id']);  
        }

        $this->load->model('Refer_members_m');

        $data['referMembers'] = $this->Refer_members_m->get_referd_user($this->session->user_id);
        $data['inctive_referMembers'] = $this->Refer_members_m->inactive_refer($this->session->user_id);

        $this->load->view('templates/1_head.php');

        $this->load->view('templates/2_nav.php');

        $this->load->view('templates/3_sidebar_menu_left.php');



        $this->load->view('refer_members',$data); //4_dynamic



        $this->load->view('templates/5_footer.php');

        $this->load->view('templates/6_script_end.php');



    }

    public function remove_member()
    {
        if(isset($_GET['user_id'])){
            $this->load->model('Refer_members_m');
        $this->Refer_members_m->remove_member($this->input->get('user_id'));
        redirect("ReferMembersCtrl");
        }
    }






}