<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class WelcomeCtrl extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or - 
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		if(isset($_SESSION['login'])&&$_SESSION['login'] == true && isset($_SESSION['referer_id']) && isset($_SESSION['referer_id']) && isset($_SESSION['referer_earn'])) { redirect('/DashboardCtrl');} else{ redirect('http://localhost:3000/');} 
	//	$this->output->cache(10);
	}
}
