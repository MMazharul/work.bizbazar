<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class InactivePage extends CI_Controller
{

    public function index()
    {
        chk_login();
        
        $this->load->view('templates/1_head.php');
        $this->load->view('templates/2_nav.php');
        $this->load->view('templates/3_sidebar_menu_left.php');

        $this->load->view('activate_dialogue'); //4_dynamic

        $this->load->view('templates/5_footer.php');
        $this->load->view('templates/6_script_end.php');

    }

}