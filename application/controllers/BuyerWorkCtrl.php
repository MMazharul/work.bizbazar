<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');



class BuyerWorkCtrl extends CI_Controller

{

    public function index()
    {
        chk_login();
			
        $this->load->model('QW_Jobs_m');
        $data['jobs'] = $this->QW_Jobs_m->get_jobs();


        $this->load->view('templates/1_head.php');

        $this->load->view('templates/2_nav.php');

        $this->load->view('templates/3_sidebar_menu_left.php');

        $this->load->view('buyer_work', $data); //4_dynamic

        $this->load->view('templates/5_footer.php');

        $this->load->view('templates/6_script_end.php');
    }

    public function set_income()
    {
        $this->load->model('Affiliate_m');
        $this->Affiliate_m->add_affiliate_income($_GET['ui'], $_GET['pc'], $_GET['price'], $_GET['prcnt'], $_GET['income']);
        echo "success";
    }

    public function job_view()
    {
        chk_login();

        $job_id = base64_decode($this->input->get('i'));
        $this->load->model('QW_Jobs_m');
        $data['job'] = $this->QW_Jobs_m->get_job_single($job_id);
        $data['bids'] = $this->QW_Jobs_m->get_bids($job_id, $this->session->user_id);
        $data['own_bid'] = $this->QW_Jobs_m->check_own_bid($job_id, $this->session->user_id);
        $data['hire_or_not'] = $this->QW_Jobs_m->check_hireOrNot($job_id, $this->session->user_id);


        $this->load->view('templates/1_head.php');
        $this->load->view('templates/2_nav.php');
        $this->load->view('templates/3_sidebar_menu_left.php');
        $this->load->view('qw_job_view', $data); //4_dynamic
        $this->load->view('templates/5_footer.php');
        $this->load->view('templates/6_script_end.php');
    }

    public function save_bid()
    {
        chk_login();

        $job_id = $this->input->post('job_id');
        $worker_id = $this->session->user_id;
        $rate = $this->input->post('rate');
        $cv = $this->input->post('cv');

        $this->load->model('QW_Jobs_m');
        $this->QW_Jobs_m->save_bid($job_id, $worker_id, $rate, $cv);
        redirect('/BuyerWorkCtrl/job_view?i=' . base64_encode($job_id));
    }

    public function edit_bid()
    {
        chk_login();

        $job_id = $this->input->post('job_id');
        $bid_id = $this->input->post('bid_id');
        $rate = $this->input->post('rate');
        $cv = $this->input->post('cv');

        $this->load->model('QW_Jobs_m');
        $this->QW_Jobs_m->edit_bid($bid_id, $rate, $cv);
        redirect('/BuyerWorkCtrl/job_view?i=' . base64_encode($job_id));
    }

    public function del_bid()
    {
        chk_login();

        $job_id = base64_decode($this->input->get('b_id'));
        $this->load->model('QW_Jobs_m');
        $this->QW_Jobs_m->del_bid($job_id);

        redirect($_SERVER['HTTP_REFERER']);
    }

    public function hired_contacts()
    {
        chk_login();

        $this->load->model('QW_Jobs_m');
        $data['hired_works'] = $this->QW_Jobs_m->hired_for_contract($this->session->user_id);

        $this->load->view('templates/1_head.php');
        $this->load->view('templates/2_nav.php');
        $this->load->view('templates/3_sidebar_menu_left.php');
        $this->load->view('qw_hired_work', $data); //4_dynamic
        $this->load->view('templates/5_footer.php');
        $this->load->view('templates/6_script_end.php');
    }

    public function contact_view()
    {
        chk_login();
        $hire_id = base64_decode($this->input->get('i'));

        if (isset($hire_id[0])) {
            $this->load->model('QW_Jobs_m');
            $hired_worker = $this->QW_Jobs_m->get_hired_workers($hire_id);
            $data['hired'] = $hired_worker[0];
            $data['job'] = $this->QW_Jobs_m->get_job_single($hired_worker[0]->job_id);
            $data['chats'] = $this->QW_Jobs_m->get_chats($hired_worker[0]->id);

            $this->load->view('templates/1_head.php');
            $this->load->view('templates/2_nav.php');
            $this->load->view('templates/3_sidebar_menu_left.php');
            $this->load->view('qw_contact_view', $data); //4_dynamic
            $this->load->view('templates/5_footer.php');
            $this->load->view('templates/6_script_end.php');
        } else {
            return redirect('/BuyerWorkCtrl/hired_contacts');
        }
    }

    public function chat_save()
    {
        $chat = $this->input->post('chat');
        $hired_id = $this->input->post('hiredId');
        $worker_id = $this->session->user_id;
        if ($chat != null && $hired_id != null) {
            $this->db = $this->load->database('qw_db', True);
            $this->db->insert('chats', ['hired_workerS_id' => $hired_id, 'workers_id' => $worker_id, 'chat' => $chat]);
        }
    }
}
