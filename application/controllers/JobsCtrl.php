<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class JobsCtrl extends CI_Controller
{
    public function __construct($config = 'rest')
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        parent::__construct();
    }

    public function index()
    {
        chk_login();
        $this->load->model('Jobs_m');
        $data['jobs'] =  $this->Jobs_m->get_jobs_link($this->session->user_id);
        $data['job_click_amnt'] =  $this->Jobs_m->get_job_click_amount();
        $data['job_allow'] =  $this->Jobs_m->job_allow();
        $data['job_limit'] =  $this->Jobs_m->check_if_jobLimit($this->session->user_id);
        $this->load->view('templates/1_head.php');
        $this->load->view('templates/2_nav.php');
        $this->load->view('templates/3_sidebar_menu_left.php');
        $this->load->view('jobs', $data); //4_dynamic
        $this->load->view('templates/5_footer.php');
        $this->load->view('templates/6_script_end.php');

        // $this->output->cache(10);
    }

    public function viewed_jobs()
    {
        if (isset($_POST['user_id'])) {
            // for quick earn
            $user_id = $this->input->post('user_id');
            $job_earn = $this->session->job_earn;
            $referer_id = $this->session->referer_id;
            $referer_earn = $this->session->referer_earn;
            $job_id = $this->input->post('job_id');
            
            //for blog view job
           /*  $user_id = $this->input->post('user_id');
            $job_id = $this->input->post('job_id');
            $job_earn = $this->input->post('job_earn');
            $referer_earn = $this->input->post('referer_earn');
            $referer_id = $this->input->post('referer_id'); */
            
            $this->Jobs_m->job_viewed($job_id, $user_id, $job_earn, $referer_id, $referer_earn);
            $this->Jobs_m->set_viewed_job($user_id,$job_id);
            $this->Jobs_m->set_work_earn($user_id);
            $this->Jobs_m->update_work_bal($user_id);
            $this->Jobs_m->set_referer_work_earn($user_id);
            $this->Jobs_m->update_refer_work_bal($user_id);
            echo json_encode(array('success' => 1));
            
        }
    }
}
