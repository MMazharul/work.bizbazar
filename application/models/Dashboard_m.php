<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Dashboard_m extends CI_Model

{

    public function __construct()

    {

        $this->load->database();

    }

    public function get_today_earn($user_id)
    {

        $query = $this->db->query("SELECT
        COUNT(a.amount) AS work_earn,
        COUNT(b.amount) AS refer_work_earned,
        COUNT(c.amount) AS refer_bonus,
        COUNT(d.amount) AS other_earned
    FROM
        work_earned a,
        refer_work_earned b,
        refer_earned c,
        other_earned d
    WHERE
        a.user_id = '$user_id' AND
        b.user_id = '$user_id' AND
        c.user_id = '$user_id' AND
        d.user_id = '$user_id' AND
        a.created_at = curdate()+'%' AND
        b.created_at = curdate()+'%' AND
        c.created_at = curdate()+'%' AND
        d.created_at = curdate()+'%' ");

        return $query->result();

    }

    public function get_total_withdraw($user_id)
    {
        $query = $this->db->query("SELECT sum(amount) as total_w FROM `withdraw` WHERE user_id = '$user_id' AND status=1");
        return $query->result();
    }

    public function last_5_refer($user_id)
    {
        $query = $this->db->query("SELECT id,name,picture,acc_active_date ,(CASE when active_status=0 THEN 'Pending' WHEN active_status=1 THEN 'Active' WHEN active_status = 2 THEN 'Pending' end) as status FROM `users_detail` WHERE refer_from = (SELECT own_refer_id FROM users_detail WHERE id = '$user_id')  ORDER BY join_date DESC LIMIT 5");
        return $query->result();
    }

    public function job_time_session()
    {
        $query = $this->db->query("SELECT `job_session_time` FROM `work_earn_plan_distribution`");
        return $query->result();
    }

    public function get_notice()
    {
        $query = $this->db->query("SELECT * FROM `notice` WHERE status = 1");
        return $query->result();
    }

    public function recent_withdraw($user_id)
    {
        $query = $this->db->query("SELECT paid_total_amount,updated_at, (case WHEN status = 0 THEN 'Pending' WHEN status = 1 THEN 'Paid' WHEN status = 2 THEN 'Rejected' END) as status FROM `withdraw` WHERE user_id='$user_id' ORDER BY updated_at DESC LIMIT 3");
        return $query->result();
    }

}