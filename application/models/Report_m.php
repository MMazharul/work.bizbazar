<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_m extends CI_Model

{

    public function __construct()

    {

        $this->load->database();

    }



    public function get_job_earn($user_id,$time)
    {
        $qury = $this->db->query("SELECT COUNT(id) as total_job, sum(amount) as total_earn FROM work_earned WHERE user_id='$user_id'  AND created_at >= curdate()+'%' - INTERVAL '$time' day AND created_at <= CURDATE()+'%' + INTERVAL 1 DAY");

        return $qury->result();

    }



    public function get_refer_work_earn($user_id,$time)

    {

        $qury = $this->db->query("SELECT COUNT(id) as total_job, sum(amount) as total_earn FROM refer_work_earned WHERE user_id='$user_id'  AND created_at >= curdate()+'%' - INTERVAL '$time' day AND created_at <= CURDATE()+'%' + INTERVAL 1 DAY");

        return $qury->result();

    }



    public function other_bonus_earn($user_id,$time)

    {

        $qury = $this->db->query("SELECT COUNT(id) as total_job, sum(amount) as total_earn FROM other_earned WHERE user_id='$user_id'  AND created_at >= curdate()+'%' - INTERVAL '$time' day AND created_at <= CURDATE()+'%' + INTERVAL 1 DAY");

        return $qury->result();

    }



    public function refer_bonus_earn($user_id,$time)

    {

        $qury = $this->db->query("SELECT COUNT(id) as total_job, sum(amount) as total_earn FROM refer_earned WHERE user_id='$user_id'  AND created_at >= curdate()+'%' - INTERVAL '$time' day AND created_at <= CURDATE()+'%' + INTERVAL 1 DAY");

        return $qury->result();

    }

    public function get_all_earn($user_id)

    {

        $qury = $this->db->query("SELECT
        (SELECT SUM(amount) FROM work_earned WHERE user_id = '$user_id') AS work_earn,
        (SELECT SUM(amount) FROM refer_work_earned WHERE user_id = '$user_id') AS refer_commission,
        (SELECT SUM(amount) FROM refer_earned WHERE user_id = '$user_id') AS refer_earn,
        (SELECT SUM(amount) FROM other_earned WHERE user_id = '$user_id') AS other_earn");

        return $qury->result();

    }

    public function get_balance_deduct_his($user_id)

    {

        $qury = $this->db->query("SELECT * FROM `balance_deduct` WHERE user_id ='$user_id' ORDER BY created_at DESC");

        return $qury->result();

    }


}
