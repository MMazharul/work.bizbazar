<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_m extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function set_user_referer($referer_id,$user_id){
        $this->db->set('refer_from', $referer_id);
        $this->db->where('id', $user_id);
        $this->db->update('users_detail');
    }

    public function chek_activation_pin($user_id, $pin)
    {
        $this->db->select('*');
        $this->db->from('users_detail');
        $this->db->Where(array('id' => $user_id, 'activation_pin' => $pin));
        $query = $this->db->get();
        return $query->num_rows() > 0 ? true : false;
    }

    public function update_user_activ_status_for_waiting($user_id)
    {
        $this->db->set('active_status', 2);
        $this->db->where('id', $user_id);
        $this->db->update('users_detail');
    }

    public function get_user_activation_status($user_id)
    {
        $query = $this->db->query("SELECT `active_status` FROM `users_detail` WHERE id='$user_id'");
        return $query->result();
    }

    public function get_user_mobile($user_id)
    {
        $query = $this->db->query("SELECT a.`mobile_num`,b.name,b.activation_pin FROM `users_login` a, users_detail b WHERE b.id=a.user_id AND a.user_id='$user_id'");
        $r = $query->result();
        return $r[0];
    }

    public function get_user_pass($user_id)
    {
        $query = $this->db->query("SELECT `password` FROM `users_login` WHERE `user_id` = '$user_id'");
        return $query->result();
    }
    public function update_pass($user_id, $pass)
    {
        $query = $this->db->query("UPDATE `users_login` SET`password`='$pass' WHERE `user_id` ='$user_id'");
    }

    public function update_profile_no_pic($name, $gender,$mail, $adress, $nid,  $user_id)
    {

        $qury = $this->db->query("UPDATE `users_detail` SET `name`='$name' ,`gender`='$gender' ,`adress`='$adress' ,`national_id`='$nid' WHERE id = '$user_id'");
        $qury = $this->db->query("UPDATE `users_login` SET `email`= '$mail' WHERE user_id='$user_id'");

    }
    public function update_profile_with_pic($name, $gender,$mail, $adress, $nid, $picture, $user_id)
    {

        $qury = $this->db->query("UPDATE `users_detail` SET `name`='$name' ,`gender`='$gender' ,`adress`='$adress' ,`national_id`='$nid' ,`picture`='$picture' WHERE id = '$user_id'");
        $qury = $this->db->query("UPDATE `users_login` SET `email`= '$mail' WHERE user_id='$user_id'");

    }

    public function get_user_all_data($user_id)
    {
        $query = $this->db->query("SELECT
        a.name,
        a.gender,
        a.adress,
        a.national_id,
        a.picture,
        a.active_status,
        a.own_refer_id,
        a.acc_active_date,
        b.mobile_num,
        b.email,
        c.reputation_lvl,
        c.milestone_lvl,
        c.total_refered,
        d.total_refer_member,
        d.percentage,
        d.reputation_milestone_name,
        d.reputation_icon,
        d.milestone_icon,
        d.reputation_milestone_icon,
        e.refer_earn_amount,
        e.work_earn_amount,
        e.refer_work_earn_amount,
        e.other_earn_amount,
        (e.refer_earn_amount+e.work_earn_amount+e.refer_work_earn_amount+e.other_earn_amount) as total_bal
        
        FROM
        
        users_detail a,
        users_login b,
        user_lvl c,
        refer_earn_plan_distribution d,
        total_balance e
        
        WHERE 
        a.id = '$user_id' AND
        b.user_id = '$user_id' AND
        c.user_id = '$user_id' AND
        d.reputation_lvl = (SELECT reputation_lvl FROM user_lvl WHERE user_id = '$user_id') AND
        d.milestone_lvl = (SELECT milestone_lvl FROM user_lvl WHERE user_id = '$user_id') AND
        e.user_id = '$user_id'
    ");

        $rs = $query->result();

        if ($rs == null) {
            $query = $this->db->query("SELECT
            a.name,
            a.gender,
            a.adress,
            a.national_id,
            a.picture,
            a.active_status,
            a.own_refer_id,
            a.acc_active_date,
            b.mobile_num,
            b.email,
            c.other_earn_amount,
            c.refer_earn_amount,
            c.work_earn_amount,
            c.refer_work_earn_amount,
            (c.refer_earn_amount+c.work_earn_amount+c.refer_work_earn_amount+c.other_earn_amount) as total_bal,
            (1) as reputation_lvl,
            (1) as milestone_lvl,
            (0) as total_refered,
            (0) as total_refer_memeber,
            (3) as percentage,
            ('Bronze Basic') as reputation_milestone_name,
            ('bronze.svg') as reputation_icon,
            ('1.svg') as milestone_icon,
            ('bronze_1.svg') as reputation_milestone_icon
            
            FROM 
            
            users_detail a,
            users_login b,
            total_balance c
            
            WHERE 
            c.user_id = '$user_id' AND
            a.id = '$user_id' AND
            b.user_id = '$user_id'");
             $rs = $query->result();
        }


        return $rs;
    }

}