<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Refer_members_m extends CI_Model

{

    public function __construct()

    {

        $this->load->database();

    }


    public function get_referd_user($user_id)

    {

        $query = $this->db->query("SELECT
        a.id,
        a.name,
        a.picture,
        (CASE a.active_status
            WHEN 0 THEN 'Inactive'
             WHEN 1 THEN 'Active'
            WHEN 2 THEN 'Pending'
            END) as active_status,
        a.acc_active_date,
        b.referer_percentage,
        d.reputation_milestone_name,
        d.reputation_milestone_icon
    FROM
        `users_detail` a,
        refer_network b,
        user_lvl c,
        refer_earn_plan_distribution d
    WHERE
        a.id = b.user_id AND
        b.referer_id = '$user_id' AND
        d.reputation_lvl = c.reputation_lvl AND
        d.milestone_lvl = c.milestone_lvl AND
        c.user_id = b.user_id");



        return $query->result();

    }
    public function inactive_refer($user_id)
    {
        $query = $this->db->query("SELECT id,name,picture,acc_active_date ,(CASE when active_status=0 THEN 'Inactive' WHEN active_status=1 THEN 'Active' WHEN active_status = 2 THEN 'Pending' end) as status FROM `users_detail` WHERE refer_from = (SELECT own_refer_id FROM users_detail WHERE id = '$user_id') and active_status=0 or refer_from = (SELECT own_refer_id FROM users_detail WHERE id = '$user_id') and active_status=2 ORDER BY join_date DESC");
        return $query->result();
    }

    public function remove_member($user_id)
    {
        $this->db->query("DELETE FROM `users_detail` WHERE id = '$user_id'");
        $this->db->query("DELETE FROM `users_login` WHERE `user_id` = '$user_id'");
        $this->db->query("DELETE FROM `activation_reqst` WHERE `user_id`= '$user_id'");

    }


}