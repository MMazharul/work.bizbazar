<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Jobs_m extends CI_Model

{

    public function __construct()

    {

        $this->load->database();
    }

    public function get_job_click_amount()

    {

        $qury = $this->db->query("SELECT `earn_per_link` FROM `work_earn_plan_distribution`");

        return $qury->result();
    }

    public function job_allow()
    {

        $qury = $this->db->query("SELECT `job_allow`, `job_allow_time` FROM `config`");

        $r = $qury->result();
        return $r[0];
    }

    public function get_jobs_link($user_id)

    {

        $qury = $this->db->query("SELECT * FROM jobs a WHERE NOT EXISTS (SELECT * from jobs_viewed b WHERE  b.user_id='$user_id' AND a.id=b.jobs_id ) and id <= IFNULL((SELECT job_limit FROM job_limit WHERE user_id='$user_id'),99999)");

        return $qury->result();
    }

    public function check_if_jobLimit($user_id)

    {

        $qury = $this->db->query("SELECT id FROM job_limit WHERE user_id='$user_id'");

        return $qury->result();
    }


    public function set_viewed_job($user_id, $job_id)

    {
        $uniq = $user_id . "-" . $job_id;

        $qury = $this->db->query("INSERT INTO `jobs_viewed`(`user_id`, `jobs_id`,`unique_userID_jobID`) VALUES ('$user_id','$job_id','$uniq')");
    }



    public function set_work_earn($user_id)

    {

        $qury = $this->db->query("INSERT INTO `work_earned`(`user_id`, `amount`) SELECT a.id as user_id ,b.earn_per_link FROM users_detail a, work_earn_plan_distribution b WHERE a.id = '$user_id'");
    }



    public function set_referer_work_earn($user_id)

    {

        $qury = $this->db->query("INSERT INTO `refer_work_earned`(`user_id`,`from_user_id`, `amount`) SELECT (SELECT referer_id from refer_network WHERE user_id='$user_id') as user_id,'$user_id', (a.earn_per_link*b.referer_percentage/100) as percent FROM work_earn_plan_distribution a, refer_network b WHERE b.user_id='$user_id'");
    }



    public function update_work_bal($user_id)

    {

        $qury = $this->db->query("UPDATE `total_balance` SET `work_earn_amount`= work_earn_amount + (SELECT earn_per_link FROM work_earn_plan_distribution),`updated_at`=CURRENT_TIMESTAMP WHERE user_id='$user_id'");
    }



    public function update_refer_work_bal($user_id)

    {

        $qury = $this->db->query("UPDATE `total_balance` SET `refer_work_earn_amount`= refer_work_earn_amount + ((SELECT referer_percentage from refer_network WHERE user_id='$user_id')*(SELECT earn_per_link from work_earn_plan_distribution)/100),`updated_at`=CURRENT_TIMESTAMP WHERE user_id=(SELECT referer_id from refer_network WHERE user_id='$user_id')");
    }

    public function job_viewed($job_id,$user_id, $job_earn ,$referer_id, $referer_earn)
    {
        $uniq = $user_id . "-" . $job_id;
        $this->db->trans_start();
        $this->db->query("INSERT INTO `jobs_viewed`(`user_id`, `jobs_id`,`unique_userID_jobID`) VALUES ('$user_id','$job_id','$uniq')");
        $this->db->query("INSERT INTO `work_earned`(`user_id`, `amount`) VALUES ('$user_id','$job_earn')");
        $this->db->query("UPDATE `total_balance` SET `work_earn_amount`= work_earn_amount + '$job_earn',`updated_at`=CURRENT_TIMESTAMP WHERE user_id='$user_id'");
        $this->db->query("INSERT INTO `refer_work_earned`(`user_id`,`from_user_id`, `amount`) values ($referer_id,$user_id,$referer_earn)");
        $this->db->query("UPDATE `total_balance` SET `refer_work_earn_amount`= refer_work_earn_amount + '$referer_earn',`updated_at`=CURRENT_TIMESTAMP WHERE user_id='$referer_id'");
        $this->db->trans_complete();
    }
}

