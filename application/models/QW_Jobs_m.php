<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class QW_Jobs_m extends CI_Model

{

    public function __construct()
    {
        $this->db = $this->load->database('qw_db', True);
    }


    public function get_jobs()
    {
        $qury = $this->db->query("SELECT a.id,a.client_id,a.title,b.name as cat_name,a.budget,a.skills,a.job_desc,a.files,a.exp_date,a.created_at FROM `job_posts` a , categories b WHERE a.category_id=b.id order by a.created_at desc");

        return $qury->result();
    }
    public function get_job_single($id)
    {
        $qury = $this->db->query("SELECT a.id,a.client_id,a.title,b.name as cat_name,a.budget,a.skills,a.job_desc,a.files,a.exp_date,a.created_at FROM `job_posts` a , categories b WHERE a.category_id=b.id and a.id='$id'");

        return $qury->result();
    }
    public function save_bid($job_id,$worker_id,$rate,$cv)
    {
        $qury = $this->db->query("INSERT INTO `bids`(`job_id`, `worker_id`, `amount`, `cv`, `created_at`) VALUES ('$job_id','$worker_id','$rate','$cv',CURRENT_TIMESTAMP)");
    }

    public function edit_bid($bid_id,$rate,$cv)
    {
        $qury = $this->db->query("UPDATE `bids` SET `amount`='$rate' ,`cv`='$cv' ,`updated_at`= CURRENT_TIMESTAMP WHERE id = '$bid_id'");
    }

    public function get_bids($job_id,$worker_id)
    {
        $qury = $this->db->query("SELECT a.id,a.job_id,a.worker_id,b.name,b.picture,a.amount,a.cv,a.created_at FROM bizba_worker.bids a, bizba_affiliate.users_detail b WHERE a.worker_id = b.id and a.job_id = '$job_id'  and not a.worker_id = '$worker_id'");

        return $qury->result();
    }

    public function check_own_bid($job_id,$worker_id)
    {
        $qury = $this->db->query("SELECT a.id,a.job_id,a.worker_id,b.name,b.picture,a.amount,a.cv,a.created_at FROM bizba_worker.bids a, bizba_affiliate.users_detail b WHERE a.worker_id = b.id and a.job_id = '$job_id'  and a.worker_id = '$worker_id'");

        return $qury->result();
    }

    public function check_hireOrNot($job_id,$worker_id)
    {
        $qury = $this->db->query("SELECT * FROM `hired_workers` WHERE job_id = '$job_id'  and worker_id = '$worker_id'");

        return $qury->result();
    }

    public function hired_for_contract($worker_id)
    {
        $qury = $this->db->query("SELECT a.id,a.job_id,c.title,c.budget,b.amount,a.created_at,a.status FROM bizba_worker.hired_workers a,bizba_worker.bids b ,bizba_worker.job_posts c WHERE a.job_id = c.id AND a.bid_id = b.id AND a.worker_id = '$worker_id'");
        return $qury->result();
    }

    public function get_hired_workers($hire_id)
    {
        $qury = $this->db->query("SELECT a.id,a.job_id,a.client_id,b.name as c_name,a.worker_id,c.name as w_name,a.bid_id,d.amount,a.paid,a.status,a.created_at FROM bizba_worker.hired_workers  a,bizba_worker.users b,bizba_affiliate.users_detail c,bizba_worker.bids d WHERE a.client_id = b.id and a.worker_id = c.id and a.bid_id = d.id AND a.id = '$hire_id'");
        return $qury->result();
    }

    public function get_chats($hire_id)
    {
        $qury = $this->db->query("SELECT * FROM `chats` WHERE hired_workers_id = '$hire_id' ORDER BY  created_at DESC");
        return $qury->result();
    }

    public function del_bid($id)
    {
        $this->db->delete('bids', array('id' => $id));
    }
}
