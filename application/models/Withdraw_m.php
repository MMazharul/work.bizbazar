<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Withdraw_m extends CI_Model

{

    public function __construct()
    {
        $this->load->database();
    }

    public function withdraw_allow()

    {

        $qury = $this->db->query("SELECT `withdraw_allow` FROM `config`");

        $r= $qury->result();
        return $r[0];
    }
    public function check_withdraw_rqst_pending($user_id)
    {
        $qury = $this->db->query("SELECT count(id) as total FROM `withdraw` WHERE user_id='$user_id' AND status=0");
 
        $r= $qury->result();
        return $r[0]->total;
    }

    public function total_withdrawn($user_id)
    {

        $qury = $this->db->query("SELECT sum(paid_total_amount) as total FROM `withdraw` WHERE user_id = '$user_id'");

        $r= $qury->result();
        return $r[0];
    }

    public function get_total_amnt($user_id)

    {

        $qury = $this->db->query("SELECT (`refer_earn_amount`+`work_earn_amount`+`refer_work_earn_amount`+`other_earn_amount`) as total FROM `total_balance` WHERE `user_id`='$user_id'");

        return $qury->result();

    }



    public function get_work_ern_amnt($user_id)

    {

        $qury = $this->db->query("SELECT `work_earn_amount` FROM `total_balance` WHERE `user_id`='$user_id'");

        return $qury->result();

    }



    public function get_refer_ern_amnt($user_id)
    {

        $qury = $this->db->query("SELECT `refer_work_earn_amount` FROM `total_balance` WHERE `user_id`='$user_id'");

        return $qury->result();

    }


    public function get_refer_bonus_amnt($user_id)

    {

        $qury = $this->db->query("SELECT `refer_earn_amount` FROM `total_balance` WHERE `user_id`='$user_id'");

        return $qury->result();

    }

    public function get_othr_ern_amnt($user_id)
    {

        $qury = $this->db->query("SELECT `other_earn_amount` FROM `total_balance` WHERE `user_id`='$user_id'");

        return $qury->result();

    }



    public function chek_pass_balnc($user_id)

    {

        $qury = $this->db->query("SELECT a.password,a.mobile_num, (b.refer_earn_amount+b.work_earn_amount+b.refer_work_earn_amount+b.other_earn_amount) as total_bal from users_login a, total_balance b WHERE a.user_id = '$user_id' and b.user_id='$user_id'");

        return $qury->result();

    }



    public function send_withdraw_req($user_id,$rcv_num,$amount,$pay_method)

    {

        $qury = $this->db->query("INSERT INTO `withdraw`(`user_id`,`receive_number`, `amount`,`pay_method`) VALUES ('$user_id','$rcv_num','$amount','$pay_method')");

    }



    public function get_all_withdraw($user_id)

    {

        $qury = $this->db->query("SELECT

        b.id,

        d.mobile_num,

        b.user_id,

        b.admin_snd_num,

        b.transection_id,

        b.paid_total_amount,
        b.paid_work_amnt,
        b.paid_refer_work_amnt,
        b.paid_refer_bon_amnt,
        b.paid_other_amnt,
        
        b.updated_at,

        a.name,

        a.picture,

        b.receive_number,

        b.amount,

        b.pay_method,

        b.created_at,

        (CASE  WHEN b.status = 0 THEN 'Pending'

        WHEN b.status = 1 THEN 'Paid'

        WHEN b.status = 2 THEN 'Rejected' END)as status,

        ( c.refer_earn_amount + c.work_earn_amount + c.refer_work_earn_amount + c.other_earn_amount

        ) AS user_total_bal,

        c.refer_earn_amount,

        c.work_earn_amount,

        c.refer_work_earn_amount,

        c.other_earn_amount

    FROM

        users_detail a,

        withdraw b,

        total_balance c,

        users_login d

    WHERE

        a.id = b.user_id AND c.user_id = b.user_id AND d.user_id = b.user_id AND b.user_id = '$user_id'
        ORDER BY b.created_at DESC");

        return $qury->result();

    }

	public function get_job_earn($user_id,$time)
	{
		$qury = $this->db->query("SELECT COUNT(id) as total_job, sum(amount) as total_earn FROM work_earned WHERE user_id='$user_id'  AND created_at >= curdate()+'%' - INTERVAL '$time' day AND created_at <= CURDATE()+'%' + INTERVAL 1 DAY");

		return $qury->row();

	}

    

    



}
