<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_reg_m extends CI_Model

{

    public function __construct()

    {

        $this->load->database();

    }



    public function check_referer($refer_id)

    {

        $this->db->select('*');

        $this->db->from('users_detail');

        $this->db->Where(array('own_refer_id' => $refer_id));

        $query = $this->db->get();

        return $query->num_rows() > 0 ? true : false;

    }



    public function check_email($mail){

        $this->db->select('*');

        $this->db->from('users_login');

        $this->db->Where(array('email' => $mail));

        $query = $this->db->get();

        return $query->num_rows() > 0 ? false : true;

    }



    public function check_mobile($mob){

        $this->db->select('*');

        $this->db->from('users_login');

        $this->db->Where(array('mobile_num' => $mob));

        $query = $this->db->get();

        return $query->num_rows() > 0 ? false : true;

    }



    public function check_nid($nid){

        $this->db->select('*');

        $this->db->from('users_detail');

        $this->db->Where(array('national_id' => $nid));

        $query = $this->db->get();

        return $query->num_rows() > 0 ? false : true;

    }



    public function get_unique_id(){

      $qury =  $this->db->query("SELECT max(id)+1 as unique_id FROM `users_detail`");

      $rslt = $qury->result();

      return $rslt[0]->unique_id; 

    }



    public function get_login_creadentials($mob){

        $query = $this->db->query("SELECT a.user_id, a.password,b.name,b.picture,b.adress, b.active_status, c.referer_id, c.referer_percentage , d.earn_per_link as job_income
        from users_login a 
         LEFT JOIN (SELECT id,name,picture,adress, active_status FROM users_detail) b
            on a.user_id=b.id
         LEFT JOIN (SELECT user_id,referer_id,referer_percentage FROM refer_network) c
             on a.user_id = c.user_id
         LEFT JOIN (SELECT id , earn_per_link from work_earn_plan_distribution ) d 
             on d.id = 1
    WHERE a.mobile_num='$mob'");

        return $query->result();

    }

    public function get_referer_id($own_refer_id){

        $query = $this->db->query("SELECT id FROM `users_detail` WHERE own_refer_id = '$own_refer_id'");

        $r = $query->result();
        return $r[0]->id;
    }



    public function update_pass($user_mobile,$new_pass){

        return $this->db->query("UPDATE `users_login` SET `password`='$new_pass' WHERE `mobile_num` ='$user_mobile'");

    }







}