<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Common_m extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}


	public function insert($table,$data)
	{
		if ($this->db->insert($table,$data)) {
			return true;
		} else {
			return false;
		}
	}
	public function get_insert_id($table,$data)
	{
		if ($this->db->insert($table,$data)) {
			$insertId = $this->db->insert_id();
			return  $insertId;

		} else {
			return false;
		}

	}


	public function delete($table,$column,$value)
	{
		if ($this->db->delete($table, [$column => $value])) {
			return true;
		}
		return false;
	}

	public function select_with_where($table,$select,$condition)
	{
		$this->db->select($select);
		$this->db->where($condition);
		$q = $this->db->get($table);

		if ($q->num_rows() > 0) {
			foreach (($q->result()) as $row) {
				$data[] = $row;
			}
			return $data;
		}

	}

	public function getRow($table,$select,$column_name,$value)
	{
		$this->db->select($select);
		$q = $this->db->get_where($table, [$column_name => $value]);
		if ($q->num_rows() > 0) {
			return $q->row();
		}
		return false;
	}
	public function updateData($table,$column,$value, $data)
	{
		$this->db->where($column,$value);
		if ($this->db->update($table, $data)) {
			return true;
		} else {
			return false;
		}
	}

	public function select_all($table,$select)
	{
		$this->db->select($select);
		$q = $this->db->get($table);

		if ($q->num_rows() > 0) {
			foreach (($q->result()) as $row) {
				$data[] = $row;
			}
			return $data;
		}

	}



}

/* End of file pts_model.php */
/* Location: ./application/models/pts_types_model.php */
