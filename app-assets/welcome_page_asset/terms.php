<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <title>Quick Earn</title>
    <link rel="icon" type="image/png" href="./img/16px.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="./img/32px.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="./img/48px.png" sizes="96x96" />
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet" />
    <!-- Your custom styles (optional) -->
    <link href="css/mystyle.css" rel="stylesheet" />
  </head>

  <body style="height: auto;">
    <header>
      <nav>
        <div class="link-group">
          <a class="link font-weight-bold" href="../../">HOME</a>
          <a class="link font-weight-bold" href="../../#about-us">ABOUT</a>
          <a class="link font-weight-bold" href="../../#contact">CONTACT</a>
        </div>

        <a href=""><img id="logo-img-quick" src="./img/quick.png" /></a>
        <!-- <a href=""><img class="logo-img" src="img/QE.png"/></a> -->
        <a href=""><img class="logo-img" src="./img/500px.gif" /></a>
        <a href=""><img id="logo-img-earn" src="./img/earn.png" /></a>

      </nav>

      <div id="sidebar">
        <div id="close-btn">
          <a href="#" class="link"><i class="fa fa-close fa-2x"></i></a>
        </div>
        <hr class="hr-dark" />

        <a class="font-weight-bold " href="../index.php">HOME</a>
        <a class="font-weight-bold " href="../index.php">ABOUT</a>
        <a class="font-weight-bold " href="../index.php">CONTACT</a>


        <div class="media-link">
          <a class="font-weight-bold " href=""><i class="fa fa-facebook link fa-2x"></i></a>
          <a class="font-weight-bold " href=""><i class="fa fa-twitter link fa-2x"></i></a>
          <a class="font-weight-bold " href=""><i class="fa fa-linkedin link fa-2x"></i></a>
          <a class="font-weight-bold " href=""><i class="fa fa-youtube-play link fa-2x"></i></a>
        </div>
      </div>
    </header>

    <br>
    <br>
    <br>
    <br>
    <br>

    <div class="item">
      <div class="content-section">
        <div class="container">
          <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
              <div class="sec-right">
                <h2>Privacy</h2>
                <div class="sec-right-text">
                  <p>
                    Quick-Earm provides its services to you with following Terms of Service ("TOS"): Thanks for
                    using our services. Being a member of Quick-Earm, you agree to abide and be bound by these
                    Terms and conditions. The terms and conditions may be modified or updated from time to time without
                    any prior notice to you. We strongly encourage you to periodically visit these terms of service, so
                    that you can be aware of them all time. To use Quick-Earm, you must be legally competent to
                    enter a binding agreement. If you do not agree with any of these terms, you should not activate
                    your Quick-Earm account and should not use this website for any longer.
                  </p>
                  <h4><b>General rules for registered members:</b></h4>
                  <ol>
                    <li>Only Bangladeshi Citizens are allowed to be a registered member.</li>
                    <li>One validate person is not allowed to have more than one account and is not permitted to use
                      multiple accounts in a single device. If so, your account will be suspended without any prior
                      notice.</li>
                    <li>You must complete or up to date your required profile information. Incomplete profile will not
                      be verified and as a result your withdrawal will be pending. </li>
                    <li>A member should be aged 18+. If you are under 18 years, we need permission from your guardians
                      to allow you on Quick-Earm.</li>
                    <li>A user is extremely prohibited to use ad blocking software during working time.</li>
                    <li>If anyone does harmful anything to Quick-Earm and does anything outside of our working
                      and marketing plan, his or her account will be suspended.</li>
                    <li>Company will debit your balance, if you violate our working process or do any kind of invalid
                      activities.</li>
                    <li>You are mostly responsible for any kind of invalid or harmful activity of your direct hand
                      referrals. </li>
                    <li>Quick-Earm account activation fee is 1000 BDT.</li>
                    <li>After a successful registration, an account can’t be moved or removed.</li>
                    <li>After sending account activation request, it takes time up to 12 hours to process and withdraw
                      processing time is up to 24 hours. If the time is crossed, you should contact to customer care
                      immediately. </li>
                    <li>From your every withdrawal, 2% will be deducted as service charge and 5% will be deposited to
                      your provident fund. You can withdraw your provident fund minimum after two years without any
                      interest. If you withdraw your provident fund after 10 years, you will receive your amount with
                      interest. </li>
                    <li>If you are suspended once, you will no longer access to your account and you will lose your
                      provident fund.</li>
                    <li>We will verify your identity according to your profile information. If anyone fails to prove
                      identity or if anyone own two or more accounts, both or all accounts will be suspended.</li>
                    <li>If your withdrawal goes to unknown for your wrong information, company will not bear the
                      responsibility. </li>
                    <li>You have to inform your complain or problem about transaction to customer care within 24 hours.
                      After 24 hours, your request will not be granted. </li>
                    <li>You should contact to customer care 10am – 10pm for any kind of customer support.</li>
                    <li>If you have more important issue, you can submit your mail to support center.</li>
                    <li>Company have full rights to change or modify the rules, term and conditions, working process,
                      income plan and marketing plan at any time.</li>
                  </ol>
                  <h4><b>Conditions to use this:</b></h4>
                  <p>
                    You understand and agree that you only can use our presentation for any legal benefit. But we are
                    not liable for any illegal activity by using our presented information. You have no right to
                    destroy, reproduce, duplicate, sell, resell or exploit any portion of the Service or access to the
                    Service for any commercial purposes. Using our Services does not give you ownership of any
                    intellectual property rights in our Services or the content you access. You may not use content
                    from our Services unless you obtain permission from its owner or are otherwise permitted by law.
                  </p>
                  <h4><b>Copyright infringements:</b></h4>
                  <p>
                    You acknowledge and agree that the content on Quick-Earm and its design, programming,
                    structure and compilation are protected by copyrights, trademarks or other proprietary rights and
                    laws. It is very easy to copy things in cyberspace but not necessarily always acceptable or legal.
                    The registered logo and all other its trademarks are owned by www.Quick-Earms.com. You agree
                    not to use or communicate said trademarks or logo without our written consent. However and by
                    exception, you may reproduce its logo on your website in order to link to Gogolweb.com. The above
                    Terms of Service are the entire agreement between 'you' and Quick-Earm'.
                  </p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
              <div class="sec-left">
                <img style="border-radius: 20px" id="windmil" class="hover-shadow" src="./img/terms.png" alt="Quick-Earn"
                  width="380px">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <br />
    <br />
    <br />
    <footer>
      <img id="windmil" src="./img/windmill.png" alt="" />
      <img src="./img/footer.svg" alt="" />

      <div id="footer">
        <div class="footer-media">
          <a class="font-weight-bold " href=""><i class="fa fa-facebook  "></i></a>
          <a class="font-weight-bold " href=""><i class="fa fa-twitter  "></i></a>
          <a class="font--bold " href=""><i class="fa fa-linkedin "></i></a>
          <a class="font-weight-bold " href=""><i class="fa fa-youtube-play "></i></a>
        </div>

        <div id="footer-link">
          <small>© Copyrights 2018 | All Right Reserved |
            <a href="./terms.php">Terms & Conditions</a> | <a href="./privacy.php">Privacy</a></small>
        </div>
      </div>
    </footer>

    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="./js/jquery-3.3.1.min.js"></script>

    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="./js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="./js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="./js/mdb.min.js"></script>
    <script type="text/javascript" src="./js/myscript.js"></script>

  </body>

</html>
