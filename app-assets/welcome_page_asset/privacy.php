<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <title>Quick Earn</title>
    <link rel="icon" type="image/png" href="./img/16px.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="./img/32px.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="./img/48px.png" sizes="96x96" />
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet" />
    <!-- Your custom styles (optional) -->
    <link href="css/mystyle.css" rel="stylesheet" />
  </head>

  <body style="height: auto;">
    <header>
      <nav>
        <div class="link-group">
          <a class="link font-weight-bold" href="../../">HOME</a>
          <a class="link font-weight-bold" href="../../#about-us">ABOUT</a>
          <a class="link font-weight-bold" href="../../#contact">CONTACT</a>
        </div>

        <a href=""><img id="logo-img-quick" src="./img/quick.png" /></a>
        <!-- <a href=""><img class="logo-img" src="img/QE.png"/></a> -->
        <a href=""><img class="logo-img" src="./img/500px.gif" /></a>
        <a href=""><img id="logo-img-earn" src="./img/earn.png" /></a>

      </nav>

      <div id="sidebar">
        <div id="close-btn">
          <a href="#" class="link"><i class="fa fa-close fa-2x"></i></a>
        </div>
        <hr class="hr-dark" />

        <a class="font-weight-bold " href="../index.php">HOME</a>
        <a class="font-weight-bold " href="../index.php">ABOUT</a>
        <a class="font-weight-bold " href="../index.php">CONTACT</a>


        <div class="media-link">
          <a class="font-weight-bold " href=""><i class="fa fa-facebook link fa-2x"></i></a>
          <a class="font-weight-bold " href=""><i class="fa fa-twitter link fa-2x"></i></a>
          <a class="font-weight-bold " href=""><i class="fa fa-linkedin link fa-2x"></i></a>
          <a class="font-weight-bold " href=""><i class="fa fa-youtube-play link fa-2x"></i></a>
        </div>
      </div>
    </header>

    <br>
    <br>
    <br>
    <br>
    <br>
 
    <div class="item">
      <div class="content-section">
        <div class="container">
          <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
              <div class="sec-right">
                <h2>Privacy</h2>
                <div class="sec-right-text">
                  <p>
                    QUICK-EARN TECHNOLOGIES LIMITED (“<b>SELF EMPLOYMENTS</b>”) provides this Privacy Policy to
                    let you know our policies and procedures regarding the collection, use and disclosure of
                    information through <a href="https://www.Quick-Earns.com/"><b>Quick-Earns</b></a> (the “<b>Site</b>”),
                    and any other websites, features, applications, widgets or online services that are owned or
                    controlled by QUICK-EARN and that post a link to this Privacy Policy (together with the Site,
                    the “<b>Service</b>”), as well as any information QUICK-EARN collects outside of the Site in
                    connection with the Service or where links to this Privacy Policy. It also describes the choices
                    available to you regarding the use of, your access to, and how to update and correct your personal
                    information.
                  </p>
                  <p>
                    By accessing or using the Service, you consent to the information collection, disclosure and use
                    practices described in this Privacy Policy. Please note that certain features or services
                    referenced in this Privacy Policy may not be offered on the Service at all times. Please also
                    review our <b>Terms of Service</b>, which governs your use of the Service.
                  </p>
                  <p>
                    We’ve provided short summaries in this Privacy Policy to help you understand what information we
                    collect, how we use it, and what choices you have. While these summaries help explain some of the
                    concepts in a simple and clear way. We encourage you to read the entire Privacy Policy to
                    understand our data practices.
                  </p>
                  <h4><b>1. INFORMATION COLLECTION</b></h4>
                  <b>Information You Provide to Us</b>
                  <p>When you use the Service, you may provide us with information about you. This may include your
                    name and contact information, financial information to make or receive payment for services
                    obtained through the SELF EMPLOYMENTS. When you use the Service, we may also collect information
                    related to your use of the Service and aggregate this with information about other users. This
                    helps us improve our Services for you. You may also provide us with information about your contacts
                    or friends if, for example, you’d like to add those contacts to a message room. Agencies may also
                    provide us with information about Freelancers associated with the Agency.</p>
                  <ul>
                    <li><b>Personal Information:</b> In the course of using the Service (whether as a Client or
                      Freelancer), we may require or otherwise collect information that identifies you as a specific
                      individual and can be used to contact or identify you (“<b>Personal Information</b>”). Examples
                      of Personal Information include your name, email address, company address, billing address, and
                      phone number.</li>
                    <li><b>Payment Information:</b> If you use the Service to make or receive payments, we will also
                      collect certain payment information, such as Bank account, credit card, bKash, Rocket or other
                      financial account information, and billing address.</li>
                    <li><b>Identity Verification:</b> We may collect Personal Information, such as your date of birth
                      or taxpayer identification number, to validate your identity or as may be required by law, such
                      as to complete tax filings. We may request documents to verify this information, such as a copy
                      of your government-issued identification or photo or a billing statement.</li>
                    <li><b>General Audience Service:</b> The Service is general audience and intended for users 18 and
                      older of Bangladeshi citizens. We do not knowingly collect Personal Information from anyone
                      younger than age 18. If we become aware that a child younger than 18 has provided us with
                      Personal Information, we will use commercially reasonable efforts to delete such information from
                      our files. If you are the parent or legal guardian of a child younger than age 18 and believe
                      that QUICK-EARN has collected Personal Information from your child, please contact us
                      immediately. </li>
                    <li><b>Non-Identifying Information or Usernames:</b> We also may collect other information, such as
                      zip codes, demographic data, information regarding your use of the Service, and general
                      project-related data (“<b>Non-Identifying Information</b>”). We may aggregate information
                      collected from QUICK-EARN registered and non-registered users (“<b>QUICK-EARN users</b>”).
                      We consider usernames to be Non-Identifying Information.</li>
                  </ul>
                  <b>Information Received from Third Parties</b>
                  <p>Third parties may also provide us information about you. If we combine that information with
                    information about you collected through the Service, we will still treat that combined information
                    as set forth in this Privacy Policy.</p>
                  <p><b>QUICK-EARN and its partners use cookies or similar technologies to analyze trends,
                      administer the website, track users’ movement around the website, and to gather demographic
                      information about our user base as a whole. The technology used to collect information
                      automatically from QUICK-EARN Users may include the following:</b></p>
                  <ul>
                    <li><b>Cookies:</b> Like many websites, we and our marketing partners, affiliates, analytics, and
                      service providers use “cookies” to collect information. A cookie is a small data file that we
                      transfer to your computer’s hard disk for record-keeping purposes. We use both persistent cookies
                      that remain on your computer or similar device (such as to save your registration ID and login
                      password for future logins to the Service and to track your compliance with the SELF EMPLOYMENTS
                      Terms of Service) and session ID cookies, which expire at the end of your browser session.</li>
                    <li><b>Web Beacons:</b> QUICK-EARN and our marketing partners, affiliates, analytics, and
                      service providers may also employ software technology known as “web beacons” and/or “tracking
                      tags” to help us keep track of what content on our Service is effective and to serve relevant
                      advertising to you.</li>
                    <li><b>Embedded Scripts:</b> We and our marketing partners, affiliates, analytics, and service
                      providers may also employ software technology known as an Embedded Script.</li>
                  </ul>
                  <p>In addition, we and our marketing partners, affiliates, analytics, and service providers may use a
                    variety of other technologies (such as tags) that collect similar information for security and
                    fraud detection purposes and we may use third parties to perform these services on our behalf.</p>
                  <b>HOW WE RESPOND TO DO NOT TRACK SIGNALS</b>
                  <p>QUICK-EARN does not respond to Do-Not-Track signals.</p>
                  <b>Feedback</b>
                  <p>We collect feedback from QUICK-EARN Users about their experience with other SELF EMPLOYMENTS
                    Users of our Service. Please note that any feedback you provide via the Service or feedback
                    provided about you is publicly viewable via the Service. On very rare occasions, we may remove
                    feedback pursuant to the relevant provisions of our Terms of Service.</p>
                  <h4><b>2. USE AND RETENTION OF INFORMATION</b></h4>
                  <p>We use information collected during the Service to provide and improve the Service, process your
                    requests, prevent fraud, provide you with information and advertising that may interest you, comply
                    with the law, and as otherwise permitted with your sanction.</p>
                  <h4><b>3. INFORMATION SHARING AND DISCLOSURE</b></h4>
                  <p>We may share information about you to provide the Services, for legal and investigative purposes,
                    in connection with sweepstakes and promotions, or if we are part of a merger or acquisition. We may
                    also share non-identifying information with third parties. You have choices as to whether we share
                    your personal information with third parties for their own marketing purposes.</p>
                  <h4><b>4. THIRD PARTY ANALYTICS PROVIDERS, AD SERVERS AND SIMILAR THIRD PARTIES</b></h4>
                  <p>We may work with advertising agencies and vendors who use technology to help us understand how
                    people use our Site. These vendors may use technologies to serve you advertisements that may
                    interest you. You can choose to opt out of receiving interest-based advertising.</p>
                  <h4><b>5. YOUR CHOICES AND OPTING OUT</b></h4>
                  <i>You have certain choices regarding how we may communicate with you.</i><br><br>
                  <p>Registered QUICK-EARN Users may update their choices regarding the types of communications
                    you receive from us through your online account. You also may opt-out of receiving marketing emails
                    from us by following the opt-out instructions provided in those emails. Please note that we reserve
                    the right to send you certain communications relating to your account or use of the Service (for
                    example, administrative and service announcements) via email and other means and these
                    transactional account messages may be unaffected if you opt-out from receiving marketing
                    communications. </p>
                  <h4><b>6. CHANGING YOUR INFORMATION OR CLOSING YOUR ACCOUNT</b></h4>
                  <p>You may request access to all personal information we have about you. You may also delete your
                    account and/or request deletion of all personal information we have about you. We will honor the
                    request to the extent we can reasonably do so, but some information will remain on the Services,
                    such as information you posted publicly.</p>
                  <h4><b>7. SECURITY</b></h4>
                  <i>We take a number of steps to protect your data, but no security is guaranteed.</i><br><br>
                  <p>QUICK-EARN takes commercially reasonable steps to help protect and secure the information it
                    collects and stores about QUICK-EARN Users. All access to the Site is encrypted using
                    industry-standard transport layer security technology (TLS). When you enter sensitive information
                    (such as tax identification number), we encrypt the transmission of that information using secure
                    socket layer technology (SSL). We also use HTTP strict transport security to add an additional
                    layer of protection for our QUICK-EARN Users. But remember that no method of transmission
                    over the Internet, or method of electronic storage, is 100% secure. Thus, while we strive to
                    protect your personal data, QUICK-EARN cannot ensure and does not warrant the security of any
                    information you transmit to us.</p>
                  <h4><b>8. LINKS TO OTHER SITES</b></h4>
                  <p>Our Service contains links to other websites. If you choose to click on a third party link, you
                    will be directed to that third party’s website. The fact that we link to a website is not an
                    endorsement, authorization or representation of our affiliation with that third party, nor is it an
                    endorsement of their privacy or information security policies or practices. We do not exercise
                    control over third party websites. These other websites may place their own cookies or other files
                    on your computer, collect data or solicit Personal Information from you. We encourage you to read
                    the privacy policies or statements of the other websites you visit.</p>
                  <h4><b>9. CHANGES TO THIS POLICY</b></h4>
                  <i>We may change this Privacy Policy. If we make material changes, we will provide notice.</i><br><br>
                  <p>QUICK-EARN may update this Privacy Policy at any time and any changes will be effective upon
                    posting. In the event that there are material changes to the way we treat your Personal
                    Information, we will display a notice through the Services prior to the change becoming effective.
                    We may also notify you by email, in our discretion. However, we will use your Personal Information
                    in a manner consistent with the Privacy Policy in effect at the time you submitted the information,
                    unless you consent to the new or revised policy.<p>
                      <h4><b>10. CONTACTING US</b></h4>
                      <p>If you have any questions about this Privacy Policy, please <b>contact us</b>.<p>


                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
              <div class="sec-left">
                <img style="border-radius: 20px" id="windmil" class="hover-shadow" src="./img/lock.jpg" alt="Quick-Earn" width="380px">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <br />
    <br />
    <br />
    <footer>
      <img id="windmil" src="./img/windmill.png" alt="" />
      <img src="./img/footer.svg" alt="" />

      <div id="footer">
        <div class="footer-media">
          <a class="font-weight-bold " href=""><i class="fa fa-facebook  "></i></a>
          <a class="font-weight-bold " href=""><i class="fa fa-twitter  "></i></a>
          <a class="font--bold " href=""><i class="fa fa-linkedin "></i></a>
          <a class="font-weight-bold " href=""><i class="fa fa-youtube-play "></i></a>
        </div>

        <div id="footer-link">
          <small>© Copyrights 2018 | All Right Reserved |
            <a href="./terms.php">Terms & Conditions</a> | <a href="./privacy.php">Privacy</a></small>
        </div>
      </div>
    </footer>

    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="./js/jquery-3.3.1.min.js"></script>

    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="./js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="./js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="./js/mdb.min.js"></script>
    <script type="text/javascript" src="./js/myscript.js"></script>

  </body>

</html>
