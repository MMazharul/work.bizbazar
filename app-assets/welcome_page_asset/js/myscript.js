$('#bar').on('click', () => {
	$('#sidebar').css("right", 0);
});
$('#close-btn').on('click', () => {
	$('#sidebar').css("right", '-260px');
});
$("section,footer").on('click', () => {
	$('#sidebar').css("right", '-260px');
});

window.onscroll = () => {
	if ($(window).width() > 830) {
		if (window.scrollY > 130) {
			$('nav').css({
				"height": "60px",
				"box-shadow": "0px 0px 5px 0px rgba(0,0,0,0.17)",
				"-moz-box-shadow": "0px 0px 5px 0px rgba(0,0,0,0.17)",
				"-webkit-box-shadow": "0px 0px 5px 0px rgba(0,0,0,0.17)"
			});
			$('.logo-img').css({
				"height": "85px",
			});
		} else {
			$('nav').css({
				"height": "80px",
				"box-shadow": "none"
			});
			$('.logo-img').css({
				"height": "95px",
			});
		}
	}
}


let layer1 = $('#layer1');
let layer2 = $('#layer2');
let layer2_c = $('#layer2-copy');
let layer3 = $('#layer3');
let scne = $('#scene');
let layer12 = $('.layer12');
// let head = $('#heading');

scne.mousemove((e) => {
	let x = (e.pageX * -1 / 12);
	let y = (e.pageY * -1 / 12);

	layer1.css({
		'transform': `translate3d(${x*-1}px,${y}px,0)`
	});

	layer2.css({
		'transform': `translate3d(${x}px,${y*-.1}px,0)`
	});

	layer2_c.css({
		'transform': `translate3d(${x}px,${y*-.1}px,0)`
	});

	layer3.css({
		'transform': `translate3d(${x*1.5}px,0px,0)`
	});

	layer12.css({
		'transform': `translate3d(${x*12}px,0px,0)`
	});

	/* head.css({
	    'transform' : `translate3d(${x+50}px,${y+50}px,0)`
	}); */

})

