'use strict';

function send_chat() {
    let chat = document.getElementById('chat')
    let pic = document.getElementById('mem_picture').value
    if (chat.innerHTML != "" && chat.innerHTML != " ") {
        let hiredId = document.getElementById('hired_id').value
        let recentChat = document.getElementById('recent_chat')
        recentChat.innerHTML = `
            <div class="row mt-2">
            <div class="col-12 col-md-6">
            <div class="d-flex align-items-center">
                    <img class="bid-profile-img" src="${window.location.origin}/app-assets/images/members/${pic}"
                    alt="profile image">
                    <div class="d-flex" style="flex-direction:column">
                    &nbsp; You
                    <small class="text-gray ">&nbsp; Worker</small>
                    </div>
            </div>
            <div class="border20 bg-white p-2 mt-2">${chat.innerHTML.trim()}</div>
            </div>
            </div>` + recentChat.innerHTML;

        $.ajax({
            type: 'post',
            url: window.location.origin + '/BuyerWorkCtrl/chat_save',
            data: {
                chat: chat.innerHTML,
                hiredId: hiredId
            },
            success: function(r) {
                console.log(33);
            }
        })
        chat.innerHTML = ""
    }
}

document.getElementById('send-btn').addEventListener('click', () => {
    send_chat()
})


document.getElementById('chat').addEventListener('keydown', e => {
    if (e.keyCode === 13 && e.shiftKey === false) {
        //e.preventDefault()
        // document.execCommand('insertHTML', false, '<br>');
        //e.target.innerHTML += '<br><br>'  
    }
    else if (e.keyCode === 13 && e.shiftKey === true) {
        e.preventDefault()
        send_chat()
    }
})