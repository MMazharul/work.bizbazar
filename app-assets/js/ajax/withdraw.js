
function set_data_modal(e) {
	$('#name').html($(e).data('name'));
	$('#status').html($(e).data('status'));
	$('#req-amount').html($(e).data('req-amount'));
	$('#paid-amount').html($(e).data('paid-amount'));
	$('#admin-snd-num').html($(e).data('admin-snd-num'));
	$('#tran-id').html($(e).data('tran-id'));
	$('#checked-at').html($(e).data('checked-at'));
	$('#requested-at').html($(e).data('requested-at'));
	$('#rcv-num').html($(e).data('rcv-num'));
	$('#pay-method').html($(e).data('pay-method'));
	$('#job-earn').html($(e).data('job-earn'));
	$('#refer-work-amnt').html($(e).data('refer-work-amnt'));
	$('#refer-bon-amnt').html($(e).data('refer-bon-amnt'));
	$('#other_amnt').html($(e).data('other_amnt'));
}

$('#withdraw').on('submit', function (e) {
	e.preventDefault();
	let rcv_num = $('#rcv_num').val();
	let amnt = $('#amnt').val();
	let pass = $('#pass').val();
	let pay_m = document.getElementById('agent').checked ? 'bkash-agent' : 'bkash-personal';

	let data = {
		rcv_num: rcv_num,
		amnt: amnt,
		pass: pass,
		pay_method: pay_m
	};
	if (rcv_num != null && amnt != null && pass != null && pay_m != null) {
		$('.loading').show();
		$.ajax({
			type: "post",
			url: "https://" + window.location.hostname + "/WithdrawCtrl/withdraw_conf",
			data: data,
			dataType: "json",
			success: function (response) {
				$('#conf').html(response.conf);
				$('.loading').hide();
			}
		});
	}
});

function conf_snd(e) {
	$(e).css('pointer-event', 'none');
	let rcv_num = $('#rcv_num').val();
	let amnt = $('#amnt').val();
	let pass = $('#pass').val();
	let pin = $('#conf > div > #conf_pin').val();
	let pay_m = document.getElementById('agent').checked ? 'bkash-agent' : 'bkash-personal';
	let data = {
		rcv_num: rcv_num,
		amnt: amnt,
		pass: pass,
		pay_method: pay_m,
		conf_pin: pin
	};
	if (rcv_num != null && amnt != null && pass != null && pay_m != null) {
		$('.loading').show();
		$.ajax({
			type: "post",
			url: "https://" + window.location.hostname + "/WithdrawCtrl/withdraw_req",
			data: data,
			dataType: "json",
			success: function (response) {
				$('#result').html(response.result);
				if (response.success == 1) {
					$('#snd').hide();
				}
				$('.loading').hide();

			}
		});
	}
}