/* author:md-arif-un;
email:arifunctg@gmail.com
linkedin: /in/arif-un */

const main_url = "http://"+window.location.hostname+"/index.php/";

var $loading = $('.loading');
$loading.hide();
//Attach the event handler to any element
$(document).ajaxStart(function () {
		//ajax request went so show the loading image
		$loading.show();
		$("button").attr("disabled",true);
	})
	.ajaxStop(function () {
		//got response so hide the loading image
		$loading.hide();
		$("button").attr("disabled",false);
	});

$("#login_form").submit(e => {
	e.preventDefault();
	let mob = $("#login-mobile-no").val();
	let pass = $("#login-password").val();
	let data = {
		mob: mob,
		pass: pass
	};
	if (mob != null && pass != null) {
		$.ajax({
			type: "post",
			url: main_url + "LoginRegCtrl/sign_in",
			data: data,
			dataType: "json",
			success: function (response) {
				if (response.success == 1) {
					$("#login_form_error").html(
						`<span class="alert alert-success round">Successfull, Loading Dashboard...</span>`
					);
					window.location.href = main_url + "DashboardCtrl";
				}
				else if(response.success == 3){
					window.location.href = main_url + "loginRegCtrl";
				}
				else {
					$("#login_form_error").html(
						`<span class="alert alert-danger round">Mobile Number or Password Wrong !</span>`
					);
					setTimeout(() => {
						$("#login_form_error").html(``);
					}, 5000);
				}
			}
		});
	} else {
		setTimeout(() => {}, 5000);
	}
});


$("#register_form").submit(e => {
	e.preventDefault();

	let name = $("#reg_name").val();
	let mobb = $("#reg_num").val();
	let mobl = mobb.replace(/(^880|^0|^88)/g, "");
	let pass = $("#reg_password").val();
	let pass_con = $("#reg_conpass").val();
	let refarer = $("#reg_referer").val();
	let mail = $("#reg_email").val();
	let nid = $("#reg_nid").val();
	let data = {
		name: name,
		mob: mobl,
		pass: pass,
		pass_con: pass_con,
		refarer: refarer,
		mail: mail,
		nid: nid
	};

	if (mobl.length == 10) {
		if (pass === pass_con) {
			if (
				name != null &&
				mobl != null &&
				pass != null &&
				pass_con != null &&
				refarer != null &&
				mail != null &&
				nid != null
			) {
				$.ajax({
					type: "post",
					url: main_url + "LoginRegCtrl/sign_up",
					data: data,
					dataType: "json",
					success: function (response) {
						if (response.success == 1) {
							$("#register_form_error").html(
								`<span class="alert alert-success round">Successfull, Loading Dashboard...</span>`
							);
							window.location.href = main_url + "DashboardCtrl";
						} else if (response.success == 0) {
							$("#register_form_error").html(
								`<span class="alert alert-danger round">Provide Valid Info !</span>`
							);
							setTimeout(() => {
								$("#register_form_error").html(``);
							}, 5000);
						} else {
							$("#register_form_error").html(
								`<span class="alert alert-danger round">${
									response.success
								}</span>`
							);
							setTimeout(() => {
								$("#register_form_error").html(``);
							}, 5000);
						}
					}
				});
			} else {
				$("#register_form_error").html(
					`<span class="alert alert-danger round">Please,Give all valid information !</span>`
				);
				setTimeout(() => {
					$("#register_form_error").html(``);
				}, 3000);
			}
		} else {
			$("#register_form_error").html(
				`<span class="alert alert-danger round">Password not matched !</span>`
			);
			setTimeout(() => {
				$("#register_form_error").html("");
			}, 3000);
		}
	} else {
		$("#register_form_error").html(
			`<span class="alert alert-danger round">Give Valid Mobile !</span>`
		);
		setTimeout(() => {
			$("#register_form_error").html("");
		}, 3000);
	}
});


function sign_in() {
	let sign_in_content = `<div class="modal-content round">
	<div class="modal-cls">
		<span class="txt-dp-blue font-weight-bold">SIGN IN</span>
		<button required type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>

	<img class="modal-bg-img" src="${main_url}..//app-assets/welcome_page_asset/img/isometric-dribbble.png" alt=""
	 width="417px" />
	<!-- Body -->
	<div class="modal-body mb-1">

		<form id="login_form" method="post">
			<div class="md-form form-sm mb-5">
				<i class="fa fa-mobile prefix"></i>
				<input required id="login-mobile-no" name="login_number" type="number" class="form-control form-control-sm validate" />
				<label data-error="x" data-success="✓" for="login-mobile-no">Your Mobile</label>
			</div>

			<div class="md-form form-sm mb-4">
				<i class="fa fa-lock prefix"></i>
				<input required id="login-password" name="login_password" type="password" class="form-control form-control-sm validate" />
				<label data-error="x" data-success="✓" for="login-password">Your password</label>
			</div>
			<!-- loading -->
			<div style="display:none" id="loading" class="text-center"><i class="fa fa-spinner fa-spin fa-3x text-info"></i></div>

			<div class="text-center mt-2">
				<button id="login_submit" onclick="submit_new_login_form()" type="button" class="btn btn-info round">
					Sign in <i class="fa fa-sign-in ml-1"></i>
				</button>
			</div>
		</form>
	</div>
	<!-- Footer -->
	<div class="modal-footer justify-content-center">
		<div class="options text-center">
			<span id="login_form_error"></span> <br />
			<p>Forgot <span onclick="forget_pass()" style="cursor: pointer;" class="blue-text">Password?</span></p>
		</div>
	</div>
</div>`;
	$('#login_modal').html(sign_in_content);
}

function forget_pass() {

	let forget_pass_content = `<div class="modal-content round">
            <div class="modal-cls">
              <span class="txt-dp-blue font-weight-bold">Forgot Password ?</span>
              <button required type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <img class="modal-bg-img" src="${main_url}../app-assets/welcome_page_asset/img/forget.jpg" alt="" width="417px" />
            <!-- Body -->
            <div class="modal-body mb-1">
              
              <form id="login_form" method="post">
                <div class="md-form form-sm mb-5">
                  <i class="fa fa-mobile prefix"></i>
                  <input required id="forget_mobile" name="login_number" type="number" class="form-control form-control-sm validate" />
                  <label data-error="x" data-success="✓" for="login-mobile-no">Your Mobile</label>
                </div>
  
                <div id="code"></div>
                <div class="text-center" id="error"></div>
                <br>
                <!-- loading -->
                <div style="display:none" id="loading" class="text-center"><i class="fa fa-spinner fa-spin fa-3x text-info"></i></div>
                <p class="text-center text-info">A confirmation SMS will send in your mobile </p>
                
                <div class="text-center mt-2">
                  <button onclick="send_sms()" type="button" class="btn btn-info round">
                    Send <i class="fa fa-mobile-phone"></i>
                  </button>
                </div>
              </form>
              
            </div>

          </div>`;

	$('#login_modal').html(forget_pass_content);
}

function new_pass() {
	let new_pass_content = `<div class="modal-content round">
            <div class="modal-cls">
              <span class="txt-dp-blue font-weight-bold">Set New Password</span>
              <button required type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <img class="modal-bg-img" src="${main_url}../app-assets/welcome_page_asset/img/forget.jpg" alt="" width="417px" />
            <!-- Body -->
            <div class="modal-body mb-1">
              
              <form id="" method="post">
  
                <div class="md-form form-sm mb-4">
                  <i class="fa fa-lock prefix"></i>
                  <input id="new-password" name="login_password" type="password" class="form-control form-control-sm validate" />
                  <label data-error="x" data-success="✓" for="login-password">New Password</label>
                </div>
                <div class="md-form form-sm mb-4">
                  <i class="fa fa-lock prefix"></i>
                  <input id="confirm-new-password" name="login_password" type="password" class="form-control form-control-sm validate" />
                  <label data-error="x" data-success="✓" for="login-password">Confirm New Password</label>
                </div>
                <!-- loading -->
                <div style="display:none" id="loading" class="text-center"><i class="fa fa-spinner fa-spin fa-3x text-info"></i></div>
                <div id="status" class="text-center"></div>
                <div class="text-center mt-2">
                  <button onclick="newPassSave()" type="button" class="btn btn-info round">
                    Save ✓
                  </button>
                </div>
              </form>
            </div>
            <br>
            <br>
            <br>
          </div>`;
	$("#login_modal").html(new_pass_content);
}
let user_mobile;

function send_sms() {
	let mobl = $('#forget_mobile').val();
	let mob = mobl.replace(/(^880|^0|^88)/g, "");
	let code = $('#confrm_code').val();
	console.log(code);
	if (code == null) {
		$.ajax({
			type: "post",
			url: main_url + "LoginRegCtrl/forget_pass_user",
			data: {
				mobile: mob
			},
			dataType: "JSON",
			success: function (response) {
				if (response.success == 1) {
					user_mobile = mob;

					$("#code").html(`<div class="md-form form-sm mb-5">
                  <i class="fa fa-mobile prefix"></i>
                  <input required id="confrm_code" name="login_number" type="number" class="form-control form-control-sm validate" />
                  <label data-error="x" data-success="✓" for="login-mobile-no">Confirmation Code</label>
                </div>`)
				} else {
					$("#error").html(`<span class="alert alert-danger round">Mobile Number Not Exists !!</span>`);
					setTimeout(() => {
						$("#error").html(``);
					}, 4000);
				}

			}
		});
	} else {
		$.ajax({
			type: "post",
			url: main_url + "LoginRegCtrl/chk_pass_reset_code",
			data: {
				cod: code
			},
			dataType: "json",
			success: function (response) {
				if (response.success == 1) {
					new_pass();
				} else {
					$("#error").html(`<span class="alert alert-danger round">Wrong Code !!</span>`);
					setTimeout(() => {
						$("#error").html(``);
					}, 4000);
				}

			}
		});
	}
	//loading
	var $loading = $('#loading');
	$(document).ajaxStart(function () {
			$loading.show();
		})
		.ajaxStop(function () {
			$loading.hide();
		});
}

function newPassSave() {
	let pass = $("#new-password").val();
	let c_pass = $("#confirm-new-password").val();
	if (pass === c_pass) {
		$.ajax({
			type: "post",
			url: main_url + "LoginRegCtrl/reset_new_pass",
			data: {
				mobile: user_mobile,
				pass: pass
			},
			dataType: "json",
			success: function (response) {
				if (response.success == 1) {
					$("#status").html(`<span class="alert alert-info round">Saved New Password!!</span>`);
					sign_in();
				}
			}
		});
	} else {
		$("#status").html(`<span class="alert alert-danger round">Password Not Matched !!</span>`);
		setTimeout(() => {
			$("#status").html(``);
		}, 4000);
	}

	//loading
	var $loading = $('#loading');
	$(document).ajaxStart(function () {
			$loading.show();
		})
		.ajaxStop(function () {
			$loading.hide();
		});
}


function submit_new_login_form() { 
	let mob = $("#login-mobile-no").val();
	let pass = $("#login-password").val();
	let data = {
		mob: mob,
		pass: pass
	};
	if (mob != null && pass != null) {
		$.ajax({
			type: "post",
			url: main_url + "LoginRegCtrl/sign_in",
			data: data,
			dataType: "json",
			success: function (response) {
				if (response.success == 1) {
					$("#login_form_error").html(
						`<span class="alert alert-success round">Successfull, Loading Dashboard...</span>`
					);
					window.location.href = main_url + "DashboardCtrl";
				} else {
					$("#login_form_error").html(
						`<span class="alert alert-danger round">Mobile Number or Password Wrong !</span>`
					);
					setTimeout(() => {
						$("#login_form_error").html(``);
					}, 5000);
				}
			}
		});
	} else {
		setTimeout(() => {}, 5000);
	}
 }
