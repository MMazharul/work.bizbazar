-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 23, 2020 at 12:48 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bizba_affiliate`
--

-- --------------------------------------------------------

--
-- Table structure for table `activation_reqst`
--

CREATE TABLE `activation_reqst` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `sender_num` int(11) NOT NULL,
  `transection_id` varchar(100) NOT NULL,
  `amount` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '2',
  `reqst_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activation_reqst`
--

INSERT INTO `activation_reqst` (`id`, `user_id`, `sender_num`, `transection_id`, `amount`, `status`, `reqst_date`, `update_date`) VALUES
(9, 3, 2147483647, '234234234', 1500, 1, '2020-06-26 14:30:27', '2020-06-26 14:30:46'),
(10, 4, 2147483647, '123123123', 1500, 1, '2020-06-26 14:37:50', '2020-06-26 14:38:09'),
(11, 5, 1814079887, 'DADDSDS', 1500, 1, '2020-06-26 15:37:33', '2020-06-26 15:38:07'),
(12, 8, 1868135578, 'sdfsd', 1500, 1, '2020-07-20 09:28:11', '2020-07-22 05:47:48');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `admin_number` varchar(20) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `admin_number`, `password`) VALUES
(1, 'bizba.admin', 'f122db007ed655921f98184e4302bba84990ff68');

-- --------------------------------------------------------

--
-- Table structure for table `affiliate_income`
--

CREATE TABLE `affiliate_income` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_code` varchar(100) NOT NULL,
  `price` int(11) NOT NULL,
  `percentage` int(11) NOT NULL,
  `income` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `affiliate_income`
--

INSERT INTO `affiliate_income` (`id`, `user_id`, `product_code`, `price`, `percentage`, `income`, `created_at`) VALUES
(1, 1005, 'null', 415, 0, 21, '2020-06-26 15:48:24'),
(2, 1001, 'null', 620, 0, 31, '2020-06-28 00:28:31'),
(3, 1001, 'null', 620, 0, 31, '2020-06-28 00:42:15'),
(4, 1, 'null', 3040, 0, 152, '2020-06-28 00:49:10'),
(5, 1, 'null', 1850, 0, 93, '2020-06-28 13:02:40'),
(6, 1, 'null', 7750, 0, 388, '2020-06-28 13:33:19'),
(7, 1, 'null', 23440, 0, 1172, '2020-06-28 21:59:20');

-- --------------------------------------------------------

--
-- Table structure for table `balance_deduct`
--

CREATE TABLE `balance_deduct` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `refer_bonus` float DEFAULT '0',
  `refer_work` float DEFAULT '0',
  `work` float DEFAULT '0',
  `other` float DEFAULT '0',
  `reason` varchar(2000) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `balance_deduct`
--

INSERT INTO `balance_deduct` (`id`, `user_id`, `refer_bonus`, `refer_work`, `work`, `other`, `reason`, `created_at`) VALUES
(0, 1, 100, 0, 0, 100, 'Rules Violation', '2020-07-02 12:40:01');

-- --------------------------------------------------------

--
-- Table structure for table `buyer_work`
--

CREATE TABLE `buyer_work` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `id` int(11) NOT NULL,
  `job_allow` tinyint(4) NOT NULL,
  `job_allow_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `withdraw_allow` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`id`, `job_allow`, `job_allow_time`, `withdraw_allow`) VALUES
(1, 0, '2020-07-15 13:00:02', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cron_run`
--

CREATE TABLE `cron_run` (
  `id` int(11) NOT NULL,
  `Time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cron_run`
--

INSERT INTO `cron_run` (`id`, `Time`) VALUES
(1, '2020-07-14 19:58:42'),
(2, '2020-07-15 12:00:02');

-- --------------------------------------------------------

--
-- Table structure for table `flexiload_api_setting`
--

CREATE TABLE `flexiload_api_setting` (
  `id` int(11) NOT NULL,
  `api_key` varchar(100) DEFAULT NULL,
  `pin_number` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flexiload_api_setting`
--

INSERT INTO `flexiload_api_setting` (`id`, `api_key`, `pin_number`) VALUES
(1, '445015955040642791595504064', '0798');

-- --------------------------------------------------------

--
-- Table structure for table `flexiload_balance`
--

CREATE TABLE `flexiload_balance` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `blance` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flexiload_balance`
--

INSERT INTO `flexiload_balance` (`id`, `user_id`, `blance`, `created_at`, `updated_at`) VALUES
(1, 8, 10100, '2020-07-23 09:49:25', '2020-07-23 09:49:25'),
(4, NULL, NULL, '2020-07-23 12:33:39', '2020-07-23 12:33:39');

-- --------------------------------------------------------

--
-- Table structure for table `flexiload_fund_req`
--

CREATE TABLE `flexiload_fund_req` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `package_id` int(11) DEFAULT NULL,
  `sender_number` int(11) DEFAULT NULL,
  `transaction_id` varchar(100) DEFAULT NULL,
  `bank_slip` varchar(100) DEFAULT NULL,
  `bank_account` varchar(100) DEFAULT NULL,
  `payment_method` varchar(50) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0' COMMENT '0=pending,1=accepted,2=rejected',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flexiload_fund_req`
--

INSERT INTO `flexiload_fund_req` (`id`, `user_id`, `package_id`, `sender_number`, `transaction_id`, `bank_slip`, `bank_account`, `payment_method`, `status`, `created_at`, `updated_at`) VALUES
(4, 8, 1, 1868135578, '1222232', NULL, NULL, 'Bkash', 1, '2020-07-22 11:53:58', '2020-07-22 11:54:55'),
(5, 8, 3, NULL, NULL, 'adsterra-personal-review-with-payment-proof.png', '112233445566', 'Bank', 1, '2020-07-22 19:49:01', '2020-07-22 19:49:01'),
(6, 8, 4, 1898822322, '#2233444555', NULL, NULL, 'Bkash', 1, '2020-07-23 10:22:00', '2020-07-23 10:22:00'),
(7, 8, 3, 2147483647, '2342342', NULL, NULL, 'Bkash', 1, '2020-07-23 11:05:25', '2020-07-23 11:05:25');

-- --------------------------------------------------------

--
-- Table structure for table `flexiload_package`
--

CREATE TABLE `flexiload_package` (
  `id` int(11) NOT NULL,
  `package_amount` double DEFAULT NULL,
  `vadility` varchar(50) DEFAULT NULL,
  `commission` int(11) DEFAULT NULL,
  `total_commission` double DEFAULT NULL,
  `status` tinyint(11) DEFAULT NULL,
  `created_at` timestamp(6) NULL DEFAULT NULL,
  `updated_at` timestamp(6) NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flexiload_package`
--

INSERT INTO `flexiload_package` (`id`, `package_amount`, `vadility`, `commission`, `total_commission`, `status`, `created_at`, `updated_at`) VALUES
(1, 2000, '6 month', 1, 20, 1, NULL, NULL),
(2, 1000, '6 month', 1, 10, 1, NULL, NULL),
(3, 3000, '6 month', 1, 30, 1, NULL, NULL),
(4, 5000, '6 month', 1, 50, 1, NULL, NULL),
(7, 7000, '6 month', 1, 70, 0, NULL, NULL),
(11, 10000, '6 month', 1, 100, 0, NULL, NULL),
(12, 15000, '6 month', 1, 150, 0, NULL, NULL),
(13, 20000, '6 month', 1, 200, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `flexiload_payment_info`
--

CREATE TABLE `flexiload_payment_info` (
  `id` int(11) NOT NULL,
  `bkash_number` int(11) DEFAULT NULL,
  `bkash_type` varchar(50) DEFAULT NULL,
  `bank_name` varchar(50) DEFAULT NULL,
  `branch_name` varchar(50) DEFAULT NULL,
  `account_number` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flexiload_payment_info`
--

INSERT INTO `flexiload_payment_info` (`id`, `bkash_number`, `bkash_type`, `bank_name`, `branch_name`, `account_number`) VALUES
(1, 1868135578, 'Persional', 'Prime Bank', 'Muradpur Branch', '01278232836322');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(11) NOT NULL,
  `link` varchar(2000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `link`) VALUES
(1, 'http://fb.com'),
(2, 'https://www.youtube.com'),
(3, 'https://www.instagram.com/'),
(4, 'https://biz-bazar.com'),
(5, 'https://biz-bazar.com'),
(6, 'https://amazon.com'),
(7, 'https://ebay.com'),
(8, 'http://amazon.co.jp/'),
(9, 'https://www.rakuten.co.jp/'),
(10, 'http://aliexpress.com/'),
(11, 'http://craigslist.org/'),
(12, 'https://world.taobao.com/'),
(13, 'https://www.walmart.com/'),
(14, 'http://etsy.com/'),
(15, 'https://www.mercadolivre.com.br/'),
(16, 'https://global.jd.com/'),
(17, 'https://www.tmall.com/'),
(18, 'http://amazon.in/'),
(19, 'http://flipkart.com/'),
(20, 'https://www.sahibinden.com/'),
(21, 'https://www.target.com/'),
(22, 'http://pinduoduo.com/'),
(23, 'http://alibaba.com/'),
(24, 'http://olx.pl/'),
(25, 'http://shopee.co.id/'),
(26, 'http://kakaku.com/'),
(27, 'https://www.costco.com/'),
(28, 'http://hepsiburada.com/'),
(29, 'https://www.hepsiburada.com/'),
(30, 'http://hepsiburada.com/');

-- --------------------------------------------------------

--
-- Table structure for table `jobs_viewed`
--

CREATE TABLE `jobs_viewed` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `jobs_id` int(11) NOT NULL,
  `unique_userID_jobID` varchar(254) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jobs_viewed`
--

INSERT INTO `jobs_viewed` (`id`, `user_id`, `jobs_id`, `unique_userID_jobID`) VALUES
(1, 1, 1, '1-1'),
(2, 1, 12, '1-12'),
(3, 1, 11, '1-11'),
(4, 1, 2, '1-2'),
(5, 1, 3, '1-3'),
(6, 1, 14, '1-14'),
(7, 1, 4, '1-4'),
(8, 1, 16, '1-16'),
(9, 1, 5, '1-5'),
(10, 1, 27, '1-27'),
(11, 1, 26, '1-26');

-- --------------------------------------------------------

--
-- Table structure for table `job_limit`
--

CREATE TABLE `job_limit` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `job_limit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `join_fee_income`
--

CREATE TABLE `join_fee_income` (
  `id` int(11) NOT NULL,
  `mobile` int(11) NOT NULL,
  `join_fee` int(11) NOT NULL,
  `pay_type` varchar(500) DEFAULT NULL,
  `referer_earn_percentage` int(11) NOT NULL,
  `2nd_lvl_percentage` int(11) NOT NULL,
  `3rd_lvl_percentage` int(11) NOT NULL,
  `4th_lvl_percentage` int(11) NOT NULL,
  `5th_lvl_percentage` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `join_fee_income`
--

INSERT INTO `join_fee_income` (`id`, `mobile`, `join_fee`, `pay_type`, `referer_earn_percentage`, `2nd_lvl_percentage`, `3rd_lvl_percentage`, `4th_lvl_percentage`, `5th_lvl_percentage`) VALUES
(1, 1886565654, 1500, 'Personal ', 1, 4, 2, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `load_transaction`
--

CREATE TABLE `load_transaction` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `number_type` tinyint(11) DEFAULT NULL,
  `phone_number` int(11) DEFAULT NULL,
  `operator` int(11) DEFAULT NULL,
  `transaction_id` varchar(100) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL COMMENT '0=fail,1=success',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `load_transaction_temporary`
--

CREATE TABLE `load_transaction_temporary` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notice`
--

CREATE TABLE `notice` (
  `id` int(11) NOT NULL,
  `notice_content` varchar(2000) CHARACTER SET utf8 NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notice`
--

INSERT INTO `notice` (`id`, `notice_content`, `status`, `created_at`) VALUES
(36, '<p>adfsdf</p>', 1, '2020-07-20 06:21:18');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `icon` varchar(1000) NOT NULL DEFAULT '<div class="preview-icon bg-warning"><h3>!</h3></div>',
  `title` varchar(100) NOT NULL DEFAULT 'New Notification',
  `notification_txt` varchar(2000) NOT NULL,
  `link` varchar(500) NOT NULL,
  `target` tinyint(4) NOT NULL,
  `user_id` int(11) NOT NULL,
  `viewed` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `icon`, `title`, `notification_txt`, `link`, `target`, `user_id`, `viewed`, `created_at`) VALUES
(1, '<div class=\"preview-icon bg-warning\"><i class=\"mdi mdi-volume-low\"></i></div>', 'New Notification From Quick-Earn', 'hello', 'NotifyCtrl', 0, 685, 1, '2020-06-20 06:09:07'),
(2, '<div class=\"preview-icon bg-success\"><i class=\"mdi mdi-square-inc-cash\"></i></div>', 'New Balance From Quick-Earn', 'Congrats! You have got bonus from Quick Earn', 'NotifyCtrl', 0, 690, 1, '2020-06-22 11:32:49'),
(3, '<div class=\"preview-icon bg-info\"><i class=\"mdi mdi-coin\"></i></div>', 'Rules Violation', 'Dear Quick-Earn User, We notice this account break our some rules and regulation, We deduct some balance from your account.Thank You.', 'ReportsCtrl/balance_deduct', 0, 1, 1, '2020-07-02 12:40:02'),
(4, '<div class=\"preview-icon bg-success\"><i class=\"mdi mdi-square-inc-cash\"></i></div>', 'New Balance From Biz-Bazar', 'Congrats! You have got bonus from Biz Bazar', 'NotifyCtrl', 0, 1, 1, '2020-07-06 09:25:45'),
(5, '<div class=\"preview-icon bg-success\"><i class=\"mdi mdi-square-inc-cash\"></i></div>', 'New Balance From Biz-Bazar', 'Congrats! You have got bonus from Biz Bazar', 'NotifyCtrl', 0, 1, 1, '2020-07-15 07:00:18');

-- --------------------------------------------------------

--
-- Table structure for table `other_earned`
--

CREATE TABLE `other_earned` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `other_earned`
--

INSERT INTO `other_earned` (`id`, `user_id`, `amount`, `created_at`) VALUES
(1, 1005, 21, '2020-06-26 17:48:24'),
(2, 1001, 31, '2020-06-28 02:28:31'),
(3, 1001, 31, '2020-06-28 02:42:15'),
(4, 1, 152, '2020-06-28 02:49:10'),
(5, 1, 93, '2020-06-28 15:02:40'),
(6, 1, 388, '2020-06-28 15:33:19'),
(7, 1, 1172, '2020-06-28 23:59:20'),
(8, 4, 100, '2020-07-02 07:08:25'),
(9, 1, 100, '2020-07-06 11:25:39'),
(10, 1, 10, '2020-07-15 09:00:10');

-- --------------------------------------------------------

--
-- Table structure for table `refer_earned`
--

CREATE TABLE `refer_earned` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `from_user_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `refer_earned`
--

INSERT INTO `refer_earned` (`id`, `user_id`, `from_user_id`, `amount`, `created_at`) VALUES
(1, 1, 3, 375, '2020-06-26 16:30:46'),
(2, 1000, 1, 60, '2020-06-26 16:30:46'),
(3, 1, 4, 375, '2020-06-26 16:38:09'),
(4, 1000, 1, 60, '2020-06-26 16:38:09'),
(5, 3, 5, 375, '2020-06-26 17:38:07'),
(6, 1, 3, 60, '2020-06-26 17:38:07'),
(7, 1000, 1, 30, '2020-06-26 17:38:07'),
(8, 1, 8, 15, '2020-07-21 22:47:48'),
(9, 1000, 1, 60, '2020-07-21 22:47:48');

-- --------------------------------------------------------

--
-- Table structure for table `refer_earn_plan_distribution`
--

CREATE TABLE `refer_earn_plan_distribution` (
  `id` int(11) NOT NULL,
  `reputation_lvl` int(11) NOT NULL,
  `milestone_lvl` int(11) NOT NULL,
  `total_refer_member` int(11) NOT NULL,
  `percentage` int(11) NOT NULL,
  `reputation_name` varchar(50) NOT NULL,
  `milestone_name` varchar(50) NOT NULL,
  `reputation_milestone_name` varchar(50) NOT NULL,
  `reputation_icon` varchar(50) NOT NULL,
  `milestone_icon` varchar(50) NOT NULL,
  `reputation_milestone_icon` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `refer_earn_plan_distribution`
--

INSERT INTO `refer_earn_plan_distribution` (`id`, `reputation_lvl`, `milestone_lvl`, `total_refer_member`, `percentage`, `reputation_name`, `milestone_name`, `reputation_milestone_name`, `reputation_icon`, `milestone_icon`, `reputation_milestone_icon`) VALUES
(1, 1, 1, 20, 3, 'Bronze', 'Basic', 'Bronze Basic', 'bronze.svg', '1.svg', 'bronze_1.svg'),
(2, 1, 2, 40, 5, 'Bronze', 'Intermediate', 'Bronze Intermediate', 'bronze.svg', '2.svg', 'bronze_2.svg'),
(3, 1, 3, 60, 7, 'Bronze', 'Advanced', 'Bronze Advanced', 'bronze.svg', '3.svg', 'bronze_3.svg'),
(4, 1, 4, 100, 10, 'Bronze', 'Expert', 'Bronze Expert', 'bronze.svg', '4.svg', 'bronze_4.svg'),
(5, 2, 1, 120, 13, 'Silver', 'Basic', 'Silver Basic', 'silver.svg', '1.svg', 'silver_1.svg'),
(6, 2, 2, 140, 15, 'Silver', 'Intermediate', 'Silver Intermediate', 'silver.svg', '2.svg', 'silver_2.svg'),
(7, 2, 3, 160, 17, 'Silver', 'Advanced', 'Silver Advanced', 'silver.svg', '3.svg', 'silver_3.svg'),
(8, 2, 4, 200, 20, 'Silver', 'Expert', 'Silver Expert', 'silver.svg', '4.svg', 'silver_4.svg'),
(9, 3, 1, 220, 23, 'Gold', 'Basic', 'Gold Basic', 'gold.svg', '1.svg', 'gold_1.svg'),
(10, 3, 2, 240, 25, 'Gold', 'Intermediate', 'Gold Intermediate', 'gold.svg', '2.svg', 'gold_2.svg'),
(11, 3, 3, 260, 27, 'Gold', 'Advanced', 'Gold Advanced', 'gold.svg', '3.svg', 'gold_3.svg'),
(12, 3, 4, 300, 30, 'Gold', 'Expert', 'Gold Expert', 'gold.svg', '4.svg', 'gold_4.svg'),
(13, 4, 1, 320, 32, 'Platinum', 'Basic', 'Platinum Basic', 'platinum.svg', '1.svg', 'platinum_1.svg'),
(14, 4, 2, 340, 35, 'Platinum', 'Intermediate', 'Platinum Intermediate', 'platinum.svg', '2.svg', 'platinum_2.svg'),
(15, 4, 3, 360, 37, 'Platinum', 'Advanced', 'Platinum Advanced', 'platinum.svg', '3.svg', 'platinum_3.svg'),
(16, 4, 4, 400, 40, 'Platinum', 'Expert', 'Platinum Expert', 'platinum.svg', '4.svg', 'platinum_4.svg'),
(17, 5, 0, 400, 25, 'Sustainable Supporter', '', 'Sustainable Supporter', 'Sustainable_Supporter.svg', '', 'Sustainable_Supporter.svg'),
(18, 6, 0, 800, 30, 'Community Connector', '', 'Community Connector', 'Community_Connector.svg', '', 'Community_Connector.svg'),
(19, 7, 0, 1200, 35, 'Bridge Builder', '', 'Bridge Builder', 'Bridge_Builder.svg', '', 'Bridge_Builder.svg'),
(20, 8, 0, 1600, 40, 'Urban Leader', '', 'Urban Leader', 'Urban_Leader.svg', '', 'Urban_Leader.svg'),
(21, 9, 0, 2000, 50, 'LEGEND', '', 'LEGEND', 'LEGEND.svg', '', 'LEGEND.svg');

-- --------------------------------------------------------

--
-- Table structure for table `refer_network`
--

CREATE TABLE `refer_network` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `referer_id` int(11) NOT NULL,
  `referer_reputation_lvl` int(11) NOT NULL,
  `referer_milestone_lvl` int(11) NOT NULL,
  `referer_total_refered_mem` int(11) NOT NULL,
  `referer_percentage` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `refer_network`
--

INSERT INTO `refer_network` (`id`, `user_id`, `referer_id`, `referer_reputation_lvl`, `referer_milestone_lvl`, `referer_total_refered_mem`, `referer_percentage`) VALUES
(1, 1, 1000, 1, 1, 0, 2),
(2, 3, 1, 1, 1, 1, 2),
(3, 4, 1, 1, 1, 2, 2),
(4, 5, 3, 1, 1, 1, 2),
(5, 8, 1, 1, 1, 3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `refer_work_earned`
--

CREATE TABLE `refer_work_earned` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `from_user_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `refer_work_earned`
--

INSERT INTO `refer_work_earned` (`id`, `user_id`, `from_user_id`, `amount`, `created_at`) VALUES
(1, 1, 4, 0.005, '2020-07-10 14:49:46'),
(2, 1, 4, 0.005, '2020-07-10 15:22:11'),
(3, 1, 4, 0.005, '2020-07-10 15:24:51'),
(4, 1, 4, 0.005, '2020-07-10 17:08:27'),
(5, 1, 4, 0.005, '2020-07-10 17:08:27'),
(6, 1000, 1, 0.005, '2020-07-10 18:31:07'),
(7, 1000, 1, 0.005, '2020-07-10 18:31:07'),
(8, 1000, 1, 0.005, '2020-07-10 19:20:20'),
(9, 1000, 1, 0.005, '2020-07-10 19:20:20'),
(10, 1000, 1, 0.005, '2020-07-10 19:25:27'),
(11, 1000, 1, 0.005, '2020-07-10 19:25:27'),
(12, 3, 5, 0.005, '2020-07-11 04:18:43'),
(13, 3, 5, 0.005, '2020-07-11 04:18:43'),
(14, 3, 5, 0.0004, '2020-07-12 15:49:31'),
(15, 3, 5, 0.0004, '2020-07-12 15:50:38'),
(16, 3, 5, 0.0004, '2020-07-12 15:52:43'),
(17, 1000, 1, 0.0006, '2020-07-13 06:10:08'),
(18, 1, 4, 0.0006, '2020-07-14 19:58:09'),
(19, 1, 4, 0.0006, '2020-07-14 20:01:21'),
(20, 3, 5, 0.0006, '2020-07-14 20:35:18'),
(21, 1000, 1, 0.0006, '2020-07-15 13:14:51'),
(22, 1000, 1, 0.0006, '2020-07-15 13:46:18'),
(23, 1000, 1, 0.0006, '2020-07-15 14:25:27'),
(24, 1000, 1, 0.0006, '2020-07-15 14:28:23'),
(25, 1000, 1, 0.0006, '2020-07-15 14:47:45'),
(26, 1000, 1, 0.0006, '2020-07-15 14:48:23'),
(27, 1000, 1, 0.0006, '2020-07-15 14:49:21'),
(28, 1000, 1, 0.0006, '2020-07-15 14:50:01'),
(29, 1000, 1, 0.0006, '2020-07-15 14:51:17'),
(30, 1000, 1, 0.0006, '2020-07-15 14:52:21'),
(31, 1000, 1, 0.0006, '2020-07-15 14:54:54');

-- --------------------------------------------------------

--
-- Table structure for table `sent_sms`
--

CREATE TABLE `sent_sms` (
  `id` int(11) NOT NULL,
  `text` varchar(2000) NOT NULL,
  `target` varchar(30) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sent_sms`
--

INSERT INTO `sent_sms` (`id`, `text`, `target`, `created_at`) VALUES
(1, 'Dear All User,\nGood Days..Please active your account and earn unlimite..  This offer 10 Feb 2019. \nhttps://quick-earn.info. Quick Earn Outsourcing.', 'Inactive Members', '2019-01-21 10:29:27'),
(2, 'Dear All User, \nGood Days..Please active your account today and enjoy instant cash bonus. \"\"Its short time offer\"\" Quick Earn Outsourcing.', 'Inactive Members', '2019-01-23 06:46:49'),
(3, 'Dear User\nQuick-Earn.Info, Today Active Any Refer Account, Enjoy Instant Cash Bonus From Quick Earn. Please Enjoy Your Get a Bonus.\nQuick Earn Outsourcing', 'Active Members', '2019-02-03 07:22:08'),
(4, 'Dear User\nQuick-Earn.Info, Today Active Any Refer Account, Enjoy Instant Cash Bonus From Quick Earn. Please Enjoy Your Get a Bonus.\nQuick Earn Outsourcing', 'Active Members', '2019-02-04 06:32:11'),
(5, 'Dear User\n\nQuick-Earn.Info, Today Active Any Refer Account, Enjoy Instant Cash Bonus From Quick Earn. Please Enjoy Your Refer & Get a Bonus.\n\nQuick Earn Outsourcing', 'Active Members', '2019-02-08 03:43:32'),
(6, 'Dear User\n\nQuick-Earn.Info, Today Active Any Refer Account, Enjoy Instant Cash Bonus From Quick Earn. Please Enjoy Your Refer & Get a Bonus.\n\nQuick Earn Outsourcing', 'Active Members', '2019-02-08 03:45:02'),
(7, 'Dear User\n\nQuick-Earn.Info, Today Active Any Refer Account, Enjoy Instant Cash Bonus From Quick Earn. Please Enjoy Your Refer & Get a Bonus.\n\nQuick Earn Outsourcing', 'Active Members', '2019-02-08 03:46:30'),
(8, 'Dear User\n\nQuick-Earn.Info, Today Active Any Refer Account, Enjoy Instant Cash Bonus From Quick Earn. Please Enjoy Your Refer & Get a Bonus.\n\nQuick Earn Outsourcing', 'Active Members', '2019-02-08 03:47:58');

-- --------------------------------------------------------

--
-- Table structure for table `total_balance`
--

CREATE TABLE `total_balance` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `refer_earn_amount` double DEFAULT '0',
  `work_earn_amount` double DEFAULT '0',
  `refer_work_earn_amount` double DEFAULT '0',
  `other_earn_amount` double NOT NULL DEFAULT '0',
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `total_balance`
--

INSERT INTO `total_balance` (`id`, `user_id`, `refer_earn_amount`, `work_earn_amount`, `refer_work_earn_amount`, `other_earn_amount`, `updated_at`) VALUES
(2, 1, 725, 1.8600000000000003, 0.0262, 815, '2020-07-21 22:47:48'),
(415, 2, 0, 0, 0, 0, '2020-06-26 16:24:38'),
(417, 3, 375, 0, 0.011799999999999998, 0, '2020-07-14 20:35:18'),
(418, 4, 0, 1.31, 0, 100, '2020-07-14 20:01:21'),
(419, 5, 0, 0.5900000000000001, 0, 0, '2020-07-14 20:35:18'),
(420, 6, 0, 0, 0, 0, '2020-07-11 10:03:33'),
(421, 7, 0, 0, 0, 0, '2020-07-12 12:49:44'),
(422, 8, 0, 0, 0, 0, '2020-07-15 08:54:51');

-- --------------------------------------------------------

--
-- Table structure for table `users_detail`
--

CREATE TABLE `users_detail` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `gender` int(11) DEFAULT '1',
  `adress` varchar(2000) DEFAULT NULL,
  `activation_pin` varchar(100) NOT NULL,
  `refer_from` int(11) NOT NULL,
  `national_id` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `active_status` int(11) NOT NULL DEFAULT '0',
  `own_refer_id` int(11) NOT NULL,
  `acc_active_date` timestamp NULL DEFAULT NULL,
  `join_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_detail`
--

INSERT INTO `users_detail` (`id`, `name`, `gender`, `adress`, `activation_pin`, `refer_from`, `national_id`, `picture`, `active_status`, `own_refer_id`, `acc_active_date`, `join_date`) VALUES
(1, 'Tarik Masud Chowdori', 1, 'GEC, Nasriabad', '37j246', 1000, '22222222222222', '1.png', 1, 1001, '2018-12-14 03:17:21', '2018-12-14 02:57:34'),
(3, 'test2', NULL, NULL, '33u964', 1001, NULL, 'avater_0.png', 1, 1003, '2020-06-26 14:30:46', '2020-06-26 14:29:15'),
(4, 'test3', NULL, NULL, '44f971', 1001, NULL, 'avater_0.png', 1, 1004, '2020-06-26 14:38:09', '2020-06-26 14:36:46'),
(5, 'rakib', NULL, NULL, '92y676', 1003, NULL, 'avater_3.png', 1, 1005, '2020-06-26 15:38:07', '2020-06-26 15:36:24'),
(6, 'Ripon Uddin', NULL, NULL, '67i755', 1001, NULL, 'avater_7.png', 0, 1006, NULL, '2020-07-11 08:03:33'),
(7, 'arif', NULL, NULL, '61n916', 1001, NULL, 'avater_1.png', 0, 1007, NULL, '2020-07-12 10:49:43'),
(8, 'Majahar', NULL, NULL, '64b232', 1001, NULL, 'avater_4.png', 1, 1008, '2020-07-22 05:47:48', '2020-07-15 06:54:51');

-- --------------------------------------------------------

--
-- Table structure for table `users_login`
--

CREATE TABLE `users_login` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `mobile_num` int(11) NOT NULL,
  `email` varchar(70) DEFAULT NULL,
  `password` varchar(500) NOT NULL,
  `username` varchar(100) NOT NULL,
  `last_refer_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_login`
--

INSERT INTO `users_login` (`id`, `user_id`, `mobile_num`, `email`, `password`, `username`, `last_refer_date`) VALUES
(2, 1, 1721016179, 'biz@mail.com', '$2y$10$wrm50NL1g9I7qDC4kHSCFuyPJWybZsm6rSHi/aY1fXCz5MPNp38C6', 'biz-admin', '2020-07-22 05:47:48'),
(750, 3, 1866604471, NULL, '$2y$10$wrm50NL1g9I7qDC4kHSCFuyPJWybZsm6rSHi/aY1fXCz5MPNp38C6', 'test23', '2020-06-26 15:38:07'),
(751, 4, 1899904471, NULL, '$2y$10$wrm50NL1g9I7qDC4kHSCFuyPJWybZsm6rSHi/aY1fXCz5MPNp38C6', 'test34', NULL),
(752, 5, 1814079887, NULL, '$2y$10$wrm50NL1g9I7qDC4kHSCFuyPJWybZsm6rSHi/aY1fXCz5MPNp38C6', 'rakib5', NULL),
(753, 6, 1829894659, NULL, '$2y$10$wrm50NL1g9I7qDC4kHSCFuyPJWybZsm6rSHi/aY1fXCz5MPNp38C6', 'RiponUddin6', NULL),
(754, 7, 1833446966, NULL, '$2y$10$wrm50NL1g9I7qDC4kHSCFuyPJWybZsm6rSHi/aY1fXCz5MPNp38C6', 'arif7', NULL),
(755, 8, 1868135578, NULL, '$2y$10$wrm50NL1g9I7qDC4kHSCFuyPJWybZsm6rSHi/aY1fXCz5MPNp38C6', 'Majahar8', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_profile`
--

CREATE TABLE `users_profile` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `skill` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_profile`
--

INSERT INTO `users_profile` (`id`, `user_id`, `title`, `description`, `skill`) VALUES
(2, 1, 'Graphic dEsignre', 'asdf jkasdf klasdfasdlkfjasdlkfjsdFsf asdfa sdfasd fasd fasdf asd fasd fasdf a sdf asdfasdf asd fasdf asd fasdfadfasdf asd fasdfasd fasdf asd fasdf asdf asd fasd fasd fasd asd fasdf asdf asdf asdadf asd sdfasdf asd fg sdfgadgasdg adfg adfgadgdafgasdf adfg addClassadfg asd asd fasd fasd fasd fasdfsd fasd fasd fadfasdfasd kj', 'asda asd asd asda s');

-- --------------------------------------------------------

--
-- Table structure for table `user_lvl`
--

CREATE TABLE `user_lvl` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `reputation_lvl` int(11) NOT NULL,
  `milestone_lvl` int(11) NOT NULL,
  `total_refered` int(11) NOT NULL,
  `earn_percentage` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_lvl`
--

INSERT INTO `user_lvl` (`id`, `user_id`, `reputation_lvl`, `milestone_lvl`, `total_refered`, `earn_percentage`) VALUES
(1, 1, 1, 1, 3, 3),
(52, 3, 1, 1, 1, 2),
(53, 4, 1, 1, 0, 2),
(54, 5, 1, 1, 0, 2),
(55, 8, 1, 1, 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `withdraw`
--

CREATE TABLE `withdraw` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `receive_number` int(11) NOT NULL,
  `admin_snd_num` int(11) DEFAULT NULL,
  `transection_id` varchar(255) DEFAULT NULL,
  `amount` double NOT NULL,
  `paid_total_amount` int(11) DEFAULT NULL,
  `paid_work_amnt` double DEFAULT NULL,
  `paid_refer_work_amnt` double DEFAULT NULL,
  `paid_refer_bon_amnt` double DEFAULT NULL,
  `paid_other_amnt` double DEFAULT NULL,
  `pay_method` varchar(50) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `withdraw`
--

INSERT INTO `withdraw` (`id`, `user_id`, `receive_number`, `admin_snd_num`, `transection_id`, `amount`, `paid_total_amount`, `paid_work_amnt`, `paid_refer_work_amnt`, `paid_refer_bon_amnt`, `paid_other_amnt`, `pay_method`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1814079887, 1721016179, 'DADDSDS', 500, 500, 0, 0, 0, 500, 'bkash-personal', 1, '2020-07-04 15:26:01', '2020-07-04 15:28:47'),
(2, 1, 1814079887, 1721016179, 'assacscsd', 500, 500, 0, 0, 0, 500, 'bkash-personal', 1, '2020-07-06 11:28:00', '2020-07-06 11:28:54');

-- --------------------------------------------------------

--
-- Table structure for table `work_earned`
--

CREATE TABLE `work_earned` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `work_earned`
--

INSERT INTO `work_earned` (`id`, `user_id`, `amount`, `created_at`) VALUES
(2, 4, 0.25, '2020-07-10 14:49:46'),
(3, 4, 0.25, '2020-07-10 15:22:11'),
(4, 4, 0.25, '2020-07-10 15:24:51'),
(5, 4, 0.25, '2020-07-10 17:08:27'),
(6, 4, 0.25, '2020-07-10 17:08:27'),
(7, 1, 0.25, '2020-07-10 18:31:07'),
(8, 1, 0.25, '2020-07-10 18:31:07'),
(9, 1, 0.25, '2020-07-10 19:20:20'),
(10, 1, 0.25, '2020-07-10 19:20:20'),
(11, 1, 0.25, '2020-07-10 19:25:27'),
(12, 1, 0.25, '2020-07-10 19:25:27'),
(13, 5, 0.25, '2020-07-11 04:18:43'),
(14, 5, 0.25, '2020-07-11 04:18:43'),
(15, 5, 0.02, '2020-07-12 15:49:31'),
(16, 5, 0.02, '2020-07-12 15:50:38'),
(17, 5, 0.02, '2020-07-12 15:52:43'),
(18, 1, 0.03, '2020-07-13 06:10:08'),
(19, 4, 0.03, '2020-07-14 19:58:09'),
(20, 4, 0.03, '2020-07-14 20:01:21'),
(21, 5, 0.03, '2020-07-14 20:35:18'),
(22, 1, 0.03, '2020-07-15 13:14:51'),
(23, 1, 0.03, '2020-07-15 13:46:18'),
(24, 1, 0.03, '2020-07-15 14:25:27'),
(25, 1, 0.03, '2020-07-15 14:28:23'),
(26, 1, 0.03, '2020-07-15 14:47:45'),
(27, 1, 0.03, '2020-07-15 14:48:23'),
(28, 1, 0.03, '2020-07-15 14:49:21'),
(29, 1, 0.03, '2020-07-15 14:50:01'),
(30, 1, 0.03, '2020-07-15 14:51:17'),
(31, 1, 0.03, '2020-07-15 14:52:21'),
(32, 1, 0.03, '2020-07-15 14:54:54');

-- --------------------------------------------------------

--
-- Table structure for table `work_earn_plan_distribution`
--

CREATE TABLE `work_earn_plan_distribution` (
  `id` int(11) NOT NULL,
  `earn_per_link` double NOT NULL,
  `job_session_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `work_earn_plan_distribution`
--

INSERT INTO `work_earn_plan_distribution` (`id`, `earn_per_link`, `job_session_time`) VALUES
(1, 1, '2020-07-16 00:00:02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activation_reqst`
--
ALTER TABLE `activation_reqst`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `affiliate_income`
--
ALTER TABLE `affiliate_income`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `buyer_work`
--
ALTER TABLE `buyer_work`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cron_run`
--
ALTER TABLE `cron_run`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flexiload_api_setting`
--
ALTER TABLE `flexiload_api_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flexiload_balance`
--
ALTER TABLE `flexiload_balance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flexiload_fund_req`
--
ALTER TABLE `flexiload_fund_req`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flexiload_package`
--
ALTER TABLE `flexiload_package`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flexiload_payment_info`
--
ALTER TABLE `flexiload_payment_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs_viewed`
--
ALTER TABLE `jobs_viewed`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_userID_jobID` (`unique_userID_jobID`);

--
-- Indexes for table `job_limit`
--
ALTER TABLE `job_limit`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `join_fee_income`
--
ALTER TABLE `join_fee_income`
  ADD PRIMARY KEY (`id`),
  ADD KEY `referer_earn_percentage` (`referer_earn_percentage`);

--
-- Indexes for table `load_transaction_temporary`
--
ALTER TABLE `load_transaction_temporary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notice`
--
ALTER TABLE `notice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `other_earned`
--
ALTER TABLE `other_earned`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `refer_earned`
--
ALTER TABLE `refer_earned`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `refer_earn_plan_distribution`
--
ALTER TABLE `refer_earn_plan_distribution`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reputation_lvl` (`reputation_lvl`),
  ADD KEY `milestone_lvl` (`milestone_lvl`),
  ADD KEY `percentage` (`percentage`);

--
-- Indexes for table `refer_network`
--
ALTER TABLE `refer_network`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id_2` (`user_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `referer_id` (`referer_id`),
  ADD KEY `referer_milestone_lvl` (`referer_milestone_lvl`) USING BTREE,
  ADD KEY `referer_reputation_lvl` (`referer_reputation_lvl`) USING BTREE,
  ADD KEY `referer_percentage` (`referer_percentage`);

--
-- Indexes for table `refer_work_earned`
--
ALTER TABLE `refer_work_earned`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sent_sms`
--
ALTER TABLE `sent_sms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `total_balance`
--
ALTER TABLE `total_balance`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `users_detail`
--
ALTER TABLE `users_detail`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `own_refer_id` (`own_refer_id`),
  ADD UNIQUE KEY `national_id` (`national_id`),
  ADD KEY `refer_from` (`refer_from`),
  ADD KEY `id_2` (`id`);

--
-- Indexes for table `users_login`
--
ALTER TABLE `users_login`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mobile_num` (`mobile_num`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `users_profile`
--
ALTER TABLE `users_profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_lvl`
--
ALTER TABLE `user_lvl`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD KEY `reputation_lvl` (`reputation_lvl`),
  ADD KEY `milestone_lvl` (`milestone_lvl`),
  ADD KEY `earn_percentage` (`earn_percentage`);

--
-- Indexes for table `withdraw`
--
ALTER TABLE `withdraw`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `work_earned`
--
ALTER TABLE `work_earned`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `work_earn_plan_distribution`
--
ALTER TABLE `work_earn_plan_distribution`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activation_reqst`
--
ALTER TABLE `activation_reqst`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `affiliate_income`
--
ALTER TABLE `affiliate_income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `buyer_work`
--
ALTER TABLE `buyer_work`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cron_run`
--
ALTER TABLE `cron_run`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `flexiload_api_setting`
--
ALTER TABLE `flexiload_api_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `flexiload_balance`
--
ALTER TABLE `flexiload_balance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `flexiload_fund_req`
--
ALTER TABLE `flexiload_fund_req`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `flexiload_package`
--
ALTER TABLE `flexiload_package`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `flexiload_payment_info`
--
ALTER TABLE `flexiload_payment_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `jobs_viewed`
--
ALTER TABLE `jobs_viewed`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `job_limit`
--
ALTER TABLE `job_limit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `join_fee_income`
--
ALTER TABLE `join_fee_income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `load_transaction_temporary`
--
ALTER TABLE `load_transaction_temporary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notice`
--
ALTER TABLE `notice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `other_earned`
--
ALTER TABLE `other_earned`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `refer_earned`
--
ALTER TABLE `refer_earned`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `refer_earn_plan_distribution`
--
ALTER TABLE `refer_earn_plan_distribution`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `refer_network`
--
ALTER TABLE `refer_network`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `refer_work_earned`
--
ALTER TABLE `refer_work_earned`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `sent_sms`
--
ALTER TABLE `sent_sms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `total_balance`
--
ALTER TABLE `total_balance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=423;

--
-- AUTO_INCREMENT for table `users_login`
--
ALTER TABLE `users_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=756;

--
-- AUTO_INCREMENT for table `users_profile`
--
ALTER TABLE `users_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_lvl`
--
ALTER TABLE `user_lvl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `withdraw`
--
ALTER TABLE `withdraw`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `work_earned`
--
ALTER TABLE `work_earned`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
